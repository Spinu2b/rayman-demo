﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Animations
{
    public class AnimationController : MonoBehaviour
    {
        private Animator animator;
        private bool animatorIsUpdating = true;

        private Dictionary<int, string> 
            currentStatesOnLayers = new Dictionary<int, string>();

        private Dictionary<int, int>
            currentPrioritiesOnLayers = new Dictionary<int, int>();

        private float registeredUpdateDeltaTime = 0f;

        void Awake()
        {
            animator = GetComponent<Animator>();

            currentStatesOnLayers.Add(0, "");
            currentStatesOnLayers.Add(1, "");
            currentStatesOnLayers.Add(2, "");

            currentPrioritiesOnLayers.Add(0, 0);
            currentPrioritiesOnLayers.Add(1, 0);
            currentPrioritiesOnLayers.Add(2, 0);

            animator.speed = 0f;
        }

        public void StopAnimatorUpdating()
        {
            this.animatorIsUpdating = false;
            animator.speed = 0f;
        }

        public void ResumeAnimatorUpdating()
        {
            this.animatorIsUpdating = true;
        }

        private void FixedUpdate()
        {
            if (Time.fixedDeltaTime < registeredUpdateDeltaTime && animatorIsUpdating)
            {
                animator.speed = 1f;
                animator.Update(Time.fixedDeltaTime);
                animator.speed = 0f;
            }
        }

        private void Update()
        {
            registeredUpdateDeltaTime = Time.deltaTime;

            if (registeredUpdateDeltaTime <= Time.fixedDeltaTime && animatorIsUpdating)
            {
                animator.speed = 1f;
                animator.Update(Time.deltaTime);
                animator.speed = 0f;
            }            

            List<int> layersIndexes = new List<int>(currentStatesOnLayers.Keys);
            foreach (var layerIndex in layersIndexes)
            {
                if (layerIndex > 0)
                {
                    var clipsAtLayer = animator.GetCurrentAnimatorClipInfo(layerIndex);
                    if (clipsAtLayer.Length > 0)
                    {
                        currentStatesOnLayers[layerIndex] =
                            animator.GetCurrentAnimatorClipInfo(layerIndex)[0].clip.name;
                    } else
                    {
                        currentStatesOnLayers[layerIndex] = "";
                    }                    
                }
            }
        }

        //private void Update()
        //{
        //    animator.speed = 1f;
        //    animator.Update(Time.deltaTime);
        //    animator.speed = 0f;
        //}

        public string GetCurrentAnimationState(int layerIndex = 0) => currentStatesOnLayers[layerIndex];

        public bool HasAnimationEnded(int layerIndex = 0) => 
            animator.GetCurrentAnimatorStateInfo(layerIndex).normalizedTime > 1 
            && !animator.IsInTransition(layerIndex);

        public float GetAnimationNormalizedTime(int layerIndex = 0)
            => animator.GetCurrentAnimatorStateInfo(layerIndex).normalizedTime;

        public void ChangeAnimationStateWithPriority(string newState, int layerIndex = 0, int priority = 1)
        {
            //stop the same animation from interrupting itself unless it has now higher priority
            if (currentStatesOnLayers[layerIndex].Equals(newState) &&
                currentPrioritiesOnLayers[layerIndex] >= priority) return;

            if (priority >= currentPrioritiesOnLayers[layerIndex] || HasAnimationEnded(layerIndex))
            {
                //play the animation
                animator.Play(newState);

                //reassign the current state
                currentStatesOnLayers[layerIndex] = newState;
                currentPrioritiesOnLayers[layerIndex] = priority;
            }            
        }

        public void ChangeAnimationState(string newState, int layerIndex = 0)
        {
            ChangeAnimationStateWithPriority(newState, layerIndex, 1);
        }

        internal void ChangeAnimationPriorityIfItIsBeingPlayed(
            string stateName, int newPriority, int layerIndex = 0)
        {
            if (currentStatesOnLayers[layerIndex].Equals(stateName))
            {
                currentPrioritiesOnLayers[layerIndex] = newPriority;
            }
        }

        public void SetAnimationFrameInManualUpdating
            (string stateName, float normalizedTime, float deltaTime, int layerIndex = 0)
        {
            animator.speed = 1f;
            animator.Play(stateName, layerIndex, normalizedTime);
            animator.Update(deltaTime);
            animator.speed = 0f;
        }
    }
}
