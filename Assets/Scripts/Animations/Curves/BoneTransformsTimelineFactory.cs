﻿using Assets.Scripts.Animations.Models.Model.Curves;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Animations.Curves
{
    public static class EditorCurveBindingFactory
    {
        #if (UNITY_EDITOR)
        public static 
            Tuple<EditorCurveBinding, EditorCurveBinding, EditorCurveBinding> 
            GetBonePositionEditorCurveBinding(
            Transform boneRootTransform, Transform boneTransform, string boneName)
        {
            string path = AnimationUtility.CalculateTransformPath(boneTransform, boneRootTransform);
            
            var posXCurveBinding = EditorCurveBinding.FloatCurve(path, typeof(Transform), "m_LocalPosition.x");
            var posYCurveBinding = EditorCurveBinding.FloatCurve(path, typeof(Transform), "m_LocalPosition.y");
            var posZCurveBinding = EditorCurveBinding.FloatCurve(path, typeof(Transform), "m_LocalPosition.z");

            return Tuple.Create(posXCurveBinding, posYCurveBinding, posZCurveBinding);
        }

        public static Tuple<EditorCurveBinding, EditorCurveBinding, EditorCurveBinding, EditorCurveBinding>
            GetBoneRotationEditorCurveBinding(
            Transform boneRootTransform, Transform boneTransform, string boneName)
        {
            string path = AnimationUtility.CalculateTransformPath(boneTransform, boneRootTransform);

            var rotWCurveBinding = EditorCurveBinding.FloatCurve(path, typeof(Transform), "m_LocalRotation.w");
            var rotXCurveBinding = EditorCurveBinding.FloatCurve(path, typeof(Transform), "m_LocalRotation.x");
            var rotYCurveBinding = EditorCurveBinding.FloatCurve(path, typeof(Transform), "m_LocalRotation.y");
            var rotZCurveBinding = EditorCurveBinding.FloatCurve(path, typeof(Transform), "m_LocalRotation.z");

            return Tuple.Create(rotWCurveBinding, rotXCurveBinding, rotYCurveBinding, rotZCurveBinding);
        }

        public static Tuple<EditorCurveBinding, EditorCurveBinding, EditorCurveBinding>
            GetBoneScaleEditorCurveBinding(
            Transform boneRootTransform, Transform boneTransform, string boneName)
        {
            string path = AnimationUtility.CalculateTransformPath(boneTransform, boneRootTransform);

            var scaleXCurveBinding = EditorCurveBinding.FloatCurve(path, typeof(Transform), "m_LocalScale.x");
            var scaleYCurveBinding = EditorCurveBinding.FloatCurve(path, typeof(Transform), "m_LocalScale.y");
            var scaleZCurveBinding = EditorCurveBinding.FloatCurve(path, typeof(Transform), "m_LocalScale.z");

            return Tuple.Create(scaleXCurveBinding, scaleYCurveBinding, scaleZCurveBinding);
        }
        #endif
    }

    public static class FloatTransformCurvesHelper
    {
        #if (UNITY_EDITOR)
        public static Tuple<AnimationCurve, AnimationCurve, AnimationCurve> 
            GetPositionTransformFloatCurve(
            AnimationClip animationClip,
            Tuple<EditorCurveBinding, EditorCurveBinding, EditorCurveBinding> curvesBindings)
        {
            AnimationCurve positionXCurve = AnimationUtility.GetEditorCurve(animationClip, curvesBindings.Item1);
            AnimationCurve positionYCurve = AnimationUtility.GetEditorCurve(animationClip, curvesBindings.Item2);
            AnimationCurve positionZCurve = AnimationUtility.GetEditorCurve(animationClip, curvesBindings.Item3);

            return Tuple.Create(positionXCurve, positionYCurve, positionZCurve);
        }

        public static Tuple<AnimationCurve, AnimationCurve, AnimationCurve, AnimationCurve>
            GetRotationTransformFloatCurve(
            AnimationClip animationClip,
            Tuple<EditorCurveBinding, EditorCurveBinding, EditorCurveBinding, EditorCurveBinding> curvesBindings)
        {
            AnimationCurve rotationWCurve = AnimationUtility.GetEditorCurve(animationClip, curvesBindings.Item1);
            AnimationCurve rotationXCurve = AnimationUtility.GetEditorCurve(animationClip, curvesBindings.Item2);
            AnimationCurve rotationYCurve = AnimationUtility.GetEditorCurve(animationClip, curvesBindings.Item3);
            AnimationCurve rotationZCurve = AnimationUtility.GetEditorCurve(animationClip, curvesBindings.Item4);

            return Tuple.Create(rotationWCurve, rotationXCurve, rotationYCurve, rotationZCurve);
        }

        public static Tuple<AnimationCurve, AnimationCurve, AnimationCurve>
            GetScaleTransformFloatCurve(
            AnimationClip animationClip,
            Tuple<EditorCurveBinding, EditorCurveBinding, EditorCurveBinding> curvesBindings)
        {
            AnimationCurve scaleXCurve = AnimationUtility.GetEditorCurve(animationClip, curvesBindings.Item1);
            AnimationCurve scaleYCurve = AnimationUtility.GetEditorCurve(animationClip, curvesBindings.Item2);
            AnimationCurve scaleZCurve = AnimationUtility.GetEditorCurve(animationClip, curvesBindings.Item3);

            return Tuple.Create(scaleXCurve, scaleYCurve, scaleZCurve);
        }
        #endif
    }

    public static class BoneTransformsTimelineFactory
    {
        #if (UNITY_EDITOR)
        public static BoneTransTimeline
            GetBoneTransformKeyframesTimelineFromAnimationClip(
                AnimationClip animationClip, Transform boneRootTransform, Transform boneTransform, string boneName)
        {
            var result = new BoneTransTimeline();

            var positionFloatCurvesBindings = EditorCurveBindingFactory
                        .GetBonePositionEditorCurveBinding(boneRootTransform, boneTransform, boneName);

            var rotationFloatCurvesBindings = EditorCurveBindingFactory
                        .GetBoneRotationEditorCurveBinding(boneRootTransform, boneTransform, boneName);

            var scaleFloatCurvesBindings = EditorCurveBindingFactory
                        .GetBoneScaleEditorCurveBinding(boneRootTransform, boneTransform, boneName);

            var positionTransformsCurves = 
                FloatTransformCurvesHelper.GetPositionTransformFloatCurve(animationClip, positionFloatCurvesBindings);

            var rotationTransformCurves =
                FloatTransformCurvesHelper.GetRotationTransformFloatCurve(animationClip, rotationFloatCurvesBindings);

            var scaleTransformCurves =
                FloatTransformCurvesHelper.GetScaleTransformFloatCurve(animationClip, scaleFloatCurvesBindings);

            int animationClipFrames = (int)Math.Round(animationClip.length * animationClip.frameRate);
            float frameRate = animationClip.frameRate;
            
            return BoneTransTimeline.WithPositionRotationScaleTransformsCurves(
                positionTransformsCurves, rotationTransformCurves, scaleTransformCurves,
                frameRate,
                animationClipFrames);
        }
        #endif
    }
}
