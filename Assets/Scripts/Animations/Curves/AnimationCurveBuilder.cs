﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Animations.Curves
{
    public class AnimationCurveBuilder
    {
        private List<Keyframe> buildingKeys;

        public AnimationCurveBuilder()
        {
            this.buildingKeys = new List<Keyframe>();
        }

        public AnimationCurveBuilder AddValueForFrame(int frameNumber, int totalFrames, float value)
        {
            buildingKeys.Add(new Keyframe(frameNumber / (float)totalFrames, value));
            return this;
        }

        public AnimationCurve Build()
        {
            var result = new AnimationCurve();
            result.keys = buildingKeys.ToArray();
            return result;
        }
    }
}
