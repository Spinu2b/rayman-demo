﻿using Assets.Scripts.Animations.Models.Rayman.Recipes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Animations.Models.Model
{
    public class AnimationClipConstructingRecipe
    {
        #if (UNITY_EDITOR)
        public string baseAnimationClipName;
        public IAnimConstrRecipe animationConstructionRecipeImplementation;
        public string newAnimationClipName;
        public int animationClipLayerIndex;
        public float animationSpeed;

        public AnimationClipConstructingRecipe(
            string baseAnimationClipName,
            IAnimConstrRecipe animationConstructionRecipeImplementation,
            string newAnimationClipName,
            float animationSpeed,
            int animationClipLayerIndex)
        {
            this.baseAnimationClipName = baseAnimationClipName;
            this.animationConstructionRecipeImplementation = animationConstructionRecipeImplementation;
            this.newAnimationClipName = newAnimationClipName;
            this.animationClipLayerIndex = animationClipLayerIndex;
            this.animationSpeed = animationSpeed;
        }


        public AdditionalAnimationClipInfo GetDerivedAnimationClipAccordingToRecipe
            (Transform animatorTransform, List<Transform> animatorBonesTransforms, AnimationClip baseAnimationClip)
        {
            var newAnimationClip = animationConstructionRecipeImplementation
                .GetDerivedAnimationClipAccordingToRecipe(
                animatorTransform, animatorBonesTransforms, newAnimationClipName, baseAnimationClip);

            return new AdditionalAnimationClipInfo(
                animationClipLayerIndex, animationSpeed, newAnimationClip);
        }
        #endif
    }
}
