﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Assets.Scripts.Animations.Curves;

namespace Assets.Scripts.Animations.Models.Model.Curves
{
    public static class FloatCurvesTimelineHelper
    {
        public static List<int> 
            GetUniformFramesNumbersForEstablishedKeyframing(
                Tuple<AnimationCurve, AnimationCurve, AnimationCurve> positionTransformsCurves,
                Tuple<AnimationCurve, AnimationCurve, AnimationCurve, AnimationCurve> rotationTransformCurves,
                Tuple<AnimationCurve, AnimationCurve, AnimationCurve> scaleTransformCurves,
                float frameRate,
                int animationFrames)
        {
            int maxFramesPosition =
                (int)Math.Round(
                    positionTransformsCurves.Item1.keys[positionTransformsCurves.Item1.length - 1].time * frameRate);

            int maxFramesRotation =
                (int)Math.Round(
                    rotationTransformCurves.Item1.keys[rotationTransformCurves.Item1.length - 1].time * frameRate);

            int maxFramesScale =
                (int)Math.Round(
                    scaleTransformCurves.Item1.keys[scaleTransformCurves.Item1.length - 1].time * frameRate);

            int maxFramesOverall = (new List<int>() { maxFramesPosition, maxFramesRotation, maxFramesScale })
                .Max();

            //return animationFrames;

            var result = new List<int>();

            result.Add(1);
            for (int i = 2; i < animationFrames; i+=3)
            {
                result.Add(i);
            }
            result.Add(animationFrames);

            //if (maxFramesOverall == maxFramesPosition)
            //{
            //    result = positionTransformsCurves.Item1.keys.Select(x => (int)Math.Round(x.time * frameRate)).ToList();
            //}
            //else if (maxFramesOverall == maxFramesRotation)
            //{
            //    result = rotationTransformCurves.Item1.keys.Select(x => (int)Math.Round(x.time * frameRate)).ToList();
            //}
            //else
            //{
            //    result = scaleTransformCurves.Item1.keys.Select(x => (int)Math.Round(x.time * frameRate)).ToList();
            //}

            //if (!result.Contains(animationFrames))
            //{
            //    result.Add(animationFrames);
            //}

            //if (!result.Contains(1))
            //{
            //    result.Add(1);
            //}

            return result;
        }

        public static float GetProperValueForCurve
            (AnimationCurve animationCurve,
             int frameNumber,
             float frameRate,
             int overallAnimationFrames)
        {
            Keyframe closestProperKeyframe = animationCurve.keys.OrderBy(
                x => Mathf.Abs(frameNumber - (x.time * frameRate))).First();

            int closestProperKeyframeFrameNumber = (int)Math.Round(closestProperKeyframe.time * frameRate);

            if (frameNumber >= (int)Math.Round(animationCurve.keys[animationCurve.length - 1].time * frameRate))
            {
                return closestProperKeyframe.value;
            } else if (frameNumber == closestProperKeyframeFrameNumber)
            {
                return closestProperKeyframe.value;
            }
            else
            {
                Keyframe startInterpolationKeyframe;
                Keyframe endInterpolationKeyframe;

                startInterpolationKeyframe =
                    animationCurve.keys.Where(
                        x => (int)Math.Round(x.time * frameRate) <= frameNumber)
                        .OrderBy(x => (int)Math.Round(x.time * frameRate)).Last();

                endInterpolationKeyframe =
                    animationCurve.keys.Where(
                        x => (int)Math.Round(x.time * frameRate) >= frameNumber)
                        .OrderBy(x => (int)Math.Round(x.time * frameRate)).First();

                int startInterpolationKeyframeFrame = (int)Math.Round(startInterpolationKeyframe.time * frameRate);
                int endInterpolationKeyframeFrame = (int)Math.Round(endInterpolationKeyframe.time * frameRate);

                int framesDifference = endInterpolationKeyframeFrame - startInterpolationKeyframeFrame;

                return Mathf.Lerp(
                    GetProperValueForCurve(
                        animationCurve, startInterpolationKeyframeFrame, frameRate, overallAnimationFrames),
                    GetProperValueForCurve(
                        animationCurve, endInterpolationKeyframeFrame, frameRate, overallAnimationFrames),
                    (frameNumber - startInterpolationKeyframeFrame) / (float)framesDifference);
            }
        }
    }

    public static class FloatCurvesKeyframesHelper
    {
        //private static int keyframeStep = 3;

        public static List<Tuple<int, BoneKeyframeTransform>> 
            IterateCombinedKeyframeTransforms(
            Tuple<AnimationCurve, AnimationCurve, AnimationCurve> positionTransformsCurves,
            Tuple<AnimationCurve, AnimationCurve, AnimationCurve, AnimationCurve> rotationTransformCurves,
            Tuple<AnimationCurve, AnimationCurve, AnimationCurve> scaleTransformCurves,
            float frameRate,
            int animationFrames)
        {
            // assume that keyframes numbers are exactly same for all coordinates in respective curves
            // that is what we expect anyway

            var result = new List<Tuple<int, BoneKeyframeTransform>>();

            foreach (var frameNumberForKeyframe in 
                FloatCurvesTimelineHelper
                    .GetUniformFramesNumbersForEstablishedKeyframing(
                        positionTransformsCurves,
                        rotationTransformCurves,
                        scaleTransformCurves,
                        frameRate,
                        animationFrames))
            {
                float positionXValue = 
                    FloatCurvesTimelineHelper
                        .GetProperValueForCurve(
                            positionTransformsCurves.Item1,
                            frameNumberForKeyframe,
                            frameRate,
                            animationFrames);

                float positionYValue = 
                    FloatCurvesTimelineHelper
                        .GetProperValueForCurve(
                            positionTransformsCurves.Item2,
                            frameNumberForKeyframe,
                            frameRate,
                            animationFrames);

                float positionZValue = 
                    FloatCurvesTimelineHelper
                        .GetProperValueForCurve(
                            positionTransformsCurves.Item3,
                            frameNumberForKeyframe,
                            frameRate,
                            animationFrames);

                
                
                float rotationWValue =
                    FloatCurvesTimelineHelper
                        .GetProperValueForCurve(
                            rotationTransformCurves.Item1,
                            frameNumberForKeyframe,
                            frameRate,
                            animationFrames);

                float rotationXValue =
                    FloatCurvesTimelineHelper
                        .GetProperValueForCurve(
                            rotationTransformCurves.Item2,
                            frameNumberForKeyframe,
                            frameRate,
                            animationFrames);

                float rotationYValue =
                    FloatCurvesTimelineHelper
                        .GetProperValueForCurve(
                            rotationTransformCurves.Item3,
                            frameNumberForKeyframe,
                            frameRate,
                            animationFrames);

                float rotationZValue =
                    FloatCurvesTimelineHelper
                        .GetProperValueForCurve(
                            rotationTransformCurves.Item4,
                            frameNumberForKeyframe,
                            frameRate,
                            animationFrames);

                float scaleXValue =
                    FloatCurvesTimelineHelper
                        .GetProperValueForCurve(
                            scaleTransformCurves.Item1,
                            frameNumberForKeyframe,
                            frameRate,
                            animationFrames);

                float scaleYValue =
                    FloatCurvesTimelineHelper
                        .GetProperValueForCurve(
                            scaleTransformCurves.Item2,
                            frameNumberForKeyframe,
                            frameRate,
                            animationFrames);

                float scaleZValue =
                    FloatCurvesTimelineHelper
                        .GetProperValueForCurve(
                            scaleTransformCurves.Item3,
                            frameNumberForKeyframe,
                            frameRate,
                            animationFrames);

                int currentFrame = frameNumberForKeyframe;

                result.Add(Tuple.Create(
                    currentFrame,
                    new BoneKeyframeTransformBuilder()
                        .WithPosition(new Vector3(positionXValue, positionYValue, positionZValue))
                        .WithRotation(new Quaternion(rotationXValue, rotationYValue, rotationZValue, rotationWValue))
                        .WithScale(new Vector3(scaleXValue, scaleYValue, scaleZValue))
                        .Build()));
            }

            return result;
        }
    }

    public struct BoneKeyframeTransform
    {
        public Vector3 position;
        public Quaternion rotation;
        public Vector3 scale;

        public BoneKeyframeTransform ReflectViaWorldVector3RightNormal()
        {
            var result = new BoneKeyframeTransform();
            result.position = Vector3.Reflect(position, Vector3.right);

            result.rotation = new Quaternion(rotation.x, -rotation.y, -rotation.z, rotation.w);

            // should we mirror also scale?
            //result.scale = Vector3.Reflect(scale, Vector3.right);
            result.scale = scale;
            return result;
        }

        public BoneKeyframeTransform WithZeroScale()
        {
            var result = new BoneKeyframeTransform();
            result.position = position;
            result.rotation = rotation;
            result.scale = new Vector3(0f, 0f, 0f);
            return result;
        }
    }

    public class BoneTransTimeline
    {
        #if (UNITY_EDITOR)
        public Dictionary<int, BoneKeyframeTransform> keyframes =
            new Dictionary<int, BoneKeyframeTransform>();

        public static BoneTransTimeline
            WithPositionRotationScaleTransformsCurves(
            Tuple<AnimationCurve, AnimationCurve, AnimationCurve> positionTransformsCurves,
            Tuple<AnimationCurve, AnimationCurve, AnimationCurve, AnimationCurve> rotationTransformCurves,
            Tuple<AnimationCurve, AnimationCurve, AnimationCurve> scaleTransformCurves,
            float frameRate,
            int animationFrames)
        {
            var result = new BoneTransTimeline();

            foreach (var positionRotationScaleCombinedKeyframeTransformInfo in
                FloatCurvesKeyframesHelper.IterateCombinedKeyframeTransforms(
                    positionTransformsCurves, rotationTransformCurves,
                    scaleTransformCurves, frameRate, animationFrames))
            {
                int keyframeNumber = positionRotationScaleCombinedKeyframeTransformInfo.Item1;
                var boneKeyframeTransform = positionRotationScaleCombinedKeyframeTransformInfo.Item2;
                result.keyframes.Add(keyframeNumber, boneKeyframeTransform);
            }

            return result;
        }

        public List<Tuple<AnimationCurve, EditorCurveBinding>> 
            GetUnityAnimationCurvesWithBindingInfos(string path, int totalFrames)
        {
            var positionXAnimationCurveBuilder = 
                new AnimationCurveBuilder();
            var positionXEditorCurveBinding = 
                EditorCurveBinding.FloatCurve(path, typeof(Transform), "m_LocalPosition.x");

            var positionYAnimationCurveBuilder = 
                new AnimationCurveBuilder();
            var positionYEditorCurveBinding =
                EditorCurveBinding.FloatCurve(path, typeof(Transform), "m_LocalPosition.y");

            var positionZAnimationCurveBuilder = 
                new AnimationCurveBuilder();
            var positionZEditorCurveBinding =
                EditorCurveBinding.FloatCurve(path, typeof(Transform), "m_LocalPosition.z");


            var rotationWAnimationCurveBuilder = 
                new AnimationCurveBuilder();
            var rotationWEditorCurveBinding =
                EditorCurveBinding.FloatCurve(path, typeof(Transform), "m_LocalRotation.w");

            var rotationXAnimationCurveBuilder = 
                new AnimationCurveBuilder();
            var rotationXEditorCurveBinding =
                EditorCurveBinding.FloatCurve(path, typeof(Transform), "m_LocalRotation.x");

            var rotationYAnimationCurveBuilder = 
                new AnimationCurveBuilder();
            var rotationYEditorCurveBinding =
                EditorCurveBinding.FloatCurve(path, typeof(Transform), "m_LocalRotation.y");

            var rotationZAnimationCurveBuilder = 
                new AnimationCurveBuilder();
            var rotationZEditorCurveBinding =
                EditorCurveBinding.FloatCurve(path, typeof(Transform), "m_LocalRotation.z");


            var scaleXAnimationCurveBuilder = 
                new AnimationCurveBuilder();
            var scaleXEditorCurveBinding =
                EditorCurveBinding.FloatCurve(path, typeof(Transform), "m_LocalScale.x");

            var scaleYAnimationCurveBuilder = 
                new AnimationCurveBuilder();
            var scaleYEditorCurveBinding =
                EditorCurveBinding.FloatCurve(path, typeof(Transform), "m_LocalScale.y");

            var scaleZAnimationCurveBuilder = 
                new AnimationCurveBuilder();
            var scaleZEditorCurveBinding =
                EditorCurveBinding.FloatCurve(path, typeof(Transform), "m_LocalScale.z");


            foreach (var keyframeItem in keyframes)
            {
                int frameNumber = keyframeItem.Key;
                BoneKeyframeTransform boneKeyframeTransform = keyframeItem.Value;

                positionXAnimationCurveBuilder.AddValueForFrame(
                    frameNumber, totalFrames, boneKeyframeTransform.position.x);
                positionYAnimationCurveBuilder.AddValueForFrame(
                    frameNumber, totalFrames, boneKeyframeTransform.position.y);
                positionZAnimationCurveBuilder.AddValueForFrame(
                    frameNumber, totalFrames, boneKeyframeTransform.position.z);

                rotationWAnimationCurveBuilder.AddValueForFrame(
                    frameNumber, totalFrames, boneKeyframeTransform.rotation.w);
                rotationXAnimationCurveBuilder.AddValueForFrame(
                    frameNumber, totalFrames, boneKeyframeTransform.rotation.x);
                rotationYAnimationCurveBuilder.AddValueForFrame(
                    frameNumber, totalFrames, boneKeyframeTransform.rotation.y);
                rotationZAnimationCurveBuilder.AddValueForFrame(
                    frameNumber, totalFrames, boneKeyframeTransform.rotation.z);

                scaleXAnimationCurveBuilder.AddValueForFrame(
                    frameNumber, totalFrames, boneKeyframeTransform.scale.x);
                scaleYAnimationCurveBuilder.AddValueForFrame(
                    frameNumber, totalFrames, boneKeyframeTransform.scale.y);
                scaleZAnimationCurveBuilder.AddValueForFrame(
                    frameNumber, totalFrames, boneKeyframeTransform.scale.z);
            }

            return new List<Tuple<AnimationCurve, EditorCurveBinding>>()
            {
                Tuple.Create(positionXAnimationCurveBuilder.Build(), positionXEditorCurveBinding),
                Tuple.Create(positionYAnimationCurveBuilder.Build(), positionYEditorCurveBinding),
                Tuple.Create(positionZAnimationCurveBuilder.Build(), positionZEditorCurveBinding),

                Tuple.Create(rotationWAnimationCurveBuilder.Build(), rotationWEditorCurveBinding),
                Tuple.Create(rotationXAnimationCurveBuilder.Build(), rotationXEditorCurveBinding),
                Tuple.Create(rotationYAnimationCurveBuilder.Build(), rotationYEditorCurveBinding),
                Tuple.Create(rotationZAnimationCurveBuilder.Build(), rotationZEditorCurveBinding),

                Tuple.Create(scaleXAnimationCurveBuilder.Build(), scaleXEditorCurveBinding),
                Tuple.Create(scaleYAnimationCurveBuilder.Build(), scaleYEditorCurveBinding),
                Tuple.Create(scaleZAnimationCurveBuilder.Build(), scaleZEditorCurveBinding),
            };
        }

        public int GetFramesCount()
        {
            return keyframes.Keys.Max();
        }

        public BoneTransTimeline ReflectViaWorldVector3RightNormal()
        {
            var result = new BoneTransTimeline();

            foreach (var keyframeItem in keyframes)
            {
                result.keyframes.Add(
                    keyframeItem.Key,
                    keyframeItem.Value.ReflectViaWorldVector3RightNormal());
            }

            return result;
        }

        public BoneTransTimeline ZeroOutScaleKeyframes()
        {
            var result = new BoneTransTimeline();

            foreach (var keyframeItem in keyframes)
            {
                result.keyframes.Add(
                    keyframeItem.Key,
                    keyframeItem.Value.WithZeroScale());
            }

            return result;
        }
#endif
    }
}
