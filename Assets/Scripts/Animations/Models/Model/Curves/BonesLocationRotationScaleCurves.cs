﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Animations.Models.Model.Curves
{
    public class BoneKeyframeTransformBuilder
    {
        private BoneKeyframeTransform result = new BoneKeyframeTransform();

        public BoneKeyframeTransformBuilder WithPosition(Vector3 position)
        {
            result.position = position;
            return this;
        }

        public BoneKeyframeTransformBuilder WithRotation(Quaternion rotation)
        {
            result.rotation = rotation;
            return this;
        }

        public BoneKeyframeTransformBuilder WithScale(Vector3 scale)
        {
            result.scale = scale;
            return this;
        }

        public BoneKeyframeTransform Build()
        {
            return result;
        }
    }

    public class BonesLocationRotationScaleCurves
    {
        #if (UNITY_EDITOR)
        private Dictionary<string, BoneTransTimeline> bonesKeyframes
            = new Dictionary<string, BoneTransTimeline>();

        public BonesLocationRotationScaleCurves RemoveKeyframesAfterKeyframeNumber(
            int keyframeNumber)
        {
            //return this;
            var result = new BonesLocationRotationScaleCurves();
            foreach (var timelineItem in bonesKeyframes)
            {
                var newBoneTransTimeline = new BoneTransTimeline();
                newBoneTransTimeline.keyframes =
                    timelineItem.Value.keyframes
                    .Where(x => x.Key <= keyframeNumber)
                    .ToDictionary(x => x.Key, x => x.Value);
                result.bonesKeyframes.Add(
                    timelineItem.Key,
                    newBoneTransTimeline);
            }
            return result;
        }

        public static BonesLocationRotationScaleCurves 
            WithBonesKeyframesTimelines(
                Dictionary<string, BoneTransTimeline> bonesCurves)
        {
            var result = new BonesLocationRotationScaleCurves();
            result.bonesKeyframes = bonesCurves;
            return result;
        }

        public List<
            Tuple<string, EditorCurveBinding, AnimationCurve>>
            GetUnityAnimationCurvesWithEditorCurveBindings()
        {
            var result = new List<Tuple<string, EditorCurveBinding, AnimationCurve>>();
            int totalFrames = GetTotalFrames();
            foreach (var boneTimelineItem in bonesKeyframes)
            {
                string path = boneTimelineItem.Key;

                foreach (var unityAnimationCurveWithBindingInfo in 
                    boneTimelineItem.Value.GetUnityAnimationCurvesWithBindingInfos(path, totalFrames))
                {             
                    EditorCurveBinding editorCurveBinding = unityAnimationCurveWithBindingInfo.Item2;
                    AnimationCurve animationCurve = unityAnimationCurveWithBindingInfo.Item1;

                    result.Add(
                        Tuple.Create(
                            path,
                            editorCurveBinding,
                            animationCurve
                        ));
                }
            }

            return result;
        }

        public int GetTotalFrames()
        {
            return bonesKeyframes.First().Value.GetFramesCount();
        }

        public BonesLocationRotationScaleCurves ReflectViaWorldVector3RightNormal()
        {
            var result = new BonesLocationRotationScaleCurves();
            foreach (var boneTimelineItem in bonesKeyframes)
            {
                result.bonesKeyframes.Add(
                    boneTimelineItem.Key,
                    boneTimelineItem.Value.ReflectViaWorldVector3RightNormal());
            }

            return result;
        }

        public BonesLocationRotationScaleCurves MapPaths(Dictionary<string, string> pathsMappings)
        {
            var result = new BonesLocationRotationScaleCurves();
            foreach (var boneTimelineItem in bonesKeyframes)
            {
                var mappedBonePath = pathsMappings[boneTimelineItem.Key];
                result.bonesKeyframes.Add(mappedBonePath, boneTimelineItem.Value);
            }
            return result;
        }

        public BonesLocationRotationScaleCurves ZeroOutScaleKeyframes()
        {
            var result = new BonesLocationRotationScaleCurves();
            foreach (var boneTimelineItem in bonesKeyframes)
            {
                result.bonesKeyframes.Add(boneTimelineItem.Key,
                    boneTimelineItem.Value.ZeroOutScaleKeyframes());
            }
            return result;
        }
        #endif
    }
}
