﻿using Assets.Scripts.Animations.Models.Model;
using Assets.Scripts.Animations.Models.Rayman.Recipes;
using Assets.Scripts.EditorOnly.AssetsCreation.Animator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Animations.Models.Rayman
{
    public static class RaymanDerivedAnimationStates
    {
        #if (UNITY_EDITOR)
        public static List<AnimationClipConstructingRecipe> animationClipConstructingRecipes =
            new List<AnimationClipConstructingRecipe>() {

                // layer 1

                new AnimationClipConstructingRecipe(
                    "OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT|Animation 106",
                    new ShootingRightHandAnimationConstructingRecipeImpl(),
                    RaymanAnimations.EXTRA_ANIM_SHOOTING_RIGHT_HAND,
                    9f,
                    1),

                new AnimationClipConstructingRecipe(
                    "OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT|Animation 0",
                    new MissingRightHandAnimationConstructingRecipeImpl(),
                    RaymanAnimations.EXTRA_ANIM_MISSING_RIGHT_HAND,
                    1f,
                    1),

                new AnimationClipConstructingRecipe(
                    "OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT|Animation 0",
                    new NonOverridingAnimationConstructingRecipeImpl(),
                    RaymanAnimations.NON_OVERRIDING_ANIMATION_LAYER_1,
                    1f,
                    1),

                // layer 2

                new AnimationClipConstructingRecipe(
                    "OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT|Animation 106",
                    new ShootingLeftHandAnimationConstructingRecipeImpl(),
                    RaymanAnimations.EXTRA_ANIM_SHOOTING_LEFT_HAND,
                    9f,
                    2
                    ),

                new AnimationClipConstructingRecipe(
                    "OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT|Animation 0",
                    new MissingLeftHandAnimationConstructingRecipeImpl(),
                    RaymanAnimations.EXTRA_ANIM_MISSING_LEFT_HAND,
                    1f,
                    2),

                new AnimationClipConstructingRecipe(
                    "OPENSPACE_MODEL_ARMATURE_OBJECT_OBJECT|Animation 0",
                    new NonOverridingAnimationConstructingRecipeImpl(),
                    RaymanAnimations.NON_OVERRIDING_ANIMATION_LAYER_2,
                    1f,
                    2),
            };
        #endif
    }
}
