﻿using Assets.Scripts.Animations.Models.Model.Curves;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Animations.Models.Rayman.Recipes
{
    public class MissingLeftHandAnimationConstructingRecipeImpl : IAnimConstrRecipe
    {
        #if (UNITY_EDITOR)
        public AnimationClip GetDerivedAnimationClipAccordingToRecipe
            (Transform animatorTransform, List<Transform> animatorBonesTransforms,
            string newAnimationClipName, AnimationClip baseAnimationClip)
        {
            var result = new AnimationClip();
            result.name = newAnimationClipName;

            BonesLocationRotationScaleCurves leftHandAnimationCurves =
                RaymanAnimationClipsCurvesHelper
                .GetLeftHandBonesLocationRotationScaleCurvesForAnimationClip(
                    animatorTransform, animatorBonesTransforms, baseAnimationClip).ZeroOutScaleKeyframes();

            AnimationClipHelper.AddLocationRotationScaleCurvesForAnimationClip(leftHandAnimationCurves, result);

            return result;
        }
        #endif
    }
}
