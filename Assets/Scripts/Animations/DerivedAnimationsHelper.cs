﻿using Assets.Scripts.Animations.Models.Model;
using Assets.Scripts.EditorOnly.AssetsCreation.Animator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Animations
{
    public static class DerivedAnimationsHelper
    {
        #if (UNITY_EDITOR)
        public static List<AdditionalAnimationClipInfo>
            GetProgrammaticallyDerivedAndConstructedAdditionalAnimationClips(
            string resourcesModelPath,
            List<AnimationClipConstructingRecipe> animationClipConstructingRecipes,
            Transform animatorTransform, List<Transform> animatorBonesTransforms)
        {
            var baseAnimationClips = AnimatorFactory.GetBaseAnimationClipsAsImportedFromModel(resourcesModelPath);

            var result = new List<AdditionalAnimationClipInfo>();

            foreach (var animClipConstrRecipe in animationClipConstructingRecipes)
            {
                var appropriateAnimationClip =
                    baseAnimationClips.Where(x => x.name.Equals(animClipConstrRecipe.baseAnimationClipName)).First();
                result.Add(
                    animClipConstrRecipe
                        .GetDerivedAnimationClipAccordingToRecipe(
                        animatorTransform, animatorBonesTransforms, appropriateAnimationClip));
            }

            return result;
        }
        #endif
    }
}
