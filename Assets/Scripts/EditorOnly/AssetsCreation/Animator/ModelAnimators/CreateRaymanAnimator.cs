﻿using Assets.Scripts.Animations.Models;
using Assets.Scripts.Animations.Models.Rayman;
using Assets.Scripts.EditorOnly.AssetsCreation.Animator;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
#if (UNITY_EDITOR)
using UnityEditor.Animations;
#endif
using UnityEngine;

public class CreateRaymanAnimator : MonoBehaviour
{
    #if (UNITY_EDITOR)
    private AnimatorFactory animatorFactory = new AnimatorFactory();

    void Start()
    {
        var loopedAnimations = new List<string>(
            RaymanAnimations.animations.Values.Where(x => x.isLooped).Select(x => x.animationName));

        var additionalDerivedAnimationsConstructingRecipes = 
            RaymanDerivedAnimationStates.animationClipConstructingRecipes;

        Animator animator = GetComponent<Animator>();

        var actualProperArmatureRootBone =
            GetComponentsInChildren<Transform>().Where(x => x.name.Equals("ROOT_CHANNEL")).First();
        var animatorProperBones = actualProperArmatureRootBone
            .GetComponentsInChildren<Transform>()
            .Where(x => !x.name.Contains("_end")).ToList();

        Debug.Log("Creating Rayman animator");
        animatorFactory.CreateAnimatorFor(
            gameObject,
            "Models/Rayman/AdditionalAnims/",
            "Assets/Animators/Rayman/raymanAnimator.controller",
            "Models/Rayman/rayman3_with_helicopter_all_clips",
            RaymanAnimations.animations,
            additionalDerivedAnimationsConstructingRecipes,
            animator.transform,
            animatorProperBones);
    }
    #endif
}
