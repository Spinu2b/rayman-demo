﻿using Assets.Scripts.Animations;
using Assets.Scripts.Animations.Models.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#if (UNITY_EDITOR)
using UnityEditor;
using UnityEditor.Animations;
#endif
using UnityEngine;

namespace Assets.Scripts.EditorOnly.AssetsCreation.Animator
{
    public static class AnimatorLayersHelper
    {
        #if (UNITY_EDITOR)
        public static void AddOverrideLayerToAnimator(
            string layerName,
            int layerIndex,
            AnimatorController animatorController)
        {
            AnimatorControllerLayer newLayer = new AnimatorControllerLayer();
            newLayer.name = layerName;
            newLayer.stateMachine = new AnimatorStateMachine();
            newLayer.stateMachine.name = newLayer.name;
            newLayer.stateMachine.hideFlags = HideFlags.HideInHierarchy;
            if (AssetDatabase.GetAssetPath(animatorController) != "")
                AssetDatabase.AddObjectToAsset(
                    newLayer.stateMachine, AssetDatabase.GetAssetPath(animatorController));
            //Custom
            newLayer.blendingMode = AnimatorLayerBlendingMode.Override;
            newLayer.defaultWeight = 1f;

            animatorController.AddLayer(newLayer);

            ////we should have non-overriding curves animation state on any secondary animations layer
            ////we can always go to afterwards to enable animations on basic layer uderneath
            //// to play properly etc
            //var dummyEmptyAnimSlot = new AnimationClip();
            //dummyEmptyAnimSlot.legacy = false;
            //dummyEmptyAnimSlot.name = "DEFAULT_STATE_" + layerIndex;
            //var defaultAnimatorStateLayer1 = animatorController.AddMotion(dummyEmptyAnimSlot, layerIndex);

            //var anyStateTransition = newLayer.stateMachine.AddAnyStateTransition(defaultAnimatorStateLayer1);
            //anyStateTransition.hasExitTime = true;
            //anyStateTransition.hasFixedDuration = false;
            //anyStateTransition.duration = 1f;
            //anyStateTransition.exitTime = 1f;
        }
        

        public static List<int> GetRequiredAnimatorLayersInfos
            (List<AnimationClipConstructingRecipe> 
                additionalDerivedAnimationsConstructingRecipes)
        {
            return additionalDerivedAnimationsConstructingRecipes
                .Select(x => x.animationClipLayerIndex).Distinct().ToList();
        }
#endif
    }

    public class AnimatorFactory
    {
        public static List<AnimationClip> 
            GetBaseAnimationClipsAsImportedFromModel(
                string animatedModelResourcesPath)
        {
            return Resources.LoadAll(
                animatedModelResourcesPath, typeof(AnimationClip))
                    .Cast<AnimationClip>().ToList(); 
        }

        public void CreateAnimatorFor(
            GameObject gameObject,
            string additionalDerivedAnimationClipsResourceAssetsDirectoryPath,
            string animatorAssetPath,
            string animatedModelResourcesPath,
            Dictionary<string, Assets.Scripts.Animations.Models.AnimationInfo> basicAnimationsMetadata,
            List<AnimationClipConstructingRecipe> additionalDerivedAnimationsConstructingRecipes,
            Transform animatorTransform,
            List<Transform> animatorBonesTransforms)
        {
            Debug.Log("Invoking Animator Factory");
            #if (UNITY_EDITOR)
            var animatorController = UnityEditor.Animations.AnimatorController
                 .CreateAnimatorControllerAtPath(animatorAssetPath);

            var animationClips = GetBaseAnimationClipsAsImportedFromModel(animatedModelResourcesPath);

            foreach (var newLayerToAddIndex in AnimatorLayersHelper
                .GetRequiredAnimatorLayersInfos(additionalDerivedAnimationsConstructingRecipes))
            {
                string newLayerName = "SECONDARY_ANIMATIONS_LAYER_" + newLayerToAddIndex;
                AnimatorLayersHelper.AddOverrideLayerToAnimator(
                    newLayerName, newLayerToAddIndex, animatorController);
            }

            var loopedAnimations = basicAnimationsMetadata
                .Where(x => x.Value.isLooped)
                .Select(x => x.Value.animationName).ToList();

            foreach (var animationClip in animationClips)
            {
                var animatorState = animatorController.AddMotion(animationClip, 0);

                if (basicAnimationsMetadata.Where(
                    x => x.Value.animationName.Equals(animationClip.name)).Count() > 0)
                {
                    var speedDedicatedToThisAnimation = basicAnimationsMetadata.Where(
                    x => x.Value.animationName.Equals(animationClip.name)).First().Value.playingSpeed;
                    animatorState.speed = speedDedicatedToThisAnimation;
                }                
                // animatorState.speed = 1f; // hopefully we should be able to set speed individually for all
                // animator states/motions separately and that should affect animator out of the box
                // without any additional steps

                if (loopedAnimations.Contains(animationClip.name)) {
                    var animationClipSettings = AnimationUtility.GetAnimationClipSettings(animationClip);
                    animationClip.wrapMode = WrapMode.Loop;
                    
                    animationClipSettings.loopTime = true;
                    
                    AnimationUtility.SetAnimationClipSettings(animationClip, animationClipSettings);
                }
            }

            var additionalAnimationClipsWithLayersToBeOverriddenDuringRuntime =
                DerivedAnimationsHelper.GetProgrammaticallyDerivedAndConstructedAdditionalAnimationClips
                    (animatedModelResourcesPath,
                        additionalDerivedAnimationsConstructingRecipes,
                        animatorTransform, animatorBonesTransforms);

            foreach (var additionalAnimClipInfo in additionalAnimationClipsWithLayersToBeOverriddenDuringRuntime)
            {
                AssetDatabase.CreateAsset(
                    additionalAnimClipInfo.animationClip,
                    "Assets/Resources/" + additionalDerivedAnimationClipsResourceAssetsDirectoryPath
                    + additionalAnimClipInfo.animationClip.name + ".anim");
            }
            AssetDatabase.Refresh();
            AssetDatabase.SaveAssets();

            foreach (var additionalAnimClipInfo in additionalAnimationClipsWithLayersToBeOverriddenDuringRuntime)
            {
                var additionalAnimName = additionalAnimClipInfo.animationClip.name;
                var additionalAnimLayer = additionalAnimClipInfo.animationClipLayerIndex;

                var animClipResourcesAssetPath = 
                    additionalDerivedAnimationClipsResourceAssetsDirectoryPath
                    + additionalAnimClipInfo.animationClip.name;

                var animationClipAsLoadedFromAssets =
                    ((AnimationClip)Resources.Load(animClipResourcesAssetPath, typeof(AnimationClip)));

                var additionalAnimatorState = animatorController.AddMotion(
                    animationClipAsLoadedFromAssets, additionalAnimLayer);

                additionalAnimatorState.speed = additionalAnimClipInfo.playingSpeed;
                animatorController.SetStateEffectiveMotion(
                    additionalAnimatorState, animationClipAsLoadedFromAssets);
            }
            AssetDatabase.Refresh();
            AssetDatabase.SaveAssets();
            #endif
        }
    }
}
