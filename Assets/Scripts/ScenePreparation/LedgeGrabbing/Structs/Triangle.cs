﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.ScenePreparation.LedgeGrabbing.Structs
{
    public static class EdgeHelper
    {
        public static Vector3 CalculateLedgeGrabNormal(Vector3 normalA, Vector3 normalB)
        {
            return Vector3.ProjectOnPlane((normalA + normalB) / 2.0f, Vector3.up).normalized;
        }
    }

    public struct Triangle
    {
        public Vector3 pointA;
        public Vector3 pointB;
        public Vector3 pointC;

        public int vertexAIndex;
        public int vertexBIndex;
        public int vertexCIndex;

        public Vector3 normalA;
        public Vector3 normalB;
        public Vector3 normalC;

        public Vector3 faceNormalWorldSpace;

        public Triangle(
            Vector3 pointA,
            Vector3 pointB,
            Vector3 pointC,
            int vertexAIndex,
            int vertexBIndex,
            int vertexCIndex,
            Vector3 normalA,
            Vector3 normalB,
            Vector3 normalC,
            Vector3 faceNormalWorldSpace)
        {
            this.pointA = pointA;
            this.pointB = pointB;
            this.pointC = pointC;
            this.vertexAIndex = vertexAIndex;
            this.vertexBIndex = vertexBIndex;
            this.vertexCIndex = vertexCIndex;
            this.normalA = normalA;
            this.normalB = normalB;
            this.normalC = normalC;
            this.faceNormalWorldSpace = faceNormalWorldSpace;
        }

        public List<EdgeWithLedgeColliderNormalInfo>
            GetEdgesWithLedgeColliderNormalInfo()
        {
            var edgeA = 
                new EdgeWithLedgeColliderNormalInfo(
                    pointA,
                    pointB,
                    EdgeHelper.CalculateLedgeGrabNormal(normalA, normalB),
                    normalA,
                    normalB,
                    vertexAIndex,
                    vertexBIndex
                    );

            var edgeB =
               new EdgeWithLedgeColliderNormalInfo(
                   pointB,
                   pointC,
                   EdgeHelper.CalculateLedgeGrabNormal(normalB, normalC),
                   normalB,
                   normalC,
                   vertexBIndex,
                   vertexCIndex
                   );

            var edgeC =
               new EdgeWithLedgeColliderNormalInfo(
                   pointC,
                   pointA,
                   EdgeHelper.CalculateLedgeGrabNormal(normalC, normalA),
                   normalC,
                   normalA,
                   vertexCIndex,
                   vertexAIndex
                   );

            return new List<EdgeWithLedgeColliderNormalInfo>() { edgeA, edgeB, edgeC };
        }
    }
}
