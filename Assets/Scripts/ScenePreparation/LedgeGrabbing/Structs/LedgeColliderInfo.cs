﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.ScenePreparation.LedgeGrabbing.Structs
{
    public class LedgeColliderInfo : MonoBehaviour
    {
        public Vector3 normal = new Vector3(0.0f, 0.0f, 0.0f);
        public Vector3 edgePointA = new Vector3(0, 0, 0);
        public Vector3 edgePointB = new Vector3(0, 0, 0);
    }
}
