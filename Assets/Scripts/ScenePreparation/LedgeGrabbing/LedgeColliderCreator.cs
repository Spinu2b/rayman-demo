﻿using Assets.Scripts.Common;
using Assets.Scripts.ScenePreparation.LedgeGrabbing.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.ScenePreparation.LedgeGrabbing
{
    public static class LedgeColliderCreator
    {
        public static void CreateGrabLedgeCollider(Vector3 pointA, Vector3 pointB, Vector3 ledgeGrabNormalWorldTransform)
        {
            GameObject ledgeColliderGameObject = 
                StretchedBoxesConstructingHelper
                    .InstantiateStretchedBoxBetweenPoints(pointA, pointB, depth: 0.05f, height: 0.5f);
            var info = ledgeColliderGameObject.AddComponent<LedgeColliderInfo>();
            info.normal = ledgeGrabNormalWorldTransform;
            info.edgePointA = pointA;
            info.edgePointB = pointB;
            ledgeColliderGameObject.GetComponent<MeshRenderer>().enabled = false;
            ledgeColliderGameObject.layer = Layers.ledgeCollidersLayerIndex;
            ledgeColliderGameObject.tag = Tags.ledgeColliderTag;
        }
    }
}
