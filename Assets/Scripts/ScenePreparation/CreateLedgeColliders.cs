﻿using Assets.Scripts.Common;
using Assets.Scripts.ScenePreparation.LedgeGrabbing;
using Assets.Scripts.ScenePreparation.LedgeGrabbing.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.ScenePreparation
{
    public class CreateLedgeColliders : MonoBehaviour
    {
        private void Start()
        {
            GameObject[] ledgeGrabbableObjects = GameObject.FindGameObjectsWithTag(Tags.ledgeGrabbableTag);
            Debug.Log("Found " + ledgeGrabbableObjects.Length);

            foreach (var ledgeGrabbableObj in ledgeGrabbableObjects)
            {
                InstantiateLedgeColliders(ledgeGrabbableObj);
            }
        }

        private void InstantiateLedgeColliders(GameObject ledgeGrabbableObj)
        {
            Mesh mesh = ledgeGrabbableObj.GetComponent<MeshFilter>().mesh;
            var vertices = mesh.vertices;
            var verticesInWorldTransform = 
                vertices.Select(x => ledgeGrabbableObj.transform.TransformPoint(x)).ToArray();

            var rotatedNormals = mesh.normals.Select(x => ledgeGrabbableObj.transform.TransformDirection(x)).ToArray();

            Debug.Log("TRIANGLES: " + mesh.triangles.Length);

            List<Triangle> triangles = 
                MeshHelper.GetFacesInWorldSpaceWithNormals(mesh.triangles,
                    verticesInWorldTransform, rotatedNormals);

            List<Triangle> appropriateFacesForLedgeColliders =
                LedgeColliderEdgesHelper.GetFacesWithRightAngleNormalToHorizontal(ledgeGrabbableObj, triangles);

            List<EdgeWithLedgeColliderNormalInfo> edgesWithLedgeGrabNormals =
                LedgeColliderEdgesHelper.GetEdgesWithLedgeGrabNormals(ledgeGrabbableObj, appropriateFacesForLedgeColliders);

            List<EdgeWithLedgeColliderNormalInfo> edgesEligibleForLedgeColliders =
                LedgeColliderEdgesHelper.GetUniqueEdges(edgesWithLedgeGrabNormals);

            foreach (var edge in edgesEligibleForLedgeColliders)
            {
                LedgeColliderCreator.CreateGrabLedgeCollider(
                    edge.pointA, edge.pointB, edge.ledgeGrabNormalWorldTransform);
            }
        }
    }
}
