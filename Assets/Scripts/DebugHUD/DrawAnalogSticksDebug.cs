﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.DebugHUD
{
    public static class AnalogCoordsHelper
    {
        public static Tuple<float, float> GetCoords(
            float originX, float originY,
            float areaRadius,
            float analogStickWidth, float analogStickHeight,
            float horizontalInputValue, float verticalInputValue)
        {
            //float centerX = originX + areaWidth / 2f;
            //float centerY = originY + areaHeight / 2f;

            float mappedXOffset = horizontalInputValue * (areaRadius);
            float mappedYOffset = -(verticalInputValue * (areaRadius));

            Vector2 offset = new Vector2(mappedXOffset, mappedYOffset);
            offset = Vector2.ClampMagnitude(offset, areaRadius);
           

            float x = (originX + offset.x) - analogStickWidth / 2f;
            float y = (originY + offset.y) - analogStickHeight / 2f;

            
            return Tuple.Create(x, y);
        }
    }

    public class DrawAnalogSticksDebug : MonoBehaviour
    {
        private float positionX;
        private float positionY;

        private float marginRight = 300;
        private float marginTop = 300;

        public Texture2D analogStickAreaTexture;
        public Texture2D circleTexture;

        //private float width = 100;
        //private float height = 100;

        private float analogStickWidth = 60f;
        private float analogStickHeight = 60f;

        private float areaRadius = 100f;
        private float areaCenterX;
        private float areaCenterY;

        float horizontalInputValue;
        float verticalInputValue;

        private void Awake()
        {
            positionX = Screen.width - (2*areaRadius) - marginRight;
            positionY = 50 + marginTop;

            areaCenterX = positionX + areaRadius;
            areaCenterY = positionY + areaRadius;
        }

        private void Update()
        {
            areaCenterX = positionX + areaRadius;
            areaCenterY = positionY + areaRadius;

            horizontalInputValue = Input.GetAxisRaw("Horizontal");
            verticalInputValue = Input.GetAxisRaw("Vertical");
        }

        private void OnGUI()
        {
            var analogStickCoords = AnalogCoordsHelper.GetCoords(
                areaCenterX, areaCenterY, areaRadius, analogStickWidth, analogStickHeight,
                horizontalInputValue, verticalInputValue);

            var analogStickAreaRect = new Rect(positionX, positionY, 2*areaRadius, 2*areaRadius);
            var analogStickRect = new Rect(analogStickCoords.Item1, analogStickCoords.Item2, analogStickWidth, analogStickHeight);
            GUI.DrawTexture(analogStickAreaRect, analogStickAreaTexture);
            GUI.DrawTexture(analogStickRect, circleTexture);
        }
    }
}
