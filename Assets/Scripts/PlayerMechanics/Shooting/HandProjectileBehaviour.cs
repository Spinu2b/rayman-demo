﻿using Assets.Scripts.Common;
using Assets.Scripts.PlayerMechanics.Aspects;
using Assets.Scripts.PlayerMechanics.Collisions;
using Assets.Scripts.PlayerMechanics.Targeting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Shooting
{
    public static class MathParabola
    {
        public static Vector3 Parabola(Vector3 start, Vector3 end, float height, float t)
        {
            t = Mathf.Clamp01(t);

            Func<float, float> f = x => -4 * height * x * x + 4 * height * x;

            var mid = Vector3.Lerp(start, end, t);

            return new Vector3(mid.x, f(t) + Mathf.Lerp(start.y, end.y, t), mid.z);
        }

        public static float GetParabolaArcLength(Vector3 start, Vector3 end, float height)
        {
            // https://www.vcalc.com/wiki/vCalc/Parabola+-+arc+length
            float b = Vector3.Distance(start, end);
            float a = height;

            float b2 = b * b;
            float a2 = a * a;
            return (0.5f * Mathf.Sqrt(b2 + 16 * a2)) + (b2 / (8 * a)) * Mathf.Log(((4 * a) + Mathf.Sqrt(b2 + 16 * a2)) / b);
        }
    }

    public static class PointRotationHelper
    {
        public static Vector3 RotatePointAroundAxis(Vector3 point, float angle, Vector3 axisOrigin, Vector3 axis)
        {
            Vector3 translatedPoint = point - axisOrigin;

            Quaternion q = Quaternion.AngleAxis(angle, axis.normalized);
            return (q * translatedPoint) + axisOrigin; //Note: q must be first (point * q wouldn't compile)
        }
    }

    public static class TrajectoryHelper
    {
        public static Vector3 RotateTowardsVector
            (Vector3 initialVector,
             Vector3 rotationBiasVector,
             float rotationProgress)
        {
            bool isPlusWhenAddingRotation = 
                Vector3.SignedAngle(initialVector, rotationBiasVector, Vector3.up)
                    >= 0f;

            return Quaternion.AngleAxis(
                isPlusWhenAddingRotation ? (rotationProgress * 180f) 
                : -(rotationProgress * 180f), Vector3.up) * initialVector;
        }

        public static Vector3 RotateTowardsVectorWithTarget
            (Vector3 currentFlyingDirection,
            Vector3 startProjectilePosition,
            Vector3 rotationBiasVector,
            Vector3 currentProjectilePosition,
            Vector3 targetPosition,
            float trajectoryProgression)
        {
            float trajectoryParabolaHeight = Vector3.Distance(startProjectilePosition, targetPosition);
            Vector3 pointOnParabola = MathParabola.Parabola(
                startProjectilePosition, targetPosition, trajectoryParabolaHeight, trajectoryProgression);
            Vector3 parabolaPointRotationAxis = (targetPosition - startProjectilePosition).normalized;

            bool isPlusWhenAddingRotation =
                Vector3.SignedAngle(currentFlyingDirection, rotationBiasVector, Vector3.up)
                    >= 0f;

            //Vector3 rotatedParabolaPoint = Quaternion.AngleAxis(90f, parabolaPointRotationAxis) * pointOnParabola;
            Vector3 rotatedParabolaPoint = PointRotationHelper.RotatePointAroundAxis(
                pointOnParabola, isPlusWhenAddingRotation ? 90f : -90f,
                startProjectilePosition,
                parabolaPointRotationAxis);

            //Vector3 rotatedParabolaPoint = pointOnParabola;
            return rotatedParabolaPoint;
        }

        public static float GetParabolaTrajectoryProgressionStep(
            float speed, Vector3 startPosition, Vector3 endPosition)
        {
            float trajectoryParabolaHeight = Vector3.Distance(startPosition, endPosition);
            float parabolaArcLength = MathParabola
                .GetParabolaArcLength(startPosition, endPosition, trajectoryParabolaHeight);

            float parts = parabolaArcLength / speed;
            return 1f / parts;
        }
    }

    public class HandProjectileBehaviour : MonoBehaviour
    {
        public Vector3 flyingDirection;
        public Vector3 initialPlayerRightVector;
        public Transform animatedPlayerPartTransform;
        public Vector3 currentCurvedPathFlyingDirection;

        public Vector3 startPosition;

        public GameObject target;

        public bool isFlyingInCurvedPath = false;

        private float flyingSpeed = 1.5f;
        private float returnSpeed = 4f;

        private float trajectoryRotationProgress = 0.0f;
        private float trajectoryRotationProgressingSpeed = 0.03f;
        private float trajectoryRotationMaxProgressingSpeed = 0.09f;

        private float flyingStartTime;
        private float returnToPlayerTimeThresholdStartSeconds = 0.5f;

        private float proximityRadiusToReturnHandToPlayerEventually = 6f;
        private float proximityRadiusForHittingTheTarget = 1f;

        private bool isFlyingAway = true;
        public bool hasToGoBackImmediately = false;

        public float chargePower = 0f;

        public PlayerShootingAspect playerShootingAspect;
        public bool isRightHand;

        private void Start()
        {
            flyingStartTime = Time.time;
            isFlyingAway = true;
        }

        private void FixedUpdate()
        {
            if ((hasToGoBackImmediately && isFlyingAway) 
                || (isFlyingAway && HasHitTheSolidObstacle()))
            {
                isFlyingAway = false;
                return;
            }

            float currentTime = Time.time;
            if (currentTime - flyingStartTime > returnToPlayerTimeThresholdStartSeconds
                && isFlyingAway && !target)
            {
                isFlyingAway = false;
                GetComponent<TrailRenderer>().enabled = false;
                return;
            } else if (isFlyingAway && target && HasHitTheTarget())
            {
                isFlyingAway = false;
                GetComponent<TrailRenderer>().enabled = false;

                target.GetComponent<TargetHitHandlingBeh>().OnTargetHit(this);
                return;
            }

            if (isFlyingAway)
            {
                if (!isFlyingInCurvedPath)
                {
                    if (target)
                    {
                        flyingDirection = (target.transform.position - transform.position).normalized;
                    }

                    transform.position = transform.position + (flyingDirection.normalized * flyingSpeed);
                    transform.forward = flyingDirection.normalized;             
                }
                else
                {
                    //if (target)
                    //{
                    //    flyingDirection = (target.transform.position - transform.position).normalized;
                    //}

                    Vector3 translation = new Vector3();
                    Vector3 positionOnTrajectory = new Vector3();

                    if (target == null)
                    {
                        translation = TrajectoryHelper.RotateTowardsVector
                            (currentCurvedPathFlyingDirection.normalized, flyingDirection.normalized,
                            trajectoryRotationProgress).normalized;
                    } else
                    {
                        if ((target.transform.position - transform.position).magnitude > 0.0001f)
                        {
                            flyingDirection = (target.transform.position - transform.position).normalized;
                        }
                        trajectoryRotationProgressingSpeed =
                            0.85f * 
                            TrajectoryHelper
                                .GetParabolaTrajectoryProgressionStep(
                                    flyingSpeed, startPosition, target.transform.position);

                        positionOnTrajectory = TrajectoryHelper.RotateTowardsVectorWithTarget
                            (currentCurvedPathFlyingDirection.normalized,
                            startPosition,
                            flyingDirection.normalized,
                            transform.position,
                            target.transform.position,
                            trajectoryRotationProgress);
                        //currentCurvedPathFlyingDirection = translation.normalized;
                    }

                    //Vector3 translation = initialCurvedPathFlyingDirection;

                    if (target == null)
                    {
                        transform.position = transform.position + translation.normalized * flyingSpeed;                       
                    } else
                    {
                        transform.position = positionOnTrajectory;
                    }                   
                    transform.forward = translation.normalized;
                    trajectoryRotationProgress += trajectoryRotationProgressingSpeed;
                }
                return;
            }
            else
            {
                GoTowardsPlayer();

                if ((transform.position -
                    (animatedPlayerPartTransform.transform.position + Vector3.up)).magnitude <
                    proximityRadiusToReturnHandToPlayerEventually)
                {
                    if (isRightHand)
                    {
                        playerShootingAspect.RegainRightHand();
                    }
                    else
                    {
                        playerShootingAspect.RegainLeftHand();
                    }
                    //enabled = false;
                    //GetComponentInChildren<MeshRenderer>().enabled = false;
                    Destroy(gameObject);
                    return;
                }

                return;
            }
        }

        private bool HasHitTheSolidObstacle()
        {
            var hit = new RaycastHit();
            return RayColliderRules.HasHitTheSolidObstacle(transform, out hit) && 
                !hit.transform.tag.Equals(Tags.pigPotCrystalsTag) && 
                !hit.transform.tag.Equals(Tags.targetablePartTag);
        }

        private bool HasHitTheTarget()
        {
            return Vector3.Distance(transform.position, target.transform.position) 
                <= proximityRadiusForHittingTheTarget;
        }

        private void GoTowardsPlayer()
        {
            transform.LookAt(animatedPlayerPartTransform.position + Vector3.up);
            transform.Translate(Vector3.forward * returnSpeed);
        }
    }
}
