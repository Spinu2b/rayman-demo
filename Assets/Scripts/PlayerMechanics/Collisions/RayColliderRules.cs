﻿using Assets.Scripts.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Collisions
{
    public static class RayColliderRules
    {
        public static bool IsLegitSolidGroundOrWallForCollision(RaycastHit hit)
        {
            return !hit.collider.gameObject.tag.Equals(Tags.gemsTag) &&
                !hit.collider.gameObject.tag.Equals(Tags.projectilesTag);
        }

        public static bool IsHittingFlatGround(
            Transform transform, out RaycastHit hit, float maxGroundRaycastDistance=1.2f, bool debug=false)
        {
            if (debug)
            {
                Debug.DrawRay(transform.position + Vector3.up, Vector3.down * maxGroundRaycastDistance, Color.green);
            }
            return Physics.Raycast(transform.position + Vector3.up, Vector3.down, out hit, maxGroundRaycastDistance) &&
                IsLegitSolidGroundOrWallForCollision(hit);
        }

        public static bool IsHittingSlope(
            Transform transform, out RaycastHit hit, float maxGroundSlopeRaycastDistance=1.5f, bool debug=false)
        {
            if (debug)
            {
                Debug.DrawRay(transform.position + Vector3.up, Vector3.down * maxGroundSlopeRaycastDistance, Color.blue);
            }

            float angleThresholdDegrees = 1;
            if (Physics.Raycast(transform.position + Vector3.up, Vector3.down, out hit, maxGroundSlopeRaycastDistance))
            {
                return (Vector3.Angle(Vector3.up, hit.normal) > angleThresholdDegrees) &&
                    IsLegitSolidGroundOrWallForCollision(hit);
            }
            return false;
        }

        public static bool IsHittingTheWall(
            Transform transform, out RaycastHit hit,
            float circleWallCollisionCheckingRadius=0.8f)
        {
            int layers = 10;
            int raysInLayer = 20;
            float layersIntervalHeight = 0.1f;
            float startLayersOffset = 0.6f;

            for (int i = 0; i < layers; i++)
            {
                if (PhysicsHelper.CastRaysInCircleVertical(
                transform.position + Vector3.up * startLayersOffset + Vector3.up * layersIntervalHeight * i,
                Vector3.forward,
                circleWallCollisionCheckingRadius,
                raysInLayer,
                out hit))
                {
                    if (Mathf.Abs(Vector3.Dot(Vector3.up, hit.normal)) < 0.5 &&
                        IsLegitSolidGroundOrWallForCollision(hit))
                    {
                        hit.normal = new Vector3(hit.normal.x, 0, hit.normal.z).normalized;
                        return true;
                    }
                }
            }
            hit = new RaycastHit();
            return false;
        }

        public static bool HasHitTheSolidObstacle(Transform transform, out RaycastHit hit)
        {
            return IsHittingTheWall(transform, out hit) 
                || IsHittingSlope(transform, out hit) 
                || IsHittingFlatGround(transform, out hit);
        }

        public static bool HasHitTheSolidObstacle(Transform transform)
        {
            var hit = new RaycastHit();
            return HasHitTheSolidObstacle(transform, out hit);
        }
    }
}
