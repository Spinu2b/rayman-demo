﻿using Assets.Scripts.Animations;
using Assets.Scripts.Common;
using Assets.Scripts.PlayerMechanics.Collisions;
using Assets.Scripts.PlayerMechanics.Shooting;
using Assets.Scripts.ScenePreparation.LedgeGrabbing.Structs;
using Assets.Scripts.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics
{
    public class RayCollider : MonoBehaviour
    {
        private static float maxGroundRaycastDistance = 1.2f;
        private static float maxGroundSlopeRaycastDistance = 1.5f;

        private float circleWallCollisionCheckingRadius = 0.8f;

        private RaycastHit emptyRaycastHit = new RaycastHit();

        private bool debug = true;
        private bool isLedgeGrabColliderCollisionDetectionEnabled = true;
        private bool isWallClimbingCollisionDetectionEnabled = true;
        private bool isRoofHangingCollisionDetectionEnabled = true;

        private void Awake()
        {
            
        }

        public void AlignOnTopOfTheGround()
        {
            RaycastHit hit;
            if (IsHittingGround(out hit))
            {
                transform.position = new Vector3(transform.position.x, hit.point.y, transform.position.z);
            }             
        }

        public bool IsHittingGround()
        {
            return IsHittingGround(out emptyRaycastHit);
        }

        public bool HasHitTheSolidObstacle()
        {
            return RayColliderRules.HasHitTheSolidObstacle(transform);
        }

        private bool IsHittingGround(out RaycastHit hit) {
            return IsHittingFlatGround(out hit) || IsHittingSlope(out hit);
        }

        public bool IsHittingFlatGround()
        {
            return IsHittingFlatGround(out emptyRaycastHit);
        }

        public bool IsHittingFlatGround(out RaycastHit hit)
        {
            return RayColliderRules.IsHittingFlatGround(transform, out hit, maxGroundRaycastDistance, debug);
            //if (debug)
            //{
            //    Debug.DrawRay(transform.position + Vector3.up, Vector3.down * maxGroundRaycastDistance, Color.green);
            //}
            //return Physics.Raycast(transform.position + Vector3.up, Vector3.down, out hit, maxGroundRaycastDistance) &&
            //    IsLegitSolidGroundOrWallForCollision(hit);
        }

        private bool IsHittingSlope(out RaycastHit hit)
        {
            return RayColliderRules.IsHittingSlope(transform, out hit, maxGroundSlopeRaycastDistance, debug);
            //if (debug)
            //{
            //    Debug.DrawRay(transform.position + Vector3.up, Vector3.down * maxGroundSlopeRaycastDistance, Color.blue);
            //}
            
            //float angleThresholdDegrees = 1;
            //if (Physics.Raycast(transform.position + Vector3.up, Vector3.down, out hit, maxGroundSlopeRaycastDistance))
            //{
            //    return (Vector3.Angle(Vector3.up, hit.normal) > angleThresholdDegrees) &&
            //        IsLegitSolidGroundOrWallForCollision(hit);        
            //}
            //return false;
        }

        private void OnDrawGizmos()
        {
            //var oldColor = Gizmos.color;
            //Gizmos.color = Color.red;
            //Gizmos.DrawWireSphere(
            //    transform.position + Vector3.up * 1.5f,
            //     sphereWallCollisionCheckingRadius);
            //Gizmos.color = oldColor;
        }

        public Tuple<Vector3, List<RaycastHit>> GetAveragedNormalFromAllTheWallsTheCollisionOccurs()
        {
            int layers = 10;
            int raysInLayer = 60;
            float layersIntervalHeight = 0.1f;
            float startLayersOffset = 0.6f;

            List<RaycastHit> hits = new List<RaycastHit>();
            List<Vector3> resultWallsNormals = new List<Vector3>();

            List<RaycastHit> resultHits = new List<RaycastHit>();

            for (int i = 0; i < layers; i++)
            {
                if (PhysicsHelper.CastRaysInCircleVerticalListPoints(
                transform.position + Vector3.up * startLayersOffset + Vector3.up * layersIntervalHeight * i,
                Vector3.forward,
                circleWallCollisionCheckingRadius,
                raysInLayer,
                out hits))
                {
                    for (int j = 0; j < hits.Count; j++)
                    {
                        RaycastHit hit = hits[j];
                        if (Mathf.Abs(Vector3.Dot(Vector3.up, hit.normal)) < 0.5)
                        {
                            hit.normal =
                                new Vector3(hit.normal.x, 0, hit.normal.z).normalized;
                            if (!Vectors3Helper.VectorsListContainsVectorWithSameDirection(hit.normal, resultWallsNormals))
                            {
                                resultWallsNormals.Add(hit.normal);
                                resultHits.Add(hit);
                            }
                        }
                    }
                }
            }
            Vector3 resultNormal =
                new Vector3(
                    resultWallsNormals.Average(x => x.x),
                    resultWallsNormals.Average(x => x.y),
                    resultWallsNormals.Average(x => x.z));
            return Tuple.Create(resultNormal, resultHits);
        }

        public bool IsHittingTheWall()
        {
            return IsHittingTheWall(out emptyRaycastHit);
        }

        public bool IsHittingRoofHanging(out RaycastHit roofHangingHit)
        {
            if (isRoofHangingCollisionDetectionEnabled)
            {
                float ceilingRoofHangingCheckRayLength = 2.0f;
                if (Physics.Raycast(transform.position + Vector3.up, Vector3.up,
                    out roofHangingHit, ceilingRoofHangingCheckRayLength))
                {
                    if (roofHangingHit.collider.gameObject.tag.Equals(Tags.roofHangingTag))
                    {
                        roofHangingHit.normal = Vector3.Project(roofHangingHit.normal, Vector3.up).normalized;
                        return true;
                    }
                }
            }
            roofHangingHit = new RaycastHit();
            return false;
        }

        public Vector3 AdjustVelocityIfIsHittingTheCeiling(Vector3 velocityInWorldSpace)
        {
            if (isRoofHangingCollisionDetectionEnabled)
            {
                RaycastHit ceilingHit = new RaycastHit();
                float ceilingCollisionCheckRayLength = 2.0f;
                if (debug)
                {
                    Debug.DrawRay(transform.position + Vector3.up, Vector3.up * ceilingCollisionCheckRayLength);
                }

                if (Physics.Raycast(transform.position + Vector3.up, Vector3.up,
                    out ceilingHit, ceilingCollisionCheckRayLength) && 
                    IsLegitSolidGroundOrWallForCollision(ceilingHit))
                {
                    return Vector3.ProjectOnPlane(velocityInWorldSpace, ceilingHit.normal);
                }
            }            
            return velocityInWorldSpace;
        }

        public bool IsHittingTheWall(out RaycastHit hit)
        {
            return RayColliderRules.IsHittingTheWall(transform, out hit, circleWallCollisionCheckingRadius);
            //int layers = 10;
            //int raysInLayer = 20;
            //float layersIntervalHeight = 0.1f;
            //float startLayersOffset = 0.6f;

            //for (int i = 0; i < layers; i++)
            //{
            //    if (PhysicsHelper.CastRaysInCircleVertical(
            //    transform.position + Vector3.up * startLayersOffset + Vector3.up * layersIntervalHeight * i,
            //    Vector3.forward,
            //    circleWallCollisionCheckingRadius,
            //    raysInLayer,
            //    out hit))
            //    {
            //        if (Mathf.Abs(Vector3.Dot(Vector3.up, hit.normal)) < 0.5 && 
            //            IsLegitSolidGroundOrWallForCollision(hit))
            //        {
            //            hit.normal = new Vector3(hit.normal.x, 0, hit.normal.z).normalized;
            //            return true;
            //        }
            //    }
            //}
            //hit = new RaycastHit();
            //return false;
        }

        public bool IsCollidingWithWallClimbObject(out RaycastHit wallClimbHit)
        {
            if (isWallClimbingCollisionDetectionEnabled)
            {
                int layers = 10;
                int raysInLayer = 20;
                float layersIntervalHeight = 0.1f;
                float startLayersOffset = 0.6f;

                for (int i = 0; i < layers; i++)
                {
                    if (PhysicsHelper.CastRaysInCircleVertical(
                    transform.position + Vector3.up * startLayersOffset + Vector3.up * layersIntervalHeight * i,
                    Vector3.forward,
                    circleWallCollisionCheckingRadius,
                    raysInLayer,
                    out wallClimbHit))
                    {
                        if (wallClimbHit.collider.gameObject.tag.Equals(Tags.wallClimbTag))
                        {
                            return true;
                        }
                    }
                }
            }            
            wallClimbHit = new RaycastHit();
            return false;
        }

        public List<RaycastHit> GetAllCollectibleCrystalsThePlayerCollidesWith()
        {
            float startLayersOffset = 0f;
            int layers = 5;
            int raysInLayer = 20;
            float layersIntervalHeight = 0.3f;
            float circleGemsCollisionCheckingRadius = 2f;

            Func<RaycastHit, bool> colliderEndingFilterCriteria = (hit) => hit.collider.CompareTag(Tags.gemsTag)
                && hit.collider.gameObject.GetComponent<CollectibleCrystal>().canBeCollected;

            return PhysicsHelper
                .CastRaysInCyllinderVerticalGetUniqueColliders(
                    transform.position + Vector3.up * startLayersOffset,
                    circleGemsCollisionCheckingRadius,
                    raysInLayer,
                    layersIntervalHeight,
                    layers,
                    colliderEndingFilterCriteria
                    );
        }

        public bool IsCollidingWithLedgeGrabCollider(out RaycastHit ledgeGrabHit)
        {
            if (isLedgeGrabColliderCollisionDetectionEnabled)
            {
                float startLayersOffset = 1.6f;
                int layers = 5;
                int raysInLayer = 20;
                float layersIntervalHeight = 0.3f;
                float circleLedgeGrabCollisionCheckingRadius = 2f;
                float ledgeGrabAirHeightThreshold = 1.5f;

                if (CheckHeight() > ledgeGrabAirHeightThreshold && PhysicsHelper
                    .CastRaysInCyllinderVertical(
                        transform.position + Vector3.up * startLayersOffset,
                        circleLedgeGrabCollisionCheckingRadius,
                        raysInLayer,
                        out ledgeGrabHit,
                        layersIntervalHeight,
                        layers,
                        Layers.ledgeCollidersLayerMask
                        ))
                {
                    if (ledgeGrabHit.collider.CompareTag(Tags.ledgeColliderTag))
                    {
                        ledgeGrabHit.normal = ledgeGrabHit.collider.gameObject.GetComponent<LedgeColliderInfo>().normal;
                        return true;
                    }
                }
            }
            ledgeGrabHit = new RaycastHit();
            return false;
        }

        public float CheckHeight()
        {
            float heightCheckMaxDistance = 1000f;
            RaycastHit heightCheckRaycastHit = new RaycastHit();
            if (Physics.Raycast(transform.position, Vector3.down, out heightCheckRaycastHit, heightCheckMaxDistance)
                && IsLegitSolidGroundOrWallForCollision(heightCheckRaycastHit))
            {
                return heightCheckRaycastHit.distance;
            } else
            {
                return heightCheckMaxDistance;
            }
        }

        private bool IsLegitSolidGroundOrWallForCollision(RaycastHit hit)
        {
            return !hit.collider.gameObject.tag.Equals(Tags.gemsTag) && 
                !hit.collider.gameObject.tag.Equals(Tags.projectilesTag);
        }

        private IEnumerator DisableLedgeGrabColliderCollisionDetectionForTimePeriod(int milliseconds)
        {
            isLedgeGrabColliderCollisionDetectionEnabled = false;
            yield return new WaitForSeconds(milliseconds / 1000f);
            isLedgeGrabColliderCollisionDetectionEnabled = true;
        }

        private IEnumerator DisableWallClimbCollisionDetectionForTimePeriod(int milliseconds)
        {
            isWallClimbingCollisionDetectionEnabled = false;
            yield return new WaitForSeconds(milliseconds / 1000f);
            isWallClimbingCollisionDetectionEnabled = true;
        }

        private IEnumerator DisableRoofHangingCollisionDetectionForTimePeriod(int milliseconds)
        {
            isRoofHangingCollisionDetectionEnabled = false;
            yield return new WaitForSeconds(milliseconds / 1000f);
            isRoofHangingCollisionDetectionEnabled = true;
        }

        public void DisableLedgeGrabColliderDetectionForMilliseconds(int milliseconds)
        {
            StartCoroutine(DisableLedgeGrabColliderCollisionDetectionForTimePeriod(milliseconds));   
        }

        public void DisableWallClimbCollisionDetectionForMilliseconds(int milliseconds)
        {
            StartCoroutine(DisableWallClimbCollisionDetectionForTimePeriod(milliseconds));
        }

        public void DisableRoofHangingCollisionDetectionForMilliseconds(int milliseconds)
        {
            StartCoroutine(DisableRoofHangingCollisionDetectionForTimePeriod(milliseconds));
        }
    }
}
