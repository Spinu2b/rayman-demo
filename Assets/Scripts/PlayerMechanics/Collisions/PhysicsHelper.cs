﻿using Assets.Scripts.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Collisions
{
    public static class PhysicsHelper
    {
        private static bool debug = false;

        public static bool CastRaysInCircleVertical(
            Vector3 origin,
            Vector3 direction,
            float radius,
            int raysCount,
            out RaycastHit hit,
            int layerMask = Layers.defaultLayerMask
            )
        {
            for (int i = 0; i < raysCount; i++)
            {
                Vector3 rayVector = (Quaternion.AngleAxis((360.0f / raysCount) * i, Vector3.up) * (direction)).normalized;
                if (debug)
                {
                    Debug.DrawRay(origin, rayVector * radius);
                }
                if (Physics.Raycast(
                    origin,
                    rayVector,
                    out hit,
                    radius,
                    layerMask))
                {
                    return true;
                }
            }
            hit = new RaycastHit();
            return false;
        }

        public static List<RaycastHit> CastRaysCollidersInCircleVerticalGetUniqueColliders(
            Vector3 origin,
            Vector3 direction,
            float radius,
            int raysCount,
            Func<RaycastHit, bool> endingColliderFilterCriteria,
            int layerMask = Layers.defaultLayerMask
            )
        {
            var result = new List<RaycastHit>();
            for (int i = 0; i < raysCount; i++)
            {
                Vector3 rayVector = (Quaternion.AngleAxis((360.0f / raysCount) * i, Vector3.up) * (direction)).normalized;
                if (debug)
                {
                    Debug.DrawRay(origin, rayVector * radius);
                }
                RaycastHit hit = new RaycastHit();
                if (Physics.Raycast(
                    origin,
                    rayVector,
                    out hit,
                    radius,
                    layerMask) && endingColliderFilterCriteria(hit))
                {
                    result.Add(hit);
                }
            }
            return result.ToLookup(x => x.collider.gameObject.GetInstanceID()).Select(x => x.First()).ToList();
        }

        public static bool CastRaysInCircleVerticalListPoints(
            Vector3 origin,
            Vector3 direction,
            float radius,
            int raysCount,
            out List<RaycastHit> hits,
            int layerMask = Layers.defaultLayerMask
            )
        {
            hits = new List<RaycastHit>();
            for (int i = 0; i < raysCount; i++)
            {
                RaycastHit hitAtGivenMoment = new RaycastHit();
                Vector3 rayVector = (Quaternion.AngleAxis((360.0f / raysCount) * i, Vector3.up) * (direction)).normalized;
                if (debug)
                {
                    Debug.DrawRay(origin, rayVector * radius);
                }
                if (Physics.Raycast(
                    origin,
                    rayVector,
                    out hitAtGivenMoment,
                    radius,
                    layerMask))
                {
                    hits.Add(hitAtGivenMoment);
                }
            }
            return hits.Count > 0;
        }

        public static bool CastRaysInCyllinderVertical(
            Vector3 origin,
            float radius,
            int raysCountInLayer,
            out RaycastHit hit,
            float layersDistanceInterval,
            int layers,
            int layerMask = Layers.defaultLayerMask
            )
        {
            for (int i = 0; i < layers; i++)
            {
                if (PhysicsHelper.CastRaysInCircleVertical(
                    origin + Vector3.up * layersDistanceInterval * i,
                    Vector3.forward,
                    radius,
                    raysCountInLayer,
                    out hit,
                    layerMask))
                {
                    return true;
                }
            }
            hit = new RaycastHit();
            return false;
        }

        public static List<RaycastHit> CastRaysInCyllinderVerticalGetUniqueColliders(
            Vector3 origin,
            float radius,
            int raysCountInLayer,
            float layersDistanceInterval,
            int layers,
            Func<RaycastHit, bool> endingColliderFilterCriteria,
            int layerMask = Layers.defaultLayerMask
            )
        {
            var result = new List<RaycastHit>();
            for (int i = 0; i < layers; i++)
            {
                result.AddRange(
                    PhysicsHelper.CastRaysCollidersInCircleVerticalGetUniqueColliders(
                        origin + Vector3.up * layersDistanceInterval * i,
                        Vector3.forward,
                        radius,
                        raysCountInLayer,
                        endingColliderFilterCriteria,
                        layerMask
                    ));
            }
            return result.ToLookup(x => x.collider.gameObject.GetInstanceID()).Select(x => x.First()).ToList();
        }

        public static bool CastRaysInCyllinderVerticalListPoints(
            Vector3 origin,
            float radius,
            int raysCountInLayer,
            out List<RaycastHit> hits,
            float layersDistanceInterval,
            int layers,
            int layerMask = Layers.defaultLayerMask
            )
        {
            hits = new List<RaycastHit>();
            for (int i = 0; i < layers; i++)
            {
                List<RaycastHit> hitsAtGivenMoment = new List<RaycastHit>();
                if (PhysicsHelper.CastRaysInCircleVerticalListPoints(
                    origin + Vector3.up * layersDistanceInterval * i,
                    Vector3.forward,
                    radius,
                    raysCountInLayer,
                    out hitsAtGivenMoment,
                    layerMask))
                {
                    hits.AddRange(hitsAtGivenMoment);
                }
            }
            return hits.Count > 0;
        }
    }
}
