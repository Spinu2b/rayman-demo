﻿using Assets.Scripts.Animations;
using Assets.Scripts.PlayerMechanics.Aspects;
using Assets.Scripts.PlayerMechanics.Rules;
using Assets.Scripts.PlayerMechanics.Shooting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Targeting
{
    public class SwingLumHitHdlBeh : TargetHitHandlingBeh
    {
        public float maxSwingingRadius = 8f;
        public float minimalSwingingExtremumAngleDegrees = 60f;

        public override void OnTargetHit(
            HandProjectileBehaviour handProjectileBehaviour)
        {
            PlayerMovementStateInfo playerMovementStateInfo =
                playerObject.GetComponent<PlayerMovementStateInfo>();
            playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_LUM_SWINGING;
            RuleLumSwinging ruleLumSwinging = playerObject.GetComponent<RuleLumSwinging>();

            GameObject animatedPlayerPart =
                playerObject.GetComponentInChildren<AnimationController>().gameObject;


            PlayerMovementMetrics playerMovementMetrics = FindObjectOfType<PlayerMovementMetrics>();
            playerMovementMetrics.cameraRule = Camera.Rules.CameraRuleEnum.CAMERA_STRAFING;
            
            var handObject = HandShootingProjectilesFactory.SpawnStaticHandObject(
                transform.position + Vector3.down * 0.5f, Vector3.up, -animatedPlayerPart.transform.right, new Vector3(1.5f, 1.5f, 1.5f));

            ruleLumSwinging.ClearRuleState(
                this.gameObject, playerObject.transform.position,
                -animatedPlayerPart.transform.forward, -animatedPlayerPart.transform.right, 
                handProjectileBehaviour.isRightHand, handObject,
                maxSwingingRadius, minimalSwingingExtremumAngleDegrees);         

            // remove player shot hand from the scene for the time of swinging on the lum
            // we will recreate it flying towards the player to regain it
            // when the player leaves the lum
            Destroy(handProjectileBehaviour.gameObject);
        }
    }
}
