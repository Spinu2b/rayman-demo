﻿using Assets.Scripts.PlayerMechanics.Shooting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Targeting
{
    public class TargetHitHandlingBeh : MonoBehaviour
    {
        protected GameObject playerObject;

        private void Awake()
        {
            playerObject = FindObjectOfType<PlayerMovementStateInfo>().gameObject;
        }

        public virtual void OnTargetHit(HandProjectileBehaviour handProjectileBehaviour) { }
    }
}
