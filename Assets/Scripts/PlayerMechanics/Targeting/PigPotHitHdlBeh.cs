﻿using Assets.Scripts.PlayerMechanics.Shooting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Targeting
{
    public enum PigPotCrystalsDrop
    {
        DROP_3_YELLOW_CRYSTALS,
        DROP_4_YELLOW_CRYSTALS
    }

    public static class PigPotCrystalsDropHelper
    {
        public static int GetJewelsAmountToDrop(PigPotCrystalsDrop pigPotCrystalsDropType)
        {
            return pigPotCrystalsDropType == 
                PigPotCrystalsDrop.DROP_3_YELLOW_CRYSTALS ? 3 : 4;
        }
    }

    public static class DropJewelsSpawner
    {
        public static void SpawnYellowJewelsInCircle(
            Vector3 spawnOriginPosition,
            float spawnRadius,
            int jewelsCount)
        {

        }
    }

    public class PigPotHitHdlBeh : TargetHitHandlingBeh
    {
        private bool isBeingHit = false;
        private bool isToBeDestroyed = false;
        private Vector3 originalPosition;
        private float currentHitShakeProgress = 0f;

        private float shakingSpeed = 0.3f;
        private float breakHitStrengthThreshold = 3f;

        public PigPotCrystalsDrop pigPotDropType;

        private void Start()
        {
            isBeingHit = false;
            isToBeDestroyed = false;
            originalPosition = transform.position;
            currentHitShakeProgress = 0f;
        }

        public override void OnTargetHit(
           HandProjectileBehaviour handProjectileBehaviour)
        {
            isBeingHit = true;  
            isToBeDestroyed = 
                isToBeDestroyed || (handProjectileBehaviour.chargePower >= breakHitStrengthThreshold);
        }

        private void FixedUpdate()
        {
            if (!isToBeDestroyed && isBeingHit)
            {                
                transform.position = originalPosition + Vector3.up * Mathf.Sin(currentHitShakeProgress);

                currentHitShakeProgress += shakingSpeed;
                if (currentHitShakeProgress > 180f * Mathf.Deg2Rad)
                {
                    currentHitShakeProgress = 0f;
                    isBeingHit = false;
                    transform.position = originalPosition;
                }
                return;
            } else if (isToBeDestroyed)
            {
                DropJewelsSpawner.SpawnYellowJewelsInCircle(
                    transform.position,
                    1.5f,
                    PigPotCrystalsDropHelper.GetJewelsAmountToDrop(pigPotDropType));
                Destroy(gameObject);
                return;
            }
        }
    }
}
