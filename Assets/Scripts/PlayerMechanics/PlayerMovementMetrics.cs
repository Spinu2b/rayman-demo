﻿using Assets.Scripts.PlayerMechanics.Camera.Rules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics
{
    public class PlayerMovementMetrics : MonoBehaviour
    {
        public Vector3 forwardDirection = new Vector3(0, 0, 0);
        public float angleFromAbsoluteForward = 0.0f;

        public CameraRuleEnum cameraRule = CameraRuleEnum.CAMERA_FOLLOW;

        public static float gravityAcceleration = 0.012f;
        public static float limitFallSpeed = 0.3f;
        public static float limitFallSpeedHelicopter = 0.05f;

        public void UpdateForwardDirectionAndRightDirection(
            Vector3 forwardDirection, float angleFromAbsoluteForward)
        {
            this.forwardDirection = forwardDirection;
            this.angleFromAbsoluteForward = angleFromAbsoluteForward;
        }
    }
}
