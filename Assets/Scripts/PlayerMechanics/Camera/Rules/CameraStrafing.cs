﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Camera.Rules
{
    public class CameraStrafing : CameraRuleBase
    {
        private void FixedUpdate()
        {
            if (playerMovementMetrics.cameraRule.Equals(CameraRuleEnum.CAMERA_STRAFING))
            {
                Vector3 lookDirection = ((playerObject.transform.position - transform.position).normalized + Vector3.up * 0.05f).normalized;
                //UpdatePlayerMovementMetrics(new Vector3(lookDirection.x, 0, lookDirection.z));
                transform.forward = Vector3.Lerp(transform.forward, lookDirection, smoothSpeed);

                Vector3 playerLastPosition;
                playerLastPosition = 
                    playerObject.position + Vector3.up +
                    (-playerMovementMetrics.forwardDirection.normalized) * distanceFromObject;
                playerLastPosition.y = playerObject.position.y + distanceFromObject / 2.5f;

                transform.position = Vector3.Lerp(transform.position, playerLastPosition, smoothSpeed);
            }
        }
    }
}
