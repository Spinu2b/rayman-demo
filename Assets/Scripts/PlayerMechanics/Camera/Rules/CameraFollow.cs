﻿using Assets.Scripts.PlayerMechanics;
using Assets.Scripts.PlayerMechanics.Camera.Rules;
using UnityEngine;

namespace Assets.Scripts
{
    public class CameraFollow : CameraRuleBase
    {
        private float cameraControlSpeed = 3f;
        private float smoothSpeedCameraRotation = 0f;

        void FixedUpdate()
        {
            if (playerMovementMetrics.cameraRule.Equals(CameraRuleEnum.CAMERA_FOLLOW))
            {
                //Vector3 lookDirection = ((playerObject.transform.position - transform.position).normalized + Vector3.up * 0.05f).normalized;
                //UpdatePlayerMovementMetrics(new Vector3(lookDirection.x, 0, lookDirection.z));
                //transform.forward = Vector3.Lerp(transform.forward, lookDirection, smoothSpeed);

                //Vector3 playerLastPosition;
                //playerLastPosition = playerObject.position - (lookDirection.normalized * distanceFromObject);
                //playerLastPosition.y = playerObject.position.y + distanceFromObject / 2.5f;

                //transform.position = Vector3.Lerp(transform.position, playerLastPosition, smoothSpeed);

                Vector2 translation = cameraMovementInput.GetTranslation();

                Vector2 verticalTranslation = Vector3.Project(translation, Vector3.up);
                Vector2 horizontalTranslation = Vector3.Project(translation, Vector3.right);

                Vector3 cameraNewPosition;
                Vector3 playerInputCameraTranslation;

                if (Vector3.Angle(Vector3.right, horizontalTranslation) < 90f)
                {
                    playerInputCameraTranslation = (transform.right * horizontalTranslation.magnitude) * cameraControlSpeed;
                    //cameraNewPosition = transform.position + (transform.right * horizontalTranslation.magnitude) * cameraControlSpeed;
                } else
                {
                    playerInputCameraTranslation = -(transform.right * horizontalTranslation.magnitude) * cameraControlSpeed;
                    //cameraNewPosition = transform.position - (transform.right * horizontalTranslation.magnitude) * cameraControlSpeed;
                }

                if (Vector3.Angle(Vector3.up, verticalTranslation) < 90f)
                {
                    //cameraNewPosition = cameraNewPosition + (transform.up * verticalTranslation.magnitude) * cameraControlSpeed;
                    //playerInputCameraTranslation += 
                } else
                {
                    //cameraNewPosition = cameraNewPosition - (transform.up * verticalTranslation.magnitude) * cameraControlSpeed;
                }

                //transform.position = Vector3.Lerp(transform.position, cameraNewPosition, smoothSpeed);

                if (playerInputCameraTranslation.magnitude > 0.01f)
                {
                    smoothSpeedCameraRotation = Mathf.Lerp(smoothSpeedCameraRotation, 1f, 0.01f);
                }
                else
                {
                    smoothSpeedCameraRotation = Mathf.Lerp(smoothSpeedCameraRotation, smoothSpeed, 0.01f);
                }

                Vector3 lookDirection = ((playerObject.transform.position - transform.position)
                    .normalized + Vector3.up * 0.05f).normalized;
                UpdatePlayerMovementMetrics(new Vector3(lookDirection.x, 0, lookDirection.z));
                transform.forward = Vector3.Lerp(transform.forward, lookDirection,
                    playerInputCameraTranslation.magnitude > 0.01f ? smoothSpeedCameraRotation : smoothSpeed);            

                Vector3 playerLastPosition;
                playerLastPosition = playerObject.position + playerInputCameraTranslation - (lookDirection.normalized * distanceFromObject);
                playerLastPosition.y = playerObject.position.y + distanceFromObject / 2.5f;

                transform.position = Vector3.Lerp(transform.position, playerLastPosition, smoothSpeed);
            }            
        }
    }
}