﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.PlayerMechanics.Camera.Rules
{
    public enum CameraRuleEnum
    {
        CAMERA_FOLLOW,
        CAMERA_STRAFING,
        CAMERA_TARGET_PLAYER_BACK_STRAFING
    }
}
