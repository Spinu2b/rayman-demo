﻿using Assets.Scripts.PlayerMechanics.Rules.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Camera.Rules
{
    public abstract class CameraRuleBase : MonoBehaviour
    {
        public Transform playerObject;

        public float distanceFromObject = 18f;
        public float smoothSpeed = 2f;

        protected PlayerMovementMetrics playerMovementMetrics;
        protected CameraMovementInput cameraMovementInput;

        protected void Awake()
        {
            playerMovementMetrics = FindObjectsOfType<PlayerMovementMetrics>()[0];
            cameraMovementInput = FindObjectOfType<CameraMovementInput>();
        }

        protected void UpdatePlayerMovementMetrics(Vector3 forwardVector)
        {
            playerMovementMetrics.UpdateForwardDirectionAndRightDirection(
                forwardVector,
                Vector3.SignedAngle(Vector3.forward, forwardVector, Vector3.up));
        }
    }
}
