﻿using Assets.Scripts.Animations;
using Assets.Scripts.PlayerMechanics.Aspects.Shooting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Camera.Rules
{
    public class CameraTargetPlayerBackStrafe : CameraRuleBase
    {
        protected TargettingAspect playerTargettingAspect;

        protected new void Awake()
        {
            base.Awake();
            this.playerTargettingAspect = playerObject.GetComponent<TargettingAspect>();
        }

        private void FixedUpdate()
        {
            if (playerMovementMetrics.cameraRule.Equals(CameraRuleEnum.CAMERA_TARGET_PLAYER_BACK_STRAFING))
            {
                if (playerTargettingAspect
                    ?.currentTarget?.transform?.position != null)
                {
                    Vector3 targetPosition = playerTargettingAspect
                        .currentTarget.transform.position;

                    Vector3 lookDirection = (targetPosition - transform.position).normalized;
                    UpdatePlayerMovementMetrics(Vector3.ProjectOnPlane(lookDirection, Vector3.up).normalized);
                    transform.forward = Vector3.Lerp(transform.forward, lookDirection, smoothSpeed);

                    Vector3 playerLastPosition;
                    playerLastPosition =
                        playerObject.position + Vector3.up +
                        (-playerMovementMetrics.forwardDirection.normalized) * distanceFromObject;
                    playerLastPosition.y = playerObject.position.y + distanceFromObject / 7.5f;

                    transform.position = Vector3.Lerp(transform.position, playerLastPosition, smoothSpeed);
                } else
                {
                    return;
                }
                
            }
        }
    }
}
