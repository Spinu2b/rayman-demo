﻿using Assets.Scripts.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Camera
{
    public class CameraMovementInput : MonoBehaviour
    {
        public Vector2 GetTranslation()
        {
            return currentTranslationFromUpdate;
        }

        private Vector2 translationFromUpdate;
        private float axesMagnitudeFromUpdate;
        private float horizontalAxisRawFromUpdate;

        private Vector2 currentTranslationFromUpdate;

        private void Update()
        {
            if (Input.GetKey(KeyControls.cameraLeftButtonKeyboard))
            {
                translationFromUpdate = Vector2.left;
            } 
            else if (Input.GetKey(KeyControls.cameraRightButtonKeyboard))
            {
                translationFromUpdate = Vector2.right;
            } else
            {
                translationFromUpdate = Vector2.zero;
            }

            //axesMagnitudeFromUpdate =
            //    Mathf.Clamp01(new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y")).magnitude);

            //horizontalAxisRawFromUpdate = Input.GetAxisRaw("Mouse X");

            //translationFromUpdate = ((Input.GetAxisRaw("Mouse Y") * Vector3.up) +
            //       (Input.GetAxisRaw("Mouse X") * Vector3.right)).normalized
            //       * axesMagnitudeFromUpdate;

            currentTranslationFromUpdate =
                Vector2.Lerp(currentTranslationFromUpdate, translationFromUpdate,
                (0.35f * Time.deltaTime) / (Time.fixedDeltaTime));


            //PlayerMovementVectorRotationWithLengthInterpolationHelper.InterpolationRotationWithLength(
            //    Vector3.forward, currentTranslationFromUpdate, translationFromUpdate,
            //    //0.25f);
            //    (0.35f * Time.deltaTime) / (Time.fixedDeltaTime));
        }
    }
}
