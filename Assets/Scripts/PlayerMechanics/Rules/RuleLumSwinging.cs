﻿using Assets.Scripts.Animations.Models;
using Assets.Scripts.PlayerMechanics.Aspects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Rules
{
    public class RuleLumSwinging : PlayerMovementRule
    {
        private GameObject lumForSwinging;     
        private bool caughtByRightHand = true;
        private float swingingRadius = 0f;

        private float swingingGravityAcceleration = 0.025f;
        private float swingingDirectionChangingVelocity = 0.2f;

        private Vector3 initialPlayerPositionForSwinging;

        private GameObject handObject;

        private float maxSwingingRadius;
        private float minimalSwingingExtremumAngleDegrees;

        private float swingingRadiusCorrectionStep = 0.05f;

        protected override PlayerMovementRuleEnum GetPlayerMovementRuleEnum()
        {
            return PlayerMovementRuleEnum.RULE_LUM_SWINGING;
        }

        public void ClearRuleState(
            GameObject lumForSwinging,
            Vector3 playerPosition,
            Vector3 playerForwardDirection,
            Vector3 playerRightDirection,
            bool caughtByRightHand,
            GameObject handObject,
            float maxSwingingRadius,
            float minimalSwingingExtremumAngleDegrees)
        {
            this.lumForSwinging = lumForSwinging;
            // we only care about vector's direction, no matter the vector's turn
            this.caughtByRightHand = caughtByRightHand;
            //this.swingingDirection = playerForwardDirection;

            playerMovementStateInfo.movementVelocity = new Vector3(0f, 0f, 0f);
            this.swingingRadius = Vector3.Distance(playerPosition, lumForSwinging.transform.position);

            this.initialPlayerPositionForSwinging = playerPosition;
            this.handObject = handObject;

            this.maxSwingingRadius = maxSwingingRadius;
            this.minimalSwingingExtremumAngleDegrees = minimalSwingingExtremumAngleDegrees;
        }

        protected override void OnRuleEnter()
        {
            animationController.StopAnimatorUpdating();
        }

        protected override void OnRuleExit()
        {
            animationController.ResumeAnimatorUpdating();
            if (this.handObject)
            {
                Destroy(this.handObject);
            }            
        }

        protected override void OnRuleFixedUpdate()
        {
            Vector3 velocityPlaneNormal = (gameObject.transform.position - lumForSwinging.transform.position).normalized;

            //if (swingingRadius > maxSwingingRadius)
            //{
            //    swingingRadius -= swingingRadiusCorrectionStep;
            //    swingingRadius = Mathf.Clamp(swingingRadius, 1f, 1000f);

            //    initialPlayerPositionForSwinging = 
            //        initialPlayerPositionForSwinging + (-velocityPlaneNormal) * swingingRadiusCorrectionStep;
            //}



            if (!Input.GetButton("Jump"))
            {
                playerMovementStateInfo.isAbleToTriggerEnteringJumpState = true;
            }

            if (Input.GetButton("Jump") &&
                playerMovementStateInfo.isAbleToTriggerEnteringJumpState)
            {
                playerMovementMetrics.cameraRule = Camera.Rules.CameraRuleEnum.CAMERA_FOLLOW;
                playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_AIR;
                playerMovementStateInfo.isJumping = true;
                playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
                playerMovementStateInfo.isAbleToTriggerEnteringJumpState = false;

                HandShootingProjectilesFactory.SpawnHandProjectileGoingBackToPlayer(
                    playerShootingAspect, animatedPlayerPart.transform, lumForSwinging.transform.position, caughtByRightHand);

                return;
            }

            if (rayCollider.HasHitTheSolidObstacle())
            {
                bool isTouchingTheGround = rayCollider.IsHittingGround();

                playerMovementMetrics.cameraRule = Camera.Rules.CameraRuleEnum.CAMERA_FOLLOW;
                playerMovementStateInfo.currentRule = 
                    isTouchingTheGround ? PlayerMovementRuleEnum.RULE_GROUND : PlayerMovementRuleEnum.RULE_AIR;
                playerMovementStateInfo.isJumping = false;
                playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
                playerMovementStateInfo.isAbleToTriggerEnteringJumpState = false;

                HandShootingProjectilesFactory.SpawnHandProjectileGoingBackToPlayer(
                    playerShootingAspect, animatedPlayerPart.transform, lumForSwinging.transform.position, caughtByRightHand);

                return;
            }

            playerMovementInput.FixedUpdatePlayerMovementInput();
            float horizontalPlayerInputValue = playerMovementInput.GetHorizontalAxisRaw();         

            transform.position = lumForSwinging.transform.position + (velocityPlaneNormal * this.swingingRadius);
            Vector3 currentVelocityWorldSpace =
                Vector3.ProjectOnPlane(
                    transform.TransformDirection(playerMovementStateInfo.movementVelocity),
                    velocityPlaneNormal).normalized *
                playerMovementStateInfo.movementVelocity.magnitude;


            Vector3 gravitionalAcceleration = new Vector3(0f, -swingingGravityAcceleration, 0f);
            Vector3 projectedGravitionalAcceleration = 
                Vector3.ProjectOnPlane(gravitionalAcceleration, velocityPlaneNormal);

            Vector3 projectedTranslation = 
                Vector3.ProjectOnPlane(-animatedPlayerPart.transform.right.normalized, velocityPlaneNormal).normalized
                * horizontalPlayerInputValue * swingingDirectionChangingVelocity;

            //currentVelocityWorldSpace = projectedTranslation;

            currentVelocityWorldSpace = 
                currentVelocityWorldSpace + projectedGravitionalAcceleration;


            playerMovementStateInfo.movementVelocity = 
                transform.InverseTransformDirection(currentVelocityWorldSpace);

            //transform.position = transform.position + projectedTranslation;
            transform.position = transform.position + currentVelocityWorldSpace;
            //transform.Translate(playerMovementStateInfo.movementVelocity);          
            //Debug.Log("Angleeee: " + Vector3.Angle(Vector3.up, velocityPlaneNormal));

            // value from 0 to 1
            Tuple<float, bool> swingProgressInfo = GetCurrentSwingTrajectoryProgressInfo(currentVelocityWorldSpace);
            UpdateSwingingAnimationAccordingToSwingProgress(
                swingProgressInfo.Item1, swingProgressInfo.Item2, Time.fixedDeltaTime);
        }

        private void UpdateSwingingAnimationAccordingToSwingProgress(
            float swingProgress, bool swingingForward, float deltaTime)
        {
            if (swingingForward)
            {
                animationController.SetAnimationFrameInManualUpdating(
                    RaymanAnimations.LumSwingingSequencePosesInTheMiddleBackToFrontRightHandStateName(),
                    swingProgress,
                    deltaTime);
            } else
            {
                animationController.SetAnimationFrameInManualUpdating(
                   RaymanAnimations.LumSwingingSequencePosesInTheMiddleFrontToBackRightHandStateName(),
                   swingProgress,
                   deltaTime
                   );
            }
            
        }

        private Tuple<float, bool> GetCurrentSwingTrajectoryProgressInfo(Vector3 velocityWorldSpace)
        {
            float angle = Vector3.Angle(
                -animatedPlayerPart.transform.forward,
                Vector3.ProjectOnPlane(velocityWorldSpace, Vector3.up).normalized);


            bool isSwingingForward = angle < 90;


            // from the law of energy/momentum conservation... should roughly just work..
            Vector3 maxStartDirectionToPlayerInSwingingSequence = 
                (initialPlayerPositionForSwinging - lumForSwinging.transform.position).normalized;
            Vector3 mirroredMaxDirectionInSwinging = 
                Vector3.Reflect(-maxStartDirectionToPlayerInSwingingSequence, Vector3.down).normalized;

            Vector3 directionToPlayer = (transform.position - lumForSwinging.transform.position).normalized;

            float angularDistanceFromSwingStartExtremumPointToCurrentPosition =
                isSwingingForward ? 
                Vector3.Angle(maxStartDirectionToPlayerInSwingingSequence, directionToPlayer) : 
                Vector3.Angle(mirroredMaxDirectionInSwinging, directionToPlayer);

            float halfOverallAngularDistance = Vector3.Angle(Vector3.down, maxStartDirectionToPlayerInSwingingSequence);

            return Tuple.Create(
                angularDistanceFromSwingStartExtremumPointToCurrentPosition / (2f * halfOverallAngularDistance),
                isSwingingForward);

            //float angularDistanceFromSwingingMiddlePointToPlayer =
            //    Vector3.Angle(Vector3.down, directionToPlayer);

            //float angularDistanceFromSwingingMiddlePointToPlayerWithSign =
            //    Vector3.SignedAngle(Vector3.down, directionToPlayer, -animatedPlayerPart.transform.right);

            //float circularDistanceFromPlayerToMiddle = 
            //    2 * Mathf.PI * swingingRadius * (angularDistanceFromSwingingMiddlePointToPlayer / 360f);
        }
    }
}
