﻿using Assets.Scripts.Animations;
using Assets.Scripts.HUD.ScoreCounting;
using Assets.Scripts.PlayerMechanics.Aspects;
using Assets.Scripts.PlayerMechanics.Aspects.Shooting;
using Assets.Scripts.PlayerMechanics.Rules.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Rules
{
    public static class CurvedShootingDirectionHelper
    {
        public static Vector3 GetCurvedShootingDirection(
            Vector3 playerMovementDirection,
            Vector3 playerForward,
            Vector3 playerRight)
        {
            float angleRight = Vector3.Angle(playerMovementDirection.normalized, playerRight.normalized);
            float additionalAngle = 20f;

            if (angleRight < 80f)
            {
                // shooting right
                return Quaternion.AngleAxis(additionalAngle, Vector3.up) * 
                    ((playerForward.normalized + playerRight.normalized) / 2.0f).normalized;               
            } else
            {
                // shooting left
                return Quaternion.AngleAxis(-additionalAngle, Vector3.up) *
                    ((playerForward.normalized + (-playerRight.normalized)) / 2.0f).normalized;
            }
        }
    }

    public abstract class PlayerMovementRule : MonoBehaviour
    {
        protected PlayerMovementMetrics playerMovementMetrics;
        protected GameObject animatedPlayerPart;
        protected AnimationController animationController;
        protected RayCollider rayCollider;
        protected PlayerMovementStateInfo playerMovementStateInfo;

        protected TargettingAspect targettingAspect;

        protected ScoreAccumulatorDeccumulator scoreHunterHook;

        protected AudioSource helicopterSound;
        protected AudioSource jumpSound;

        protected PlayerShootingAspect playerShootingAspect;

        protected PlayerMovementInput playerMovementInput;

        protected bool previousRuleEnabledState = false;

        protected virtual void OnRuleFixedUpdate() { }
        protected virtual void OnRuleUpdate() { }

        protected virtual void OnRuleEnter() { }
        protected virtual void OnRuleExit() { }
        protected abstract PlayerMovementRuleEnum GetPlayerMovementRuleEnum();

        protected void HandleGemsCollectiblesCollisions()
        {
            List<RaycastHit> gemsHits = rayCollider.GetAllCollectibleCrystalsThePlayerCollidesWith();
            foreach (var gem in gemsHits)
            {
                gem.collider.gameObject.GetComponent<CollectibleCrystal>()
                    .InvokeCollectedBehaviour(animatedPlayerPart.transform);
                scoreHunterHook.ScorePointsInTime(10f, 1000);
            }
        }

        protected bool IsInStrafingKindRule()
        {
            return playerMovementStateInfo.currentRule.Equals(PlayerMovementRuleEnum.RULE_STRAFING_AIR) ||
                playerMovementStateInfo.currentRule.Equals(PlayerMovementRuleEnum.RULE_STRAFING_GROUND);
        }

        protected void HandleShooting()
        {
            if (playerShootingAspect.HasHandsToShoot() 
                && playerShootingAspect.CanTriggerShootingHand() 
                && playerShootingAspect.IsPressingShootingButtonInstantShot())
            {
                if (!IsInStrafingKindRule())
                {
                    playerShootingAspect.ShootHand();
                } else
                {
                    if (!playerShootingAspect.IsGoingForwardOrBackwardsForShooting())
                    {
                        Vector3 shootingDirection =
                            CurvedShootingDirectionHelper.GetCurvedShootingDirection(
                                Vector3.ProjectOnPlane(
                                    transform.TransformDirection(
                                        playerMovementStateInfo.movementVelocity.normalized),
                                    Vector3.up),
                                -animatedPlayerPart.transform.forward.normalized,
                                -animatedPlayerPart.transform.right.normalized);
                        playerShootingAspect
                            .ShootHandFreelyInCurvedPath(
                                shootingDirection);
                    } else
                    {
                        playerShootingAspect.ShootHand();
                    }                    
                }                
                return;
            } else if (playerShootingAspect.HasHandsToShoot()
                && playerShootingAspect.CanTriggerShootingHand()
                && playerShootingAspect.IsPressingShootingButtonForCharging())
            {
                if (playerShootingAspect.HasJustStartedPressingShootingButtonForCharging())
                {
                    playerShootingAspect.InitiateFistChargingCycleForAppropriateHand();
                    return;
                }

                playerShootingAspect.KeepChargingCycle();
                return;
            } else if (playerShootingAspect.HasHandsToShoot()
                && playerShootingAspect.WantsToFinishChargingAndShoot())
            {
                if (!IsInStrafingKindRule())
                {
                    playerShootingAspect.FinishFistChargingCycleAndShootHand();
                } else
                {
                    if (!playerShootingAspect.IsGoingForwardOrBackwardsForShooting())
                    {
                        Vector3 shootingDirection =
                            CurvedShootingDirectionHelper.GetCurvedShootingDirection(
                                Vector3.ProjectOnPlane(
                                    transform.TransformDirection(
                                        playerMovementStateInfo.movementVelocity.normalized),
                                    Vector3.up),
                                -animatedPlayerPart.transform.forward.normalized,
                                -animatedPlayerPart.transform.right.normalized);
                            //Vector3.Project(transform.TransformDirection(
                            //    playerMovementStateInfo.movementVelocity.normalized),
                            //    -animatedPlayerPart.transform.right.normalized).normalized;
                        playerShootingAspect
                            .FinishFistChargingCycleAndShootHandFreelyInCurvedPath(
                                shootingDirection);
                    } else
                    {
                        playerShootingAspect.FinishFistChargingCycleAndShootHand();
                    }
                }
               
                //playerShootingAspect.ShootHand();
                return;
            }
        }

        private void Update()
        {
            if (playerMovementStateInfo.currentRule.Equals(GetPlayerMovementRuleEnum()))
            {
                OnRuleUpdate();
            }
        }

        private void FixedUpdate()
        {
            bool currentRuleEnabledState = playerMovementStateInfo.currentRule.Equals(GetPlayerMovementRuleEnum());
            if (currentRuleEnabledState && !previousRuleEnabledState)
            {
                OnRuleEnter();
            }

            if (!currentRuleEnabledState && previousRuleEnabledState)
            {
                OnRuleExit();
            }

            if (playerMovementStateInfo.currentRule.Equals(GetPlayerMovementRuleEnum()))
            {
                HandleGemsCollectiblesCollisions();
                OnRuleFixedUpdate();
            }

            previousRuleEnabledState = currentRuleEnabledState;
        }

        protected void Awake()
        {
            this.playerMovementMetrics = FindObjectsOfType<PlayerMovementMetrics>()[0];
            this.animatedPlayerPart = GetComponentInChildren<AnimationController>().gameObject;
            this.animationController = GetComponentInChildren<AnimationController>();
            this.rayCollider = GetComponent<RayCollider>();
            this.playerMovementStateInfo = GetComponent<PlayerMovementStateInfo>();

            this.helicopterSound = GetComponents<AudioSource>()[0];
            this.jumpSound = GetComponents<AudioSource>()[1];

            this.scoreHunterHook = FindObjectOfType<ScoreAccumulatorDeccumulator>();

            this.playerShootingAspect = GetComponent<PlayerShootingAspect>();
            this.targettingAspect = GetComponent<TargettingAspect>();

            this.playerMovementInput = FindObjectOfType<PlayerMovementInput>();
        }
    }
}
