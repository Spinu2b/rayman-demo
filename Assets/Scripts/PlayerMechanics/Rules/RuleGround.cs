﻿using Assets.Scripts.Animations.Models;
using Assets.Scripts.Common;
using Assets.Scripts.PlayerMechanics.Camera.Rules;
using Assets.Scripts.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Rules
{
    public static class GroundMovementHelper
    {
        public static Vector3 GetClippedTranslationConsideringWalkingCycles(
            Vector3 inputTranslation, float maxSpeed)
        {
            if (inputTranslation.magnitude > 0.1f && inputTranslation.magnitude < 0.3f)
            {
                return inputTranslation.normalized * (maxSpeed / 3f);
            }
            else if (inputTranslation.magnitude > 0.1f && inputTranslation.magnitude <= 0.8f)
            {
                return inputTranslation.normalized * (maxSpeed / 2f);
            }
            else if (inputTranslation.magnitude > 0.8f)
            {
                return inputTranslation.normalized * (maxSpeed);
            } else
            {
                return Vector3.zero;
            }
        }
    }

    public class RuleGround : GroundRulesBase
    {
        private float playerRunningSpeed = 0.21f;
        //private Vector3 movementVelocity = new Vector3(0.0f, 0.0f, 0.0f);

        protected override PlayerMovementRuleEnum GetPlayerMovementRuleEnum()
        {
            return PlayerMovementRuleEnum.RULE_GROUND;
        }

        protected override void OnRuleEnter()
        {
            playerMovementStateInfo.isHelicopter = false;
            helicopterSound.Pause();
            playerMovementStateInfo.isJumping = false;
        }

        protected override void OnRuleExit()
        {
            
        }

        protected override void OnRuleFixedUpdate()
        {
            #region Rule
            if (Input.GetButton(KeyControls.strafingButton))
            {
                // exit ground rule right away and go into strafing ground rule
                playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_STRAFING_GROUND;
                playerMovementMetrics.cameraRule = CameraRuleEnum.CAMERA_STRAFING;
                return;
            }

            if (Input.GetButton(KeyControls.groundRollButton) &&
                (!playerGroundRollingAspect.IsPerformingGroundRoll() && !playerGroundRollingAspect.HasEndedGroundRoll()))
            {
                playerGroundRollingAspect.StartPerformingForwardGroundRoll(
                    -animatedPlayerPart.transform.forward);
                return;
            }

            HandleShooting();
            rayCollider.AlignOnTopOfTheGround();

            playerMovementInput.FixedUpdatePlayerMovementInput();

            Vector3 inputTranslation = playerMovementInput.GetTranslation();
            Vector3 translation = playerMovementInput.GetTranslation() * playerRunningSpeed;

            Vector3 translationDirectionInWorldSpace =
                    (Quaternion.AngleAxis(playerMovementMetrics.angleFromAbsoluteForward, Vector3.up)
                    * translation).normalized;

            playerMovementStateInfo.movementVelocity.y = 0;

            if (!playerGroundRollingAspect.IsPerformingGroundRoll())
            {             
                if (inputTranslation.magnitude < 0.1f)
                {
                    float movementSmoothingInterpolation = 0.6f;
                    playerMovementStateInfo.movementVelocity.x =
                        Mathf.Lerp(playerMovementStateInfo.movementVelocity.x, 0, movementSmoothingInterpolation);
                    playerMovementStateInfo.movementVelocity.z =
                        Mathf.Lerp(playerMovementStateInfo.movementVelocity.z, 0, movementSmoothingInterpolation);
                }
                else
                {
                    float movementSmoothingInterpolation = 0.25f;
                    playerMovementStateInfo.movementVelocity.x =
                        Mathf.Lerp(playerMovementStateInfo.movementVelocity.x, translation.x, movementSmoothingInterpolation);
                    playerMovementStateInfo.movementVelocity.z =
                        Mathf.Lerp(playerMovementStateInfo.movementVelocity.z, translation.z, movementSmoothingInterpolation);
                }

                if (inputTranslation.magnitude > 0.1f)
                {
                    transform.forward = playerMovementMetrics.forwardDirection;
                    animatedPlayerPart.transform.forward = -translationDirectionInWorldSpace;
                }
            } else
            {
                playerMovementStateInfo.movementVelocity = 
                    transform.InverseTransformDirection(playerGroundRollingAspect
                        .GetMovementVelocityAppropriateForCurrentForwardRollCycle());
            }

            playerMovementStateInfo.movementVelocity = transform.InverseTransformDirection
                (WallCollisionHelper
                   .GetMovementVelocityConsideringWallCollisions(
                    gameObject,
                    rayCollider,
                    -animatedPlayerPart.transform.forward,
                    transform.TransformDirection(playerMovementStateInfo.movementVelocity)));

            Vector3 nonInterpolatedInputTranslation = playerMovementInput.GetNonInterpolatedTranslation();

            string currentAnimationState = animationController.GetCurrentAnimationState();

            if (
                !RaymanAnimationsGroundHelper.IsUprisingWalkingRunningAnimation(currentAnimationState)
                &&
                !RaymanAnimationsGroundHelper.IsExitingWalkingRunningAnimation(currentAnimationState)
                &&
                !RaymanAnimationsGroundHelper.IsLandingAnimation(currentAnimationState))
            {
                transform.Translate(playerMovementStateInfo.movementVelocity);
            } else if (
                !RaymanAnimationsGroundHelper.IsExitingWalkingRunningAnimation(currentAnimationState))
            {
                transform.Translate(playerMovementStateInfo.movementVelocity / 2f);
            } else if (nonInterpolatedInputTranslation.magnitude <= 0.1f)
            {
                if (animationController.GetCurrentAnimationState().Equals(
                    RaymanAnimations.ExitingWalkingCycle2ToGroundIdleStateName()))
                {
                    transform.Translate(transform.InverseTransformDirection(
                        -animatedPlayerPart.transform.forward).normalized * playerRunningSpeed * 0.25f *
                        (1f - animationController.GetAnimationNormalizedTime()));
                } else
                {
                    transform.Translate(transform.InverseTransformDirection(
                        -animatedPlayerPart.transform.forward).normalized * playerRunningSpeed * 0.6f *
                        (1f - animationController.GetAnimationNormalizedTime()));
                }                
            }                                 

            if (Input.GetButton("Jump") 
                && rayCollider.IsHittingGround()
                && playerMovementStateInfo.isAbleToTriggerEnteringJumpState
                && !playerGroundRollingAspect.IsPerformingGroundRoll())
            {
                playerMovementStateInfo.isJumping = true;
                playerMovementStateInfo.isAbleToTriggerEnteringJumpState = false;
                playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
                playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_AIR;
                return;
            }

            if (!rayCollider.IsHittingGround())
            {
                playerGroundRollingAspect.BreakTheRollCycle();
                playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_AIR;
                return;
            }

            if (!Input.GetButton("Jump"))
            {
                playerMovementStateInfo.isAbleToTriggerEnteringJumpState = true;
                playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
            }

            #endregion
            #region Animation

            if (enteringFromGroundStrafing)
            {
                animationController.ChangeAnimationState(
                    RaymanAnimations.StrafingExitIntoIdleGroundStateName());
                enteringFromGroundStrafing = false;
                return;
            }

            if (landingWithHelicopter)
            {
                if (nonInterpolatedInputTranslation.magnitude > 0.1f && nonInterpolatedInputTranslation.magnitude <= 0.8f)
                {
                    animationController.ChangeAnimationState(RaymanAnimations.LandingFromHelicopterToRunningCycle4StateName());
                } else if (nonInterpolatedInputTranslation.magnitude <= 0.1f)
                {
                    animationController.ChangeAnimationState(RaymanAnimations.LandingFromHelicopterToGroundIdleStateName());
                } else
                {
                    animationController.ChangeAnimationState(RaymanAnimations.LandingFromHelicopterToRunningCycle4StateName());
                }

                
                landingWithHelicopter = false;
                landing = false;
                return;
            } else if (landing)
            {
                if (nonInterpolatedInputTranslation.magnitude > 0.1f && nonInterpolatedInputTranslation.magnitude <= 0.8f)
                {
                    animationController.ChangeAnimationState(RaymanAnimations.LandingFromVerticalJumpToWalkingCycle2StateName());
                }
                else if (nonInterpolatedInputTranslation.magnitude <= 0.1f)
                {
                    animationController.ChangeAnimationState(RaymanAnimations.LandingFromVerticalJumpToGroundIdleStateName());
                }
                else
                {
                    animationController.ChangeAnimationState(
                        RaymanAnimations.LandingFromRunningJumpAfterAirFlipToRunningCycle4StateName());
                }


                landingWithHelicopter = false;
                landing = false;
                return;
            }

            

            if (nonInterpolatedInputTranslation.magnitude > 0.1f && nonInterpolatedInputTranslation.magnitude <= 0.8f
                && animationController.GetCurrentAnimationState().Equals(RaymanAnimations.IdleAnimationStateName()))
            {
                animationController.ChangeAnimationState(
                    RaymanAnimations.UprisingFromGroundIdleToWalkingCycle1StateName());
                return;
            } else if (nonInterpolatedInputTranslation.magnitude > 0.8f && 
                animationController.GetCurrentAnimationState().Equals(RaymanAnimations.IdleAnimationStateName()))
            {
                animationController.ChangeAnimationState(
                    RaymanAnimations.UprisingFromGroundIdleToRunningCycle4StateName());
                return;
            } else if (nonInterpolatedInputTranslation.magnitude <= 0.1f && 
                animationController.GetCurrentAnimationState().Equals(
                    RaymanAnimations.RunningAnimationStateName()))
            {
                animationController.ChangeAnimationState(
                    RaymanAnimations.ExitingRunningCycle4ToGroundIdleStateName());
                return;
            } else if (nonInterpolatedInputTranslation.magnitude <= 0.1f &&
                animationController.GetCurrentAnimationState().Equals(
                    RaymanAnimations.WalkingCycle2StateName()))
            {
                animationController.ChangeAnimationState(
                    RaymanAnimations.ExitingWalkingCycle2ToGroundIdleStateName());
                return;
            }

            if (nonInterpolatedInputTranslation.magnitude > 0.1f && nonInterpolatedInputTranslation.magnitude <= 0.8f && 
                
                (animationController.GetCurrentAnimationState().Equals(
                    RaymanAnimations.UprisingFromGroundIdleToWalkingCycle1StateName())

                ||
                RaymanAnimationsGroundHelper.IsLandingAnimation(animationController.GetCurrentAnimationState())
                )
                && !animationController.HasAnimationEnded())
            {
                return;
            }

            else if (nonInterpolatedInputTranslation.magnitude > 0.8f &&
                (animationController.GetCurrentAnimationState().Equals(
                    RaymanAnimations.UprisingFromGroundIdleToRunningCycle4StateName())
                ||
                RaymanAnimationsGroundHelper.IsLandingAnimation(animationController.GetCurrentAnimationState()))
                && !animationController.HasAnimationEnded())
            {
                return;
            }

            else if (nonInterpolatedInputTranslation.magnitude < 0.1f &&
                (
                    animationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.ExitingRunningCycle4ToGroundIdleStateName())
                    ||
                    animationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.ExitingWalkingCycle2ToGroundIdleStateName()
                ))
                && !animationController.HasAnimationEnded())
            {
                return;
            }

            //if (Input.GetButtonDown("Fire1"))
            //{
            //    var animator = animatedPlayerPart.GetComponent<Animator>();
            //    Debug.Log("Clips: " + animator.runtimeAnimatorController.animationClips.Count());
            //    animatedPlayerPart.GetComponent<Animator>().Play(RaymanAnimations.ExtraAnimShootingRightHandStateName(), 1);
            //}
            if (inputTranslation.magnitude > 0.1f && inputTranslation.magnitude < 0.4f)
            {
                animationController.ChangeAnimationState(
                    RaymanAnimations.WalkingCycle1StateName());
            }
            else if (inputTranslation.magnitude > 0.1f && inputTranslation.magnitude <= 0.8f)
            {
                animationController.ChangeAnimationState(RaymanAnimations.WalkingCycle2StateName());
            }
            else if (inputTranslation.magnitude > 0.8f)
            {
                animationController.ChangeAnimationState(
                    RaymanAnimations.RunningAnimationStateName());
            }
            else if ((!RaymanAnimationsGroundHelper
                .IsLandingAnimation(animationController.GetCurrentAnimationState())
                && !animationController.GetCurrentAnimationState()
                .Equals(RaymanAnimations.StrafingExitIntoIdleGroundStateName())) || 
                animationController.HasAnimationEnded())
            {
                animationController.ChangeAnimationState(
                    RaymanAnimations.IdleAnimationStateName());
            }
            #endregion
        }
    }
}
