﻿using Assets.Scripts.Animations.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Rules
{
    public class RuleRoofHanging : PlayerMovementRule
    {
        private RaycastHit roofHangingHit;
        private float playerHangingMovingSpeed = 0.05f;

        private bool canControlPlayer = false;

        protected override PlayerMovementRuleEnum GetPlayerMovementRuleEnum()
        {
            return PlayerMovementRuleEnum.RULE_ROOF_HANGING;
        }

        protected override void OnRuleEnter()
        {
            Vector3 roofHangingNormal = roofHangingHit.normal;
            transform.position = roofHangingHit.point + roofHangingNormal * 2.7f;

            animationController.ChangeAnimationStateWithPriority(
                RaymanAnimations.RoofHangingIdleStateName(),
                priority: 5);

            EnablePlayerControlAfterMilliseconds(250);
        }

        protected override void OnRuleFixedUpdate()
        {

            if (canControlPlayer)
            {
                if (!Input.GetButton("Jump"))
                {
                    playerMovementStateInfo.isAbleToTriggerEnteringJumpState = true;
                }

                playerMovementInput.FixedUpdatePlayerMovementInput();
                Vector3 inputTranslation = playerMovementInput.GetTranslation();
                Vector3 translation = playerMovementInput.GetTranslation() * playerHangingMovingSpeed;
                //float axesMagnitude =
                //    Mathf.Clamp01(new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")).magnitude);

                //Vector3 translation =
                //       ((Input.GetAxisRaw("Vertical") * Vector3.forward) +
                //       (Input.GetAxisRaw("Horizontal") * Vector3.right)).normalized
                //       * axesMagnitude * playerHangingMovingSpeed;

                if (Input.GetButton("Jump") && playerMovementStateInfo.isAbleToTriggerEnteringJumpState)
                {
                    rayCollider.DisableRoofHangingCollisionDetectionForMilliseconds(400);
                    playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_AIR;
                    playerMovementStateInfo.isJumping = false;
                    playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
                    playerMovementStateInfo.isAbleToTriggerEnteringJumpState = false;
                    playerMovementStateInfo.movementVelocity.y = -0.01f;
                    animationController.ChangeAnimationStateWithPriority(
                        RaymanAnimations.RoofHangingLeavingStateName(),
                        priority: 8);
                    return;
                }
                
                if (inputTranslation.magnitude > 0.1f)
                {
                    animationController.ChangeAnimationStateWithPriority(
                        RaymanAnimations.RoofHangingGoingForwardStateName(),
                        priority: 5);

                    //if (animationController.GetCurrentAnimationState()
                    //    .Equals(RaymanAnimations.RoofHangingStartingGoingForwardStateName()) &&
                    //    animationController.HasAnimationEnded())
                    //{
                    //    animationController.ChangeAnimationStateWithPriority(
                    //        RaymanAnimations.RoofHangingGoingForwardStateName(),
                    //        RaymanAnimations.RoofHangingGoingForwardStateSpeed(), 5);
                    //}
                    //else if (!animationController.GetCurrentAnimationState()
                    //   .Equals(RaymanAnimations.RoofHangingStartingGoingForwardStateName()) 

                    //   &&

                    //   !animationController.GetCurrentAnimationState()
                    //    .Equals(RaymanAnimations.RoofHangingGoingForwardStateName()))
                    //{
                    //    animationController.ChangeAnimationStateWithPriority(
                    //        RaymanAnimations.RoofHangingStartingGoingForwardStateName(),
                    //        RaymanAnimations.RoofHangingStartingGoingForwardStateSpeed(), 6);
                    //}

                    Vector3 translationInWorldSpace = transform.TransformDirection(translation);

                    animatedPlayerPart.transform.forward = 
                        -Vector3.ProjectOnPlane(translationInWorldSpace, Vector3.up).normalized;
                    transform.forward = playerMovementMetrics.forwardDirection;

                    transform.Translate(translation);
                } else
                {
                    animationController.ChangeAnimationStateWithPriority(
                        RaymanAnimations.RoofHangingIdleStateName(),
                        priority: 5);
                    //if (animationController.GetCurrentAnimationState()
                    //    .Equals(RaymanAnimations.RoofHangingEndingGoingForwardStateName()) &&
                    //    animationController.HasAnimationEnded())
                    //{
                    //    animationController.ChangeAnimationStateWithPriority(
                    //        RaymanAnimations.RoofHangingIdleStateName(),
                    //        RaymanAnimations.RoofHangingIdleStateSpeed(), 5);
                    //} else if 
                    //    (!animationController.GetCurrentAnimationState()
                    //    .Equals(RaymanAnimations.RoofHangingEndingGoingForwardStateName())

                    //    && 

                    //    !animationController.GetCurrentAnimationState()
                    //        .Equals(RaymanAnimations.RoofHangingIdleStateName())) {
                    //    animationController.ChangeAnimationStateWithPriority(
                    //        RaymanAnimations.RoofHangingEndingGoingForwardStateName(),
                    //        RaymanAnimations.RoofHangingEndingGoingForwardStateSpeed(), 6);
                    //}
                }
            }
        }

        private void EnablePlayerControlAfterMilliseconds(int milliseconds)
        {
            StartCoroutine(EnableCanControlPlayerFlagAfterTimePeriod(milliseconds));
        }

        private IEnumerator EnableCanControlPlayerFlagAfterTimePeriod(int milliseconds)
        {
            yield return new WaitForSeconds(milliseconds / 1000f);
            canControlPlayer = true;
        }

        public void ClearRuleState(RaycastHit roofHangingHit)
        {
            this.roofHangingHit = roofHangingHit;
            this.canControlPlayer = false;
        }
    }
}
