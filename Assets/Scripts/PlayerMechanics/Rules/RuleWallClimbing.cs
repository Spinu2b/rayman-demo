﻿using Assets.Scripts.Animations.Models;
using Assets.Scripts.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Rules
{
    public class RuleWallClimbing : PlayerMovementRule
    {
        private RaycastHit wallClimbHit;
        private float playerClimbingSpeed = 0.05f;

        protected override PlayerMovementRuleEnum GetPlayerMovementRuleEnum()
        {
            return PlayerMovementRuleEnum.RULE_WALL_CLIMBING;
        }

        protected override void OnRuleEnter()
        {
            Vector3 wallClimbNormal = wallClimbHit.normal;

            transform.position = wallClimbHit.point;
            //transform.forward = Vector3.up;
            transform.forward = -wallClimbNormal;

            animatedPlayerPart.transform.forward = wallClimbNormal;
            animatedPlayerPart.transform.position = wallClimbHit.point + wallClimbNormal * 1.2f;
            

            animationController.ChangeAnimationStateWithPriority(
                RaymanAnimations.WallClimbingIdleStateName(), priority: 5);
        }

        protected override void OnRuleFixedUpdate()
        {
            if (!Input.GetButton("Jump"))
            {
                playerMovementStateInfo.isAbleToTriggerEnteringJumpState = true;
            }

            playerMovementInput.FixedUpdatePlayerMovementInput();
            Vector3 inputTranslation = playerMovementInput.GetTranslationUpRight();
            Vector3 translation = playerMovementInput.GetTranslationUpRight() * playerClimbingSpeed;

            //float axesMagnitude =
            //    Mathf.Clamp01(new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")).magnitude);

            //Vector3 translation =
            //       ((Input.GetAxisRaw("Vertical") * Vector3.up) +
            //       (Input.GetAxisRaw("Horizontal") * Vector3.right)).normalized
            //       * axesMagnitude * playerClimbingSpeed;

            Vector3 translationInWorldSpace = transform.TransformDirection(translation);

            Debug.DrawRay(transform.position, transform.forward);

            if (Input.GetButton("Jump") && 
                playerMovementStateInfo.isAbleToTriggerEnteringJumpState)
            {
                transform.position = animatedPlayerPart.transform.position;
                animatedPlayerPart.transform.position = transform.position;
                rayCollider.DisableWallClimbCollisionDetectionForMilliseconds(400);

                // allow anims to be interrupted by whatever next rule will bring
                // since these anims from wall climbing are no longer relevant from this point on
                animationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.WallClimbingIdleStateName(), 1);

                animationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.WallClimbingUpStateName(), 1);

                animationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.WallClimbingDownStateName(), 1);

                animationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.WallClimbingLeftStateName(), 1);
                animationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.WallClimbingRightStateName(), 1);

                animationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.WallClimbingUpRightStateName(), 1);
                animationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.WallClimbingUpLeftStateName(), 1);

                animationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.WallClimbingDownRightStateName(), 1);
                animationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.WallClimbingDownLeftStateName(), 1);


                playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_AIR;
                playerMovementStateInfo.isJumping = true;
                playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
                playerMovementStateInfo.isAbleToTriggerEnteringJumpState = false;
                return;
            }

            if (inputTranslation.magnitude < 0.1f)
            {
                animationController.ChangeAnimationStateWithPriority(
                    RaymanAnimations.WallClimbingIdleStateName(),
                    priority: 5);
            } else
            {
                if (Vector3.Angle(Vector3.up, translation) < 20f && 
                    WillStillCollideWithWallClimbAndDoesNotCollideWithWall(translationInWorldSpace))
                {
                    // going straight up
                    animationController.ChangeAnimationStateWithPriority(
                        RaymanAnimations.WallClimbingUpStateName(),
                        priority: 5);

                    transform.Translate(translation);
                }
                else if (Vector3.Angle(Vector3.up, translation) < 80f && 
                    WillStillCollideWithWallClimbAndDoesNotCollideWithWall(translationInWorldSpace))
                {
                    // going diagonal up
                    if (Vector3.Angle(translation, Vector3.right) < 90f)
                    {
                        // going diagonal up right
                        animationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.WallClimbingUpRightStateName(),
                            priority: 5);
                    } else
                    {
                        // going diagonal up left
                        animationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.WallClimbingUpLeftStateName(),
                            priority: 5);
                    }
                    
                    transform.Translate(translation);
                }
                else if (Vector3.Angle(Vector3.up, translation) < 90f && 
                    WillStillCollideWithWallClimbAndDoesNotCollideWithWall(translationInWorldSpace))
                {
                    // going left or right
                    if (Vector3.Angle(translation, Vector3.right) < 90f)
                    {
                        // going right
                        animationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.WallClimbingRightStateName(),
                            priority: 5);
                    }
                    else
                    {
                        // going left
                        animationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.WallClimbingLeftStateName(),
                            priority: 5);
                    }
                }
                else if (Vector3.Angle(Vector3.up, translation) < 160f && 
                    WillStillCollideWithWallClimbAndDoesNotCollideWithWall(translationInWorldSpace))
                {
                    //going diagonal down
                    if (Vector3.Angle(translation, Vector3.right) < 90f)
                    {
                        // going diagonal down right
                        animationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.WallClimbingDownRightStateName(),
                            priority: 5);
                    }
                    else
                    {
                        // going diagonal down left
                        animationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.WallClimbingDownLeftStateName(),
                            priority: 5);
                    }

                    transform.Translate(translation);
                }
                else if (WillStillCollideWithWallClimbAndDoesNotCollideWithWall(translationInWorldSpace))
                {
                    //going straight down
                    animationController.ChangeAnimationStateWithPriority(
                        RaymanAnimations.WallClimbingDownStateName(),
                        priority: 5);
                    transform.Translate(translation);
                } else
                {
                    animationController.ChangeAnimationStateWithPriority(
                        RaymanAnimations.WallClimbingIdleStateName(),
                        priority: 5);
                }
            }            
        }

        private bool WillStillCollideWithWallClimbAndDoesNotCollideWithWall(Vector3 translationInWorldSpace)
        {
            float wallCollisionCheckRayLength = 0.3f;

            Debug.DrawRay(animatedPlayerPart.transform.position + translationInWorldSpace,
                animatedPlayerPart.transform.right * wallCollisionCheckRayLength);
            Debug.DrawRay(animatedPlayerPart.transform.position + translationInWorldSpace,
                -animatedPlayerPart.transform.right * wallCollisionCheckRayLength);
            Debug.DrawRay(animatedPlayerPart.transform.position + translationInWorldSpace,
                animatedPlayerPart.transform.up * wallCollisionCheckRayLength);
            Debug.DrawRay(animatedPlayerPart.transform.position + translationInWorldSpace,
                -animatedPlayerPart.transform.up * wallCollisionCheckRayLength);

            bool willCollideWithWall =
                Physics.Raycast(
                    animatedPlayerPart.transform.position + translationInWorldSpace,
                        animatedPlayerPart.transform.right, wallCollisionCheckRayLength) ||
                Physics.Raycast(
                    animatedPlayerPart.transform.position + translationInWorldSpace,
                        animatedPlayerPart.transform.up, wallCollisionCheckRayLength) ||
                Physics.Raycast(
                    animatedPlayerPart.transform.position + translationInWorldSpace,
                        -animatedPlayerPart.transform.up, wallCollisionCheckRayLength) ||
                Physics.Raycast(
                    animatedPlayerPart.transform.position + translationInWorldSpace,
                        -animatedPlayerPart.transform.right, wallCollisionCheckRayLength);

            Vector3 wallClimbNormal = wallClimbHit.normal;

            float wallClimbCheckRayLength = 2f;
            float wallClimbCheckRayDepth = 1.5f;

            RaycastHit wallClimbCollideHit = new RaycastHit();

            Debug.DrawRay(animatedPlayerPart.transform.position + translationInWorldSpace.normalized
                * wallClimbCheckRayLength,
               (-wallClimbNormal) * wallClimbCheckRayDepth);

            bool willCollideWithWallClimb = Physics.Raycast(
                animatedPlayerPart.transform.position + translationInWorldSpace.normalized * wallClimbCheckRayLength,
                -wallClimbNormal, out wallClimbCollideHit, wallClimbCheckRayDepth);

            willCollideWithWallClimb = willCollideWithWallClimb &&
                wallClimbCollideHit.collider.gameObject.tag.Equals(Tags.wallClimbTag);

            return willCollideWithWallClimb && !willCollideWithWall;
        }

        public void ClearRuleState(RaycastHit wallClimbHit)
        {
            this.wallClimbHit = wallClimbHit;
        }
    }
}
