﻿using Assets.Scripts.HUD.Targetting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.PlayerMechanics.Rules.Base
{
    public interface IStrafingRuleUtils
    {
        public ShootSightDirection GetShootSightDirection();
    }
}
