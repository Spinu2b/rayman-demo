﻿using Assets.Scripts.Animations;
using Assets.Scripts.Animations.Models;
using Assets.Scripts.ScenePreparation.LedgeGrabbing.Structs;
using Assets.Scripts.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Rules
{
    public class RuleLedgeGrabbing : PlayerMovementRule
    {
        private RaycastHit ledgeColliderHit;
        private bool canClimbTheLedge = false;
        private bool isAllowedToClimb = false;

        private Vector3 startingHangingPosition;
        private Vector3 endingClimbingPosition;
        
        public void ClearRuleState(RaycastHit ledgeColliderHit)
        {
            this.ledgeColliderHit = ledgeColliderHit;
            canClimbTheLedge = false;
            isAllowedToClimb = false;
        }

        protected override PlayerMovementRuleEnum GetPlayerMovementRuleEnum()
        {
            return PlayerMovementRuleEnum.RULE_LEDGE_GRABBING; 
        }

        protected override void OnRuleExit()
        {
            //animatedPlayerPart.transform.position = transform.position;
        }

        protected override void OnRuleEnter()
        {
            LedgeColliderInfo ledgeColliderInfo = 
                ledgeColliderHit.collider.gameObject.GetComponent<LedgeColliderInfo>();
            Vector3 grabNormal = ledgeColliderInfo.normal;
            Vector3 nearestPointOnEdge = LedgeGrabbingHelper.GetNearestPointOnEdgeFrom(
                transform.position, ledgeColliderInfo.edgePointA, ledgeColliderInfo.edgePointB);

            animatedPlayerPart.transform.forward = grabNormal;
            transform.position = nearestPointOnEdge;
            animatedPlayerPart.transform.position = nearestPointOnEdge + Vector3.down * 1.78f + grabNormal.normalized * 0.82f;

            DelayAllowingPlayerToClimbMilliseconds(300);

            animationController.ChangeAnimationStateWithPriority(
                RaymanAnimations.StartingHangingOnLedgeFromFallingStateName(),
                priority: 5);
        }

        private IEnumerator AllowPlayerToClimbAfterMilliseconds(int milliseconds)
        {
            yield return new WaitForSeconds(milliseconds / 1000f);
            isAllowedToClimb = true;
        }

        private void DelayAllowingPlayerToClimbMilliseconds(int milliseconds)
        {
            StartCoroutine(AllowPlayerToClimbAfterMilliseconds(milliseconds));
        }

        protected override void OnRuleFixedUpdate()
        {

            if (animationController.GetCurrentAnimationState().Equals(
                RaymanAnimations.StartingHangingOnLedgeFromFallingStateName()) &&
                animationController.HasAnimationEnded())
            {
                animationController.ChangeAnimationState(
                    RaymanAnimations.HangingOnLedgeStateName());
                canClimbTheLedge = true;
                return;
            }

            if (!Input.GetButton("Jump"))
            {
                playerMovementStateInfo.isAbleToTriggerEnteringJumpState = true;
            }

            LedgeColliderInfo ledgeColliderInfo =
                    ledgeColliderHit.collider.gameObject.GetComponent<LedgeColliderInfo>();
            Vector3 grabNormal = ledgeColliderInfo.normal;

            if (Input.GetButton("Jump") && playerMovementStateInfo.isAbleToTriggerEnteringJumpState)
            {
                // leaving ledge with jump
                Vector3 nearestPointOnEdge = LedgeGrabbingHelper.GetNearestPointOnEdgeFrom(
                    transform.position, ledgeColliderInfo.edgePointA, ledgeColliderInfo.edgePointB);

                animatedPlayerPart.transform.position = transform.position;

                transform.position = nearestPointOnEdge + Vector3.down * 1.78f + grabNormal.normalized * 0.82f;
                rayCollider.DisableLedgeGrabColliderDetectionForMilliseconds(800);

                // allow starting hanging on ledge anim to be interrupted if player 
                // immediately pressed jump button while this animation is still being played
                animationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.StartingHangingOnLedgeFromFallingStateName(), 1);

                animationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.ClimbingFromHangingOnLedgeStateName(), 1);

                playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_AIR;
                playerMovementStateInfo.isJumping = true;
                playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
                playerMovementStateInfo.isAbleToTriggerEnteringJumpState = false;

                //Vector3 translationInLocalSpace = transform.InverseTransformDirection(translation);
                //playerMovementStateInfo.movementVelocity.x = translationInLocalSpace.x;
                //playerMovementStateInfo.movementVelocity.z = translationInLocalSpace.z;
                return;
            }

            if (canClimbTheLedge)
            {
                float axesMagnitude =
                Mathf.Clamp01(new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")).magnitude);

                Vector3 translation =
                       transform.TransformDirection((Input.GetAxisRaw("Vertical") * Vector3.forward).normalized +
                       (Input.GetAxisRaw("Horizontal") * Vector3.right).normalized).normalized
                       * axesMagnitude;

                if (Vector3.Angle(translation, -grabNormal) < 80f && translation.magnitude > 0.1f && isAllowedToClimb)
                {
                    //if deciding to climb the ledge
                    canClimbTheLedge = false;
                    animationController.ChangeAnimationStateWithPriority(
                        RaymanAnimations.ClimbingFromHangingOnLedgeStateName(),
                        priority: 5);

                    Vector3 nearestPointOnEdge = LedgeGrabbingHelper.GetNearestPointOnEdgeFrom(
                        transform.position, ledgeColliderInfo.edgePointA, ledgeColliderInfo.edgePointB);

                    startingHangingPosition = nearestPointOnEdge + Vector3.down * 1.78f + grabNormal.normalized * 0.82f;
                    endingClimbingPosition = transform.position + ((-grabNormal.normalized) * 1.18f);
                    return;
                } else if (!Input.GetButton("Jump") && Vector3.Angle(translation, -grabNormal) > 100f &&
                    translation.magnitude > 0.1f)
                {
                    //if deciding to leave the ledge and fall down
                    Vector3 nearestPointOnEdge = LedgeGrabbingHelper.GetNearestPointOnEdgeFrom(
                        transform.position, ledgeColliderInfo.edgePointA, ledgeColliderInfo.edgePointB);

                    animatedPlayerPart.transform.position = transform.position;

                    transform.position = nearestPointOnEdge + Vector3.down * 1.78f + grabNormal.normalized * 0.82f;
                    rayCollider.DisableLedgeGrabColliderDetectionForMilliseconds(800);

                    playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_AIR;
                    playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
                    playerMovementStateInfo.isAbleToTriggerEnteringJumpState = false;
                    playerMovementStateInfo.movementVelocity.y = 0;
                    return;
                } 
                // otherwise ignore all Player's intentions, inputs etc for now :)
            }
        }

        protected override void OnRuleUpdate()
        {
            if (animationController.GetCurrentAnimationState()
                .Equals(RaymanAnimations.ClimbingFromHangingOnLedgeStateName()))
            {
                if (!animationController.HasAnimationEnded())
                {
                    // still climbing, adjust Rayman's model position in-between
                    // since climbing animation has positioning offset...
                    float animationNormalizedTime = animationController.GetAnimationNormalizedTime();
                    animatedPlayerPart.transform.position =
                        Vector3.Lerp(
                            startingHangingPosition,
                            endingClimbingPosition,
                            animationNormalizedTime);

                    return;
                }
                else
                {
                    transform.position = endingClimbingPosition;
                    animatedPlayerPart.transform.position = transform.position;
                    playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_GROUND;
                    playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
                    playerMovementStateInfo.isAbleToTriggerEnteringJumpState = false;
                    // climbing ended, move to GROUND rule with 'clear/canonical' state
                    return;
                }
            }
        }
    }
}
