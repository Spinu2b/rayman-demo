﻿using Assets.Scripts.Animations.Models;
using Assets.Scripts.Common;
using Assets.Scripts.HUD.Targetting;
using Assets.Scripts.PlayerMechanics.Camera.Rules;
using Assets.Scripts.PlayerMechanics.Rules.Base;
using Assets.Scripts.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Rules
{
    public class RuleStrafingGround : GroundRulesBase, IStrafingRuleUtils
    {
        private float playerStrafingSpeed = 0.12f;

        public ShootSightDirection GetShootSightDirection()
        {
            Vector3 translation = playerMovementInput.GetTranslation();
            float angle = Vector3.SignedAngle(Vector3.forward, translation.normalized, Vector3.up);
            if (translation.magnitude < 0.95f || (angle >= -10 && angle <= 10) || (angle >= 170 || angle <= -170))               
            {
                // straight
                return ShootSightDirection.DOWN_ARROW_STRAIGHT;
            }
            else if (angle > 10)
            {
                // right
                return ShootSightDirection.DOWN_ARROW_RIGHT;
            } else
            {
                // left
                return ShootSightDirection.DOWN_ARROW_LEFT;
            }
        }

        protected override PlayerMovementRuleEnum GetPlayerMovementRuleEnum()
        {
            return PlayerMovementRuleEnum.RULE_STRAFING_GROUND;
        }

        protected override void OnRuleEnter() 
        {
            animationController.ChangeAnimationState(
                RaymanAnimations.StrafingEnterFromIdleGroundStateName());
        }

        protected override void OnRuleFixedUpdate()
        {
            #region Rule
            if (!Input.GetButton(KeyControls.strafingButton) && !playerGroundRollingAspect.IsPerformingGroundRoll())
            {
                // exit strafing ground rule right away and go into regular ground rule
                GetComponent<RuleGround>().SetTransitionData(fromGroundStrafing: true);
                playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_GROUND;
                playerMovementMetrics.cameraRule = CameraRuleEnum.CAMERA_FOLLOW;
                return;
            }            

            HandleShooting();
            rayCollider.AlignOnTopOfTheGround();

            playerMovementInput.FixedUpdatePlayerMovementInput();
            Vector3 inputTranslation = playerMovementInput.GetTranslation();
            Vector3 translation = playerMovementInput.GetTranslation() * playerStrafingSpeed;

            Vector3 translationDirectionInWorldSpace =
                    (Quaternion.AngleAxis(playerMovementMetrics.angleFromAbsoluteForward, Vector3.up)
                    * translation).normalized;

            playerMovementStateInfo.movementVelocity.y = 0;

            if (inputTranslation.magnitude < 0.1)
            {
                float movementSmoothingInterpolation = 0.6f;
                playerMovementStateInfo.movementVelocity.x =
                    Mathf.Lerp(playerMovementStateInfo.movementVelocity.x, 0, movementSmoothingInterpolation);
                playerMovementStateInfo.movementVelocity.z =
                    Mathf.Lerp(playerMovementStateInfo.movementVelocity.z, 0, movementSmoothingInterpolation);
            }
            else
            {
                //float movementSmoothingInterpolation = 0.25f;
                float movementSmoothingInterpolation = 0.25f;
                playerMovementStateInfo.movementVelocity.x =
                    Mathf.Lerp(playerMovementStateInfo.movementVelocity.x, translation.x, movementSmoothingInterpolation);
                playerMovementStateInfo.movementVelocity.z =
                    Mathf.Lerp(playerMovementStateInfo.movementVelocity.z, translation.z, movementSmoothingInterpolation);
            }            

            if (inputTranslation.magnitude > 0.1)
            {
                transform.forward = playerMovementMetrics.forwardDirection;                

                if (Input.GetButton(KeyControls.groundRollButton)
                && (!playerGroundRollingAspect.IsPerformingGroundRoll() && !playerGroundRollingAspect.HasEndedGroundRoll()))
                {
                    playerGroundRollingAspect.StartPerformingStrafingGroundRoll(
                        -animatedPlayerPart.transform.forward.normalized,
                        -animatedPlayerPart.transform.right.normalized,
                        translationDirectionInWorldSpace.normalized);
                    return;
                }
            }

            if (targettingAspect.currentTarget)
            {
                Vector3 targetLookingDirection =
                    targettingAspect.GetPlayerLookingDirectionTowardsTarget(
                        animatedPlayerPart.transform.position);
                animatedPlayerPart.transform.forward = -targetLookingDirection;
                playerMovementMetrics.cameraRule = CameraRuleEnum.CAMERA_TARGET_PLAYER_BACK_STRAFING;
            } else
            {
                playerMovementMetrics.cameraRule = CameraRuleEnum.CAMERA_STRAFING;
            }

            if (playerGroundRollingAspect.IsPerformingGroundForwardRoll())
            {
                playerMovementStateInfo.movementVelocity =
                    transform.InverseTransformDirection(
                        playerGroundRollingAspect.GetMovementVelocityAppropriateForCurrentForwardRollCycle());
            } else if (playerGroundRollingAspect.IsPerformingGroundLeftOrRightRoll())
            {
                playerMovementStateInfo.movementVelocity =
                    transform.InverseTransformDirection(
                        playerGroundRollingAspect.GetMovementVelocityAppropriateForCurrentLeftRightRollCycle());
            } else if (playerGroundRollingAspect.IsPerformingGroundBackwardsRoll())
            {
                playerMovementStateInfo.movementVelocity =
                    transform.InverseTransformDirection(
                        playerGroundRollingAspect.GetMovementVelocityAppropriateForCurrentBackwardsRollCycle());
            }

            playerMovementStateInfo.movementVelocity = transform.InverseTransformDirection
                (WallCollisionHelper
                   .GetMovementVelocityConsideringWallCollisions(
                    gameObject,
                    rayCollider,
                    -animatedPlayerPart.transform.forward,
                    transform.TransformDirection(playerMovementStateInfo.movementVelocity)));

            transform.Translate(playerMovementStateInfo.movementVelocity);

            if (Input.GetButton("Jump")
                && rayCollider.IsHittingGround()
                && playerMovementStateInfo.isAbleToTriggerEnteringJumpState)
            {
                playerMovementStateInfo.isJumping = true;
                playerMovementStateInfo.isAbleToTriggerEnteringJumpState = false;
                playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
                playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_STRAFING_AIR;
                playerGroundRollingAspect.BreakTheRollCycle();
                //playerMovementMetrics.cameraRule = CameraRuleEnum.CAMERA_FOLLOW;
                return;
            }

            if (!rayCollider.IsHittingGround())
            {
                playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_STRAFING_AIR;
                //playerMovementMetrics.cameraRule = CameraRuleEnum.CAMERA_FOLLOW;
                playerGroundRollingAspect.BreakTheRollCycle();
                return;
            }

            if (!Input.GetButton("Jump"))
            {
                playerMovementStateInfo.isAbleToTriggerEnteringJumpState = true;
                playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
            }

            #endregion
            #region Animation
            if (!playerGroundRollingAspect.IsPerformingGroundRoll())
            {
                if (inputTranslation.magnitude > 0.1)
                {
                    float angleBetweenTranslationAndAnimatedPlayerPartForward =
                        Vector3.Angle(
                            -animatedPlayerPart.transform.forward.normalized, translationDirectionInWorldSpace);

                    float angleBetweenTranslationAndAnimatedPlayerPartRight =
                        Vector3.Angle(
                            -animatedPlayerPart.transform.right.normalized, translationDirectionInWorldSpace
                            );
                    if (angleBetweenTranslationAndAnimatedPlayerPartForward < 20f)
                    {
                        // going straight forward
                        animationController.ChangeAnimationState(
                            RaymanAnimations.StrafingForwardStateName());
                    }
                    else if (angleBetweenTranslationAndAnimatedPlayerPartForward < 80f)
                    {
                        // going forward left or forward right
                        if (angleBetweenTranslationAndAnimatedPlayerPartRight < 90f)
                        {
                            // going forward right
                            animationController.ChangeAnimationState(
                                RaymanAnimations.StrafingForwardRightStateName());
                        }
                        else
                        {
                            // going forward left
                            animationController.ChangeAnimationState(
                                RaymanAnimations.StrafingForwardLeftStateName());
                        }
                    }
                    else if (angleBetweenTranslationAndAnimatedPlayerPartForward < 90f)
                    {
                        // going left or right
                        if (angleBetweenTranslationAndAnimatedPlayerPartRight < 90f)
                        {
                            // going right
                            animationController.ChangeAnimationState(
                                RaymanAnimations.StrafingRightStateName());
                        }
                        else
                        {
                            // going left
                            animationController.ChangeAnimationState(
                                RaymanAnimations.StrafingLeftStateName());
                        }
                    }
                    else if (angleBetweenTranslationAndAnimatedPlayerPartForward < 160f)
                    {
                        // going backwards left or backwards right
                        if (angleBetweenTranslationAndAnimatedPlayerPartRight < 90f)
                        {
                            // going backwards right
                            animationController.ChangeAnimationState(
                                RaymanAnimations.StrafingBackwardsRightStateName());
                        }
                        else
                        {
                            // going backwards left
                            animationController.ChangeAnimationState(
                                RaymanAnimations.StrafingBackwardsLeftStateName());
                        }
                    }
                    else
                    {
                        // going straight backwards
                        animationController.ChangeAnimationState(
                            RaymanAnimations.StrafingBackwardsStateName());
                    }

                    //animationController.ChangeAnimationState(
                    //    RaymanAnimations.StrafingIdleStateName());
                }
                else if (!animationController.GetCurrentAnimationState()
                    .Equals(RaymanAnimations.StrafingEnterFromIdleGroundStateName()) 
                    || animationController.HasAnimationEnded())
                {
                    animationController.ChangeAnimationState(
                        RaymanAnimations.StrafingIdleStateName());
                }
            }            
            #endregion
        }

    }
}
