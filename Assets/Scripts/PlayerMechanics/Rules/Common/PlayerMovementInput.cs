﻿using Assets.Scripts.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Rules.Common
{
    public static class PlayerMovementVectorRotationWithLengthInterpolationHelper
    {
        public static Vector3 InterpolationRotationWithLength(
            Vector3 forward, Vector3 start, Vector3 end, float t)
        {
            float interpolatedLength = Mathf.Lerp(start.magnitude, end.magnitude, t);

            float angle = Vector3.SignedAngle(forward.normalized, end.normalized, Vector3.up);

            float angleFromForwardToStart = Vector3.SignedAngle(forward, start, Vector3.up);
            float angleBetweenStartAndEnd = Vector3.SignedAngle(start, end, Vector3.up);

            return Quaternion.AngleAxis(angleFromForwardToStart + 
                (angleBetweenStartAndEnd * t), Vector3.up) * Vector3.forward * interpolatedLength;
        }
    }

    public class PlayerMovementInput : MonoBehaviour
    {
        private Vector3 currentTranslation;
        private float axesMagnitude = 0.0f;
        private Vector3 zeroTranslation = Vector3.zero;
        private float horizontalAxisRaw = 0.0f;

        private Vector3 translationFromUpdate;
        private float axesMagnitudeFromUpdate;
        private float horizontalAxisRawFromUpdate;

        private Vector3 currentTranslationFromUpdate;
        

        private void Update()
        {
            axesMagnitudeFromUpdate =
                Mathf.Clamp01(new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")).magnitude);

            horizontalAxisRawFromUpdate = Input.GetAxisRaw("Horizontal");

            translationFromUpdate = ((Input.GetAxisRaw("Vertical") * Vector3.forward) +
                   (Input.GetAxisRaw("Horizontal") * Vector3.right)).normalized
                   * axesMagnitudeFromUpdate;

            if (Input.GetKey(KeyControls.walkingButtonKeyboard))
            {
                translationFromUpdate = translationFromUpdate / 2f;
            }

            currentTranslationFromUpdate =
                PlayerMovementVectorRotationWithLengthInterpolationHelper.InterpolationRotationWithLength(
                    Vector3.forward, currentTranslationFromUpdate, translationFromUpdate,
                    //0.25f);
                    (0.25f * Time.deltaTime)/(Time.fixedDeltaTime));
        }

        public void FixedUpdatePlayerMovementInput()
        {
            //axesMagnitude =
            //    Mathf.Clamp01(new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")).magnitude);

            //this.horizontalAxisRaw = Input.GetAxisRaw("Horizontal");

            //Vector3 translation =
            //       ((Input.GetAxisRaw("Vertical") * Vector3.forward) +
            //       (Input.GetAxisRaw("Horizontal") * Vector3.right)).normalized
            //       * axesMagnitude;

            //currentTranslation = 
            //    PlayerMovementVectorRotationWithLengthInterpolationHelper.InterpolationRotationWithLength(
            //        Vector3.forward, currentTranslation, translation, 0.25f);
        }

        public Vector3 GetTranslationUpRight()
        {
            return Quaternion.AngleAxis(-90f, Vector3.right) * currentTranslationFromUpdate;
        }

        public float GetHorizontalAxisRaw()
        {
            return horizontalAxisRawFromUpdate;
        }

        public Vector3 GetNonInterpolatedTranslation()
        {
            return translationFromUpdate;
        }

        public Vector3 GetTranslation()
        {
            return currentTranslationFromUpdate;
        }
    }
}
