﻿using Assets.Scripts.Animations.Models;
using Assets.Scripts.Common;
using Assets.Scripts.HUD.Targetting;
using Assets.Scripts.PlayerMechanics.Camera.Rules;
using Assets.Scripts.PlayerMechanics.Rules.Base;
using Assets.Scripts.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Rules
{
    public class RuleStrafingAir : PlayerMovementRule, IStrafingRuleUtils
    {
        private float playerInAirStrafingSpeed = 0.16f;
        private float playerJumpSpeed = 0.28f;

        protected override PlayerMovementRuleEnum GetPlayerMovementRuleEnum()
        {
            return PlayerMovementRuleEnum.RULE_STRAFING_AIR;
        }

        protected override void OnRuleEnter()
        {
            playerMovementStateInfo.isAbleToTriggerEnteringJumpState = false;
            playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
        }

        protected override void OnRuleExit()
        {
            if (!playerMovementStateInfo.currentRule.Equals(PlayerMovementRuleEnum.RULE_AIR))
            {
                playerMovementStateInfo.isJumping = false;
                playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
                playerMovementStateInfo.isAbleToTriggerEnteringJumpState = false;
                playerMovementStateInfo.isHelicopter = false;
                helicopterSound.Pause();
            }
        }

        protected float CalculateAngleBetweenTranslationInWorldSpaceAndPlayerAnimatedPartForward(
            Vector3 translationInWorldSpace)
        {
            return Vector3.Angle(translationInWorldSpace.normalized, -animatedPlayerPart.transform.forward.normalized);
        }

        protected float CalculateAngleBetweenTranslationInWorldSpaceAndPlayerAnimatedPartRight(
            Vector3 translationInWorldSpace)
        {
            return Vector3.Angle(translationInWorldSpace.normalized, -animatedPlayerPart.transform.right.normalized);
        }

        protected override void OnRuleFixedUpdate()
        {
            if (!Input.GetButton(KeyControls.strafingButton))
            {
                // exit strafing air rule right away and go into regular air rule
                playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_AIR;
                playerMovementMetrics.cameraRule = CameraRuleEnum.CAMERA_FOLLOW;
                return;
            }

            HandleShooting();
            playerMovementInput.FixedUpdatePlayerMovementInput();
            Vector3 inputTranslation = playerMovementInput.GetTranslation();
            Vector3 translation = playerMovementInput.GetTranslation() * playerInAirStrafingSpeed;

            //float axesMagnitude =
            //    Mathf.Clamp01(new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")).magnitude);

            //Vector3 translation =
            //       ((Input.GetAxisRaw("Vertical") * Vector3.forward) +
            //       (Input.GetAxisRaw("Horizontal") * Vector3.right)).normalized
            //       * axesMagnitude * playerInAirStrafingSpeed;

            Vector3 translationDirectionInWorldSpace =
                    (Quaternion.AngleAxis(playerMovementMetrics.angleFromAbsoluteForward, Vector3.up)
                    * translation).normalized;

            if (inputTranslation.magnitude < 0.1)
            {
                float movementSmoothingInterpolation = 0.6f;
                playerMovementStateInfo.movementVelocity.x =
                    Mathf.Lerp(playerMovementStateInfo.movementVelocity.x, 0, movementSmoothingInterpolation);
                playerMovementStateInfo.movementVelocity.z =
                    Mathf.Lerp(playerMovementStateInfo.movementVelocity.z, 0, movementSmoothingInterpolation);
            }
            else
            {
                //float movementSmoothingInterpolation = 0.25f;
                float movementSmoothingInterpolation = 0.25f;
                playerMovementStateInfo.movementVelocity.x =
                    Mathf.Lerp(playerMovementStateInfo.movementVelocity.x, translation.x, movementSmoothingInterpolation);
                playerMovementStateInfo.movementVelocity.z =
                    Mathf.Lerp(playerMovementStateInfo.movementVelocity.z, translation.z, movementSmoothingInterpolation);
            }

            if (inputTranslation.magnitude > 0.1)
            {
                transform.forward = playerMovementMetrics.forwardDirection;
                //animatedPlayerPart.transform.forward = -translationDirectionInWorldSpace;
            }

            if (targettingAspect.currentTarget)
            {
                Vector3 targetLookingDirection = 
                    targettingAspect.GetPlayerLookingDirectionTowardsTarget(
                        animatedPlayerPart.transform.position);
                //transform.forward = targetLookingDirection;
                animatedPlayerPart.transform.forward = -targetLookingDirection;
                playerMovementMetrics.cameraRule = CameraRuleEnum.CAMERA_TARGET_PLAYER_BACK_STRAFING;
            } else
            {
                playerMovementMetrics.cameraRule = CameraRuleEnum.CAMERA_STRAFING;
            }

            playerMovementStateInfo.movementVelocity = transform.InverseTransformDirection
                (WallCollisionHelper
                   .GetMovementVelocityConsideringWallCollisions(
                    gameObject,
                    rayCollider,
                    -animatedPlayerPart.transform.forward,
                    transform.TransformDirection(playerMovementStateInfo.movementVelocity)));

            playerMovementStateInfo.movementVelocity = transform.InverseTransformDirection(
                rayCollider.AdjustVelocityIfIsHittingTheCeiling(
                    transform.TransformDirection(playerMovementStateInfo.movementVelocity)));

            if (playerMovementStateInfo.isJumping)
            {
                playerMovementStateInfo.isJumping = false;
                jumpSound.Play();
                playerMovementStateInfo.movementVelocity.y = playerJumpSpeed;

                Vector3 flatVelocity = Vector3.ProjectOnPlane(playerMovementStateInfo.movementVelocity, Vector3.up);

                if (inputTranslation.magnitude > 0.1f)
                {
                    float angleBetweenPlayerForwardAndTranslation =
                        CalculateAngleBetweenTranslationInWorldSpaceAndPlayerAnimatedPartForward(
                            translationDirectionInWorldSpace);

                    float angleBetweenPlayerRightAndTranslation =
                        CalculateAngleBetweenTranslationInWorldSpaceAndPlayerAnimatedPartRight(
                            translationDirectionInWorldSpace
                            );

                    if (angleBetweenPlayerForwardAndTranslation < 45f)
                    {
                        // jumping forward
                        animationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.JumpStartBounceFromGroundForwardStrafingAirAnimationStateName(),
                            priority: 3);
                    } else if (angleBetweenPlayerForwardAndTranslation < 120f)
                    {
                        // jumping left or right
                        if (angleBetweenPlayerRightAndTranslation < 90f)
                        {
                            // jumping right
                            animationController.ChangeAnimationStateWithPriority(
                                RaymanAnimations.JumpStartBounceFromGroundRightStrafingAirAnimationStateName(),
                                priority: 3);
                        }
                        else
                        {
                            // jumping left
                            animationController.ChangeAnimationStateWithPriority(
                                RaymanAnimations.JumpStartBounceFromGroundLeftStrafingAirAnimationStateName(),
                                priority: 3);
                        }
                    } else
                    {
                        // jumping backwards
                        animationController.ChangeAnimationStateWithPriority(
                            RaymanAnimations.JumpStartBounceFromGroundBackwardsStrafingAirAnimationStateName(),
                            priority: 3);
                    }

                    //animationController.ChangeAnimationStateWithPriority(
                    //    RaymanAnimations.JumpStartRunningBounceFromGroundAnimationStateName(),
                    //    priority: 3);
                }
                else
                {
                    // jumping straight up
                    animationController.ChangeAnimationStateWithPriority(
                        RaymanAnimations.JumpStartBounceFromGroundAnimationStateName(),
                        priority: 3);
                }
            }

            if (playerMovementStateInfo.movementVelocity.y > -PlayerMovementMetrics.limitFallSpeed)
            {
                if (!playerMovementStateInfo.isHelicopter)
                {
                    playerMovementStateInfo.movementVelocity.y -= PlayerMovementMetrics.gravityAcceleration;
                }
            }

            if (playerMovementStateInfo.isHelicopter)
            {
                playerMovementStateInfo.movementVelocity.y = -PlayerMovementMetrics.limitFallSpeedHelicopter;
            }

            transform.Translate(playerMovementStateInfo.movementVelocity);

            if (
                (
                    animationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.JumpStartBounceFromGroundForwardStrafingAirAnimationStateName())
                    ||
                    animationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.JumpStartBounceFromGroundBackwardsStrafingAirAnimationStateName())
                    ||
                    animationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.JumpStartBounceFromGroundLeftStrafingAirAnimationStateName())
                    ||
                    animationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.JumpStartBounceFromGroundRightStrafingAirAnimationStateName())

                ) &&
                  animationController.HasAnimationEnded())
            {
                if (playerMovementStateInfo.movementVelocity.y < 0.1f)
                {
                    //animationController.ChangeAnimationStateWithPriority(
                    //    RaymanAnimations.AirRollFromJumpStartRunningBounceFromGroundAnimationStateName(),
                    //    RaymanAnimations.AirRollFromJumpStartRunningBounceFromGroundAnimationStateSpeed(), 2);


                    // change into respective roll/extremum of the animation sequence in the air

                    if (animationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.JumpStartBounceFromGroundForwardStrafingAirAnimationStateName()))
                    {
                        // extremum when strafing forward
                        animationController.ChangeAnimationState(
                            RaymanAnimations.StrafingAirJumpExtremumAirRollForwardAnimationStateName());
                    } else if (animationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.JumpStartBounceFromGroundBackwardsStrafingAirAnimationStateName()))
                    {
                        // backflip when strafing backwards
                        animationController.ChangeAnimationState(
                            RaymanAnimations.StrafingAirJumpExtremumAirRollBackwardsAnimationStateName());
                    } else if (animationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.JumpStartBounceFromGroundLeftStrafingAirAnimationStateName()))
                    {
                        // extremum (air flip?) when strafing left
                        animationController.ChangeAnimationState(
                            RaymanAnimations.StrafingAirJumpExtremumAirRollLeftAnimationStateName());
                    } else
                    {
                        // extremum (air flip?) when strafing right
                        animationController.ChangeAnimationState(
                            RaymanAnimations.StrafingAirJumpExtremumAirRollRightAnimationStateName());
                    }

                    //animationController.ChangeAnimationState(
                    //    RaymanAnimations.AirRollFromJumpStartRunningBounceFromGroundAnimationStateName());
                }
            }
            else if (
                (
                    (
                        animationController.GetCurrentAnimationState().Equals(
                            RaymanAnimations.StrafingAirJumpExtremumAirRollForwardAnimationStateName())
                            
                       ||
                       
                       animationController.GetCurrentAnimationState().Equals(
                            RaymanAnimations.StrafingAirJumpExtremumAirRollBackwardsAnimationStateName())

                       ||

                       animationController.GetCurrentAnimationState().Equals(
                            RaymanAnimations.StrafingAirJumpExtremumAirRollLeftAnimationStateName())

                       ||

                       animationController.GetCurrentAnimationState().Equals(
                            RaymanAnimations.StrafingAirJumpExtremumAirRollRightAnimationStateName())
                    )
                    && animationController.HasAnimationEnded())
               )
            {

                // change into finishing falling animation appropriate for given strafing air jumping sequence

                if (animationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.StrafingAirJumpExtremumAirRollForwardAnimationStateName()))
                {
                    // falling from extremum forward
                    animationController.ChangeAnimationState(
                        RaymanAnimations.StrafingAirFallingAfterExtremumAirRollForwardAnimationStateName());
                }
                else if (animationController.GetCurrentAnimationState().Equals(
                  RaymanAnimations.StrafingAirJumpExtremumAirRollBackwardsAnimationStateName()))
                {
                    // falling from extremum backwards
                    animationController.ChangeAnimationState(
                        RaymanAnimations.StrafingAirFallingAfterExtremumAirRollBackwardsAnimationStateName());
                }
                else if (animationController.GetCurrentAnimationState().Equals(
                  RaymanAnimations.StrafingAirJumpExtremumAirRollLeftAnimationStateName()))
                {
                    // falling from extremum left
                    animationController.ChangeAnimationState(
                        RaymanAnimations.StrafingAirFallingAfterExtremumAirRollLeftAnimationStateName());
                }
                else
                {
                    // falling from extremum right
                    animationController.ChangeAnimationState(
                        RaymanAnimations.StrafingAirFallingAfterExtremumAirRollRightAnimationStateName());
                }

                //animationController.ChangeAnimationState(
                //    RaymanAnimations.FallingFromJumpFromRunningAnimationStateName());
            }

            else
                if ((playerMovementStateInfo.movementVelocity.y < 0.1f &&
                    animationController.GetCurrentAnimationState().Equals(
                    RaymanAnimations.JumpStartBounceFromGroundAnimationStateName()))

                    ||

                    (!playerMovementStateInfo.isHelicopter &&
                    !animationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.FallingFromJumpFromRunningAnimationStateName()))

                        &&


                        (!playerMovementStateInfo.isHelicopter &&
                            !animationController.GetCurrentAnimationState().Equals(
                            RaymanAnimations.StrafingAirFallingAfterExtremumAirRollForwardAnimationStateName()))

                            &&

                            (!playerMovementStateInfo.isHelicopter &&
                                !animationController.GetCurrentAnimationState().Equals(
                                RaymanAnimations.StrafingAirFallingAfterExtremumAirRollBackwardsAnimationStateName()))

                            &&

                            (!playerMovementStateInfo.isHelicopter &&
                                !animationController.GetCurrentAnimationState().Equals(
                                RaymanAnimations.StrafingAirFallingAfterExtremumAirRollLeftAnimationStateName()))


                                &&


                            (!playerMovementStateInfo.isHelicopter &&
                               !animationController.GetCurrentAnimationState().Equals(
                               RaymanAnimations.StrafingAirFallingAfterExtremumAirRollRightAnimationStateName()))



                      &&


                    !animationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.StrafingAirJumpExtremumAirRollForwardAnimationStateName())

                    &&

                    !animationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.StrafingAirJumpExtremumAirRollBackwardsAnimationStateName())

                    &&

                    !animationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.StrafingAirJumpExtremumAirRollLeftAnimationStateName())

                    &&
                    !animationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.StrafingAirJumpExtremumAirRollRightAnimationStateName()))
            {
                animationController.ChangeAnimationState(
                    RaymanAnimations.FallingAnimationStateName());
            }

            if (Input.GetButton("Jump") &&
                    !playerMovementStateInfo.isHelicopter
                    && playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState)
            {
                playerMovementStateInfo.isHelicopter = true;
                helicopterSound.Play();
                playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
                animationController.ChangeAnimationStateWithPriority(
                   RaymanAnimations.ActivateHelicopterFromFreeFallingAnimationStateName(), priority: 4);
            }

            if (playerMovementStateInfo.isHelicopter
                    && !Input.GetButton("Jump") &&
                    playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState)
            {
                playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
                playerMovementStateInfo.isHelicopter = false;
                helicopterSound.Pause();
                animationController.ChangeAnimationState(
                   RaymanAnimations.FallingAnimationStateName());
            }

            if (animationController.GetCurrentAnimationState().Equals(
                  RaymanAnimations.ActivateHelicopterFromFreeFallingAnimationStateName()) &&
                  animationController.HasAnimationEnded())
            {
                animationController.ChangeAnimationState(
                   RaymanAnimations.HelicopterStateName());
            }

            RaycastHit ledgeGrabHit = new RaycastHit();
            RaycastHit wallClimbHit = new RaycastHit();
            RaycastHit roofHangingHit = new RaycastHit();
            if (rayCollider.IsHittingGround() && playerMovementStateInfo.movementVelocity.y <= 0)
            {
                // allow falling animation to be interrupted
                animationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.FallingAnimationStateName(), newPriority: 1);
                animationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.JumpStartBounceFromGroundAnimationStateName(), newPriority: 1);
                playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_STRAFING_GROUND;
                playerMovementStateInfo.isHelicopter = false;
                helicopterSound.Pause();
                return;
            }
            else if (rayCollider.IsCollidingWithLedgeGrabCollider(out ledgeGrabHit))
            {
                playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_LEDGE_GRABBING;
                playerMovementStateInfo.isHelicopter = false;
                helicopterSound.Pause();
                GetComponent<RuleLedgeGrabbing>().ClearRuleState(ledgeGrabHit);
                return;
            }
            else if (rayCollider.IsCollidingWithWallClimbObject(out wallClimbHit))
            {
                playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_WALL_CLIMBING;
                helicopterSound.Pause();
                playerMovementStateInfo.isHelicopter = false;
                GetComponent<RuleWallClimbing>().ClearRuleState(wallClimbHit);
                return;
            }
            else if (rayCollider.IsHittingRoofHanging(out roofHangingHit))
            {
                playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_ROOF_HANGING;
                helicopterSound.Pause();
                playerMovementStateInfo.isHelicopter = false;
                GetComponent<RuleRoofHanging>().ClearRuleState(roofHangingHit);
                return;
            }
            else if (!Input.GetButton("Jump"))
            {
                playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = true;
            }
        }

        public ShootSightDirection GetShootSightDirection()
        {
            Vector3 translation = playerMovementInput.GetTranslation();
            float angle = Vector3.SignedAngle(Vector3.forward, translation.normalized, Vector3.up);
            if (translation.magnitude < 0.95f || (angle >= -10 && angle <= 10) || (angle >= 170 || angle <= -170))
            {
                // straight
                return ShootSightDirection.DOWN_ARROW_STRAIGHT;
            }
            else if (angle > 10)
            {
                // right
                return ShootSightDirection.DOWN_ARROW_RIGHT;
            }
            else
            {
                // left
                return ShootSightDirection.DOWN_ARROW_LEFT;
            }
        }
    }
}
