﻿using Assets.Scripts.Animations.Models;
using Assets.Scripts.Common;
using Assets.Scripts.PlayerMechanics.Camera.Rules;
using Assets.Scripts.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Rules
{
    public class RuleAir : PlayerMovementRule
    {
        private float playerInAirSpeed = 0.21f;
        private float playerJumpSpeed = 0.28f;

        protected override PlayerMovementRuleEnum GetPlayerMovementRuleEnum()
        {
            return PlayerMovementRuleEnum.RULE_AIR;
        }

        protected override void OnRuleEnter()
        {
            playerMovementStateInfo.isAbleToTriggerEnteringJumpState = false;
            playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
        }

        protected override void OnRuleExit()
        {
            if (!playerMovementStateInfo.currentRule.Equals(PlayerMovementRuleEnum.RULE_STRAFING_AIR))
            {
                playerMovementStateInfo.isJumping = false;
                playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
                playerMovementStateInfo.isAbleToTriggerEnteringJumpState = false;
                playerMovementStateInfo.isHelicopter = false;
                helicopterSound.Pause();
            }            
        }

        protected override void OnRuleFixedUpdate()
        {
            if (Input.GetButton(KeyControls.strafingButton))
            {
                // exit air rule right away and go into strafing air rule
                playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_STRAFING_AIR;
                playerMovementMetrics.cameraRule = CameraRuleEnum.CAMERA_STRAFING;
                return;
            }

            HandleShooting();
            playerMovementInput.FixedUpdatePlayerMovementInput();
            Vector3 inputTranslation = playerMovementInput.GetTranslation();
            Vector3 translation = playerMovementInput.GetTranslation() * playerInAirSpeed;
            //float axesMagnitude =
            //    Mathf.Clamp01(new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")).magnitude);

            //Vector3 translation =
            //       ((Input.GetAxisRaw("Vertical") * Vector3.forward) +
            //       (Input.GetAxisRaw("Horizontal") * Vector3.right)).normalized
            //       * axesMagnitude * playerInAirSpeed;

            Vector3 translationDirectionInWorldSpace =
                    (Quaternion.AngleAxis(playerMovementMetrics.angleFromAbsoluteForward, Vector3.up)
                    * translation).normalized;

            if (inputTranslation.magnitude < 0.1)
            {
                float movementSmoothingInterpolation = 0.6f;
                playerMovementStateInfo.movementVelocity.x = 
                    Mathf.Lerp(playerMovementStateInfo.movementVelocity.x, 0, movementSmoothingInterpolation);
                playerMovementStateInfo.movementVelocity.z = 
                    Mathf.Lerp(playerMovementStateInfo.movementVelocity.z, 0, movementSmoothingInterpolation);
            }
            else
            {
                //float movementSmoothingInterpolation = 0.25f;
                float movementSmoothingInterpolation = 0.25f;
                playerMovementStateInfo.movementVelocity.x = 
                    Mathf.Lerp(playerMovementStateInfo.movementVelocity.x, translation.x, movementSmoothingInterpolation);
                playerMovementStateInfo.movementVelocity.z = 
                    Mathf.Lerp(playerMovementStateInfo.movementVelocity.z, translation.z, movementSmoothingInterpolation);
            }

            if (inputTranslation.magnitude > 0.1)
            {
                transform.forward = playerMovementMetrics.forwardDirection;
                animatedPlayerPart.transform.forward = -translationDirectionInWorldSpace;
            }

            playerMovementStateInfo.movementVelocity = transform.InverseTransformDirection
                (WallCollisionHelper
                   .GetMovementVelocityConsideringWallCollisions(
                    gameObject,
                    rayCollider,
                    -animatedPlayerPart.transform.forward,
                    transform.TransformDirection(playerMovementStateInfo.movementVelocity)));

            playerMovementStateInfo.movementVelocity = transform.InverseTransformDirection(
                rayCollider.AdjustVelocityIfIsHittingTheCeiling(
                    transform.TransformDirection(playerMovementStateInfo.movementVelocity)));

            if (playerMovementStateInfo.isJumping)
            {
                playerMovementStateInfo.isJumping = false;
                jumpSound.Play();
                playerMovementStateInfo.movementVelocity.y = playerJumpSpeed;

                Vector3 flatVelocity = Vector3.ProjectOnPlane(playerMovementStateInfo.movementVelocity, Vector3.up);

                if (inputTranslation.magnitude > 0.7f)
                {
                    animationController.ChangeAnimationStateWithPriority(
                        RaymanAnimations.JumpStartRunningBounceFromGroundAnimationStateName(),
                        priority: 3);
                } else
                {
                    animationController.ChangeAnimationStateWithPriority(
                        RaymanAnimations.JumpStartBounceFromGroundAnimationStateName(),
                        priority: 3);
                }
            }

            if (playerMovementStateInfo.movementVelocity.y > -PlayerMovementMetrics.limitFallSpeed)
            {
                if (!playerMovementStateInfo.isHelicopter)
                {
                    playerMovementStateInfo.movementVelocity.y -= PlayerMovementMetrics.gravityAcceleration;
                }       
            }

            if (playerMovementStateInfo.isHelicopter)
            {
                playerMovementStateInfo.movementVelocity.y = -PlayerMovementMetrics.limitFallSpeedHelicopter;
            }

            transform.Translate(playerMovementStateInfo.movementVelocity);

            if (animationController.GetCurrentAnimationState().Equals(
                  RaymanAnimations.JumpStartRunningBounceFromGroundAnimationStateName()) &&
                  animationController.HasAnimationEnded())
            {
                if (playerMovementStateInfo.movementVelocity.y < 0.1f)
                {
                    //animationController.ChangeAnimationStateWithPriority(
                    //    RaymanAnimations.AirRollFromJumpStartRunningBounceFromGroundAnimationStateName(),
                    //    RaymanAnimations.AirRollFromJumpStartRunningBounceFromGroundAnimationStateSpeed(), 2);

                    animationController.ChangeAnimationState(
                        RaymanAnimations.AirRollFromJumpStartRunningBounceFromGroundAnimationStateName());
                }                
            }
            else if ((animationController.GetCurrentAnimationState().Equals(
                    RaymanAnimations.AirRollFromJumpStartRunningBounceFromGroundAnimationStateName())
                    && animationController.HasAnimationEnded())
               )
            {
                animationController.ChangeAnimationState(
                    RaymanAnimations.FallingFromJumpFromRunningAnimationStateName());
            }

            else 
                if ((playerMovementStateInfo.movementVelocity.y < 0.1f && 
                    animationController.GetCurrentAnimationState().Equals(
                    RaymanAnimations.JumpStartBounceFromGroundAnimationStateName())) 
                    
                    ||
                    
                    (!playerMovementStateInfo.isHelicopter &&
                    !animationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.FallingFromJumpFromRunningAnimationStateName()))
                        
                        &&

                    !animationController.GetCurrentAnimationState().Equals(
                        RaymanAnimations.AirRollFromJumpStartRunningBounceFromGroundAnimationStateName()))
            {
                animationController.ChangeAnimationState(
                    RaymanAnimations.FallingAnimationStateName());
            }

            if (Input.GetButton("Jump") &&
                    !playerMovementStateInfo.isHelicopter 
                    && playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState)
            {
                playerMovementStateInfo.isHelicopter = true;
                helicopterSound.Play();
                playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
                animationController.ChangeAnimationStateWithPriority(
                   RaymanAnimations.ActivateHelicopterFromFreeFallingAnimationStateName(), priority: 4);
            }

            if (playerMovementStateInfo.isHelicopter
                    && !Input.GetButton("Jump") && 
                    playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState)
            {
                playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = false;
                playerMovementStateInfo.isHelicopter = false;
                helicopterSound.Pause();
                animationController.ChangeAnimationState(
                   RaymanAnimations.FallingAnimationStateName());
            }

            if (animationController.GetCurrentAnimationState().Equals(
                  RaymanAnimations.ActivateHelicopterFromFreeFallingAnimationStateName()) &&
                  animationController.HasAnimationEnded())
            {
                animationController.ChangeAnimationState(
                   RaymanAnimations.HelicopterStateName());
            }

            RaycastHit ledgeGrabHit = new RaycastHit();
            RaycastHit wallClimbHit = new RaycastHit();
            RaycastHit roofHangingHit = new RaycastHit();
            if (rayCollider.IsHittingGround() && playerMovementStateInfo.movementVelocity.y <= 0)
            {
                // allow falling animation to be interrupted
                animationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.FallingAnimationStateName(), newPriority: 1);
                animationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                    RaymanAnimations.JumpStartBounceFromGroundAnimationStateName(), newPriority: 1);
                playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_GROUND;
                GetComponent<RuleGround>().SetTransitionData(
                    landing: true,
                    withHelicopterFlag: playerMovementStateInfo.isHelicopter);
                return;
            }
            else if (rayCollider.IsCollidingWithLedgeGrabCollider(out ledgeGrabHit)) 
            {
                playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_LEDGE_GRABBING;
                playerMovementStateInfo.isHelicopter = false;
                helicopterSound.Pause();
                GetComponent<RuleLedgeGrabbing>().ClearRuleState(ledgeGrabHit);
                return;
            }
            else if (rayCollider.IsCollidingWithWallClimbObject(out wallClimbHit))
            {
                playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_WALL_CLIMBING;
                helicopterSound.Pause();
                playerMovementStateInfo.isHelicopter = false;
                GetComponent<RuleWallClimbing>().ClearRuleState(wallClimbHit);
                return;
            }
            else if (rayCollider.IsHittingRoofHanging(out roofHangingHit))
            {
                playerMovementStateInfo.currentRule = PlayerMovementRuleEnum.RULE_ROOF_HANGING;
                helicopterSound.Pause();
                playerMovementStateInfo.isHelicopter = false;
                GetComponent<RuleRoofHanging>().ClearRuleState(roofHangingHit);
                return;
            }
            else if (!Input.GetButton("Jump"))
            {
                playerMovementStateInfo.isAbleToTriggerEnteringHelicopterState = true;
            }
        }
    }
}
