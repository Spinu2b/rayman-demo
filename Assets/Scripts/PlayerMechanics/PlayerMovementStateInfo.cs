﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics
{
    public enum PlayerMovementRuleEnum
    {
        RULE_GROUND,
        RULE_AIR,
        RULE_LEDGE_GRABBING,
        RULE_WALL_CLIMBING,
        RULE_ROOF_HANGING,

        RULE_STRAFING_GROUND,
        RULE_STRAFING_AIR,
        RULE_LUM_SWINGING
    }

    public class PlayerMovementStateInfo : MonoBehaviour
    {
        public bool isJumping = false;
        public bool isHelicopter = false;

        public PlayerMovementRuleEnum currentRule;

        public bool isAbleToTriggerEnteringHelicopterState = false;
        public bool isAbleToTriggerEnteringJumpState = false;

        public bool jumpButtonIsDown = false;

        public Vector3 movementVelocity = new Vector3(0, 0, 0);

        private void Awake()
        {
            currentRule = PlayerMovementRuleEnum.RULE_AIR;
        }
    }
}
