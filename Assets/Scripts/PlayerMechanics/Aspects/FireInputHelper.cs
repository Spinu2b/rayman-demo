﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Aspects
{
    public class FireInputHelper : MonoBehaviour
    {
        private float instantShotFireMaxTimePressingThreshold = 0.05f;
        private float lastFirePressingTime = 0;
        private float currentFirePressingTime = 0;

        private bool checkedForFirePressingCycleFinish = false;
        private bool checkedForChargingFirePressingCycleStart = false;
        private bool finishedFirePressingCycle = true;

        public float GetInstantShotFireMaxTimePressingThreshold()
        {
            return instantShotFireMaxTimePressingThreshold;
        }

        private void Update()
        {
            if (IsPressingFire())
            {
                currentFirePressingTime += Time.deltaTime;
                finishedFirePressingCycle = false;
            } else if (!finishedFirePressingCycle)
            {
                lastFirePressingTime = currentFirePressingTime;
                currentFirePressingTime = 0f;
                finishedFirePressingCycle = true;
                checkedForFirePressingCycleFinish = false;
                checkedForChargingFirePressingCycleStart = false;
            }
        }

        public bool IsPressingFire()
        {
            return Input.GetButton("Fire1");
        }

        public float LastFirePressingSumTime()
        {
            return lastFirePressingTime;
        }

        public float CurrentFirePressingSumTime()
        {
            return currentFirePressingTime;
        }

        public bool HasJustFinishedPressingFire()
        {
            if (finishedFirePressingCycle && !checkedForFirePressingCycleFinish)
            {
                checkedForFirePressingCycleFinish = true;
                return true;
            } else
            {
                return false;
            }
        }

        public bool HasJustStartedPressingFireForCharging()
        {
            if (!finishedFirePressingCycle && !checkedForChargingFirePressingCycleStart)
            {
                checkedForChargingFirePressingCycleStart = true;
                return true;
            } else
            {
                return false;
            }
        }
    }
}
