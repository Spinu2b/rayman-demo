﻿using Assets.Scripts.Animations;
using Assets.Scripts.Animations.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Aspects
{
    public class PlayerGroundRollingAspect : MonoBehaviour
    {
        private AnimationController animationController;
        private GameObject animatedPlayerPart;

        public float groundRollForwardSpeed = 0.22f;
        public float groundRollStrafingForwardSpeed = 0.17f;
        public float groundRollLeftRightSpeed = 0.20f;
        public float groundRollBackwardsSpeed = 0.15f;
        private Vector3 currentForwardRollDirection;
        private Vector3 currentBackwardsRollDirection;
        private Vector3 currentLeftRightRollDirection;

        private void Awake()
        {
            this.animationController = GetComponentInChildren<AnimationController>();
            this.animatedPlayerPart = animationController.gameObject;
        }

        public bool IsPerformingGroundRoll()
        {
            return
                (
                    animationController.GetCurrentAnimationState().Equals(RaymanAnimations.StrafingRollForwardStateName())
                    ||
                    animationController.GetCurrentAnimationState().Equals(RaymanAnimations.StrafingRollBackwardsStateName())
                    ||
                    animationController.GetCurrentAnimationState().Equals(RaymanAnimations.StrafingRollLeftStateName())
                    ||
                    animationController.GetCurrentAnimationState().Equals(RaymanAnimations.StrafingRollRightStateName())
                ) && !animationController.HasAnimationEnded();
        }

        public bool HasEndedGroundRoll()
        {
            return
                (
                    animationController.GetCurrentAnimationState().Equals(RaymanAnimations.StrafingRollForwardStateName())
                    ||
                    animationController.GetCurrentAnimationState().Equals(RaymanAnimations.StrafingRollBackwardsStateName())
                    ||
                    animationController.GetCurrentAnimationState().Equals(RaymanAnimations.StrafingRollLeftStateName())
                    ||
                    animationController.GetCurrentAnimationState().Equals(RaymanAnimations.StrafingRollRightStateName())
                ) && animationController.HasAnimationEnded();
        }

        public void StartPerformingForwardGroundRoll(Vector3 rollDirection)
        {
            animationController.ChangeAnimationStateWithPriority(RaymanAnimations.StrafingRollForwardStateName(), priority: 10);
            currentForwardRollDirection = rollDirection.normalized;
        }

        public void BreakTheRollCycle()
        {
            animationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                RaymanAnimations.StrafingRollForwardStateName(), newPriority: 0);
            animationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                RaymanAnimations.StrafingRollBackwardsStateName(), newPriority: 0);
            animationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                RaymanAnimations.StrafingRollLeftStateName(), newPriority: 0);
            animationController.ChangeAnimationPriorityIfItIsBeingPlayed(
                RaymanAnimations.StrafingRollRightStateName(), newPriority: 0);
        }

        public Vector3 GetMovementVelocityAppropriateForCurrentForwardRollCycle()
        {
            return currentForwardRollDirection * groundRollForwardSpeed;
        }

        public Vector3 GetMovementVelocityAppropriateForCurrentBackwardsRollCycle()
        {
            return currentBackwardsRollDirection * groundRollBackwardsSpeed;
        }

        public Vector3 GetMovementVelocityAppropriateForCurrentLeftRightRollCycle()
        {
            return currentLeftRightRollDirection * groundRollLeftRightSpeed;
        }

        public void StartPerformingStrafingGroundRoll(
            Vector3 playerForwardDirection,
            Vector3 playerRightDirection,
            Vector3 rollInitialDirection)
        {
            float angleBetweenDirectionAndAnimatedPlayerPartForward =
                         Vector3.Angle(
                             playerForwardDirection.normalized, rollInitialDirection.normalized);

            float angleBetweenDirectionAndAnimatedPlayerPartRight =
                Vector3.Angle(
                    playerRightDirection.normalized, rollInitialDirection.normalized
                    );

            if (angleBetweenDirectionAndAnimatedPlayerPartForward < 20f)
            {
                // roll forward
                //animationController.ChangeAnimationStateWithPriority(
                //    RaymanAnimations.StrafingRollForwardStateName(), priority: 10);
                StartPerformingForwardGroundRoll(rollInitialDirection);
                return;
            } else if (angleBetweenDirectionAndAnimatedPlayerPartForward < 160f)
            {
                // roll left or right
                if (angleBetweenDirectionAndAnimatedPlayerPartRight < 90f)
                {
                    // roll right
                    animationController.ChangeAnimationStateWithPriority(
                        RaymanAnimations.StrafingRollRightStateName(), priority: 10);
                } else
                {
                    // roll left
                    animationController.ChangeAnimationStateWithPriority(
                        RaymanAnimations.StrafingRollLeftStateName(), priority: 10);
                }

                currentLeftRightRollDirection = rollInitialDirection;
            } else
            {
                // roll backwards
                animationController.ChangeAnimationStateWithPriority(
                    RaymanAnimations.StrafingRollBackwardsStateName(), priority: 10);
                currentBackwardsRollDirection = rollInitialDirection;
            }
        }

        public bool IsPerformingGroundBackwardsRoll()
        {
            return animationController.GetCurrentAnimationState()
                .Equals(RaymanAnimations.StrafingRollBackwardsStateName())
                && !animationController.HasAnimationEnded();
        }

        public bool IsPerformingGroundForwardRoll()
        {
            return animationController.GetCurrentAnimationState()
                .Equals(RaymanAnimations.StrafingRollForwardStateName())
                && !animationController.HasAnimationEnded();
        }

        public bool IsPerformingGroundLeftOrRightRoll()
        {
            return 
                (
                    animationController.GetCurrentAnimationState()
                    .Equals(RaymanAnimations.StrafingRollRightStateName()) 
                    ||
                    animationController.GetCurrentAnimationState()
                    .Equals(RaymanAnimations.StrafingRollLeftStateName())
                )
                && !animationController.HasAnimationEnded();
        }

        private void FixedUpdate()
        {
            if (HasEndedGroundRoll())
            {
                BreakTheRollCycle();
            }
        }
    }
}
