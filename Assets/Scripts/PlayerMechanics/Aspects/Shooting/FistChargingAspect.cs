﻿using Assets.Scripts.Animations;
using Assets.Scripts.Animations.Models;
using Assets.Scripts.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Aspects.Shooting
{
    public static class FistChargingAnimationHelper
    {
        private static Vector3 rightChargingHandOriginOffset = Vector3.right;
        private static Vector3 leftChargingHandOriginOffset = -Vector3.right;

        private static float handSpinningRadiusAroundOrigin = 0.5f;
        private static float handSpinningAngularDegreesSpeed = 40.0f;

        public static void SpinRightFistChargingPrefab(
            Transform animatedPlayerPart, Transform currentChargingFistPrefabTransform)
        {
            Vector3 handOrigin = animatedPlayerPart.transform.position + Vector3.up * 1.2f +
                (-animatedPlayerPart.TransformDirection(rightChargingHandOriginOffset));

            Vector3 rotationAxis = -animatedPlayerPart.TransformDirection(rightChargingHandOriginOffset);

            //currentChargingFistPrefabTransform.Translate(Vector3.up);
            //currentChargingFistPrefabTransform.RotateAround(
            //    handOrigin, rotationAxis, handSpinningAngularDegreesSpeed * Time.deltaTime);

            currentChargingFistPrefabTransform.GetComponent<SpinningFistChargBeh>()
                .Spin(handOrigin, rotationAxis, 0.7f, handSpinningAngularDegreesSpeed);
        }

        public static void SpinLeftFistChargingPrefab(
            Transform animatedPlayerPart, Transform currentChargingFistPrefabTransform)
        {
            Vector3 handOrigin = animatedPlayerPart.transform.position + Vector3.up * 1.2f +
                (-animatedPlayerPart.TransformDirection(leftChargingHandOriginOffset));

            Vector3 rotationAxis = -animatedPlayerPart.TransformDirection(leftChargingHandOriginOffset);

            currentChargingFistPrefabTransform.GetComponent<SpinningFistChargBeh>()
                .Spin(handOrigin, rotationAxis, 0.7f, handSpinningAngularDegreesSpeed);
            //currentChargingFistPrefabTransform.Translate(Vector3.up);
            //currentChargingFistPrefabTransform.RotateAround(
            //    handOrigin, rotationAxis, handSpinningAngularDegreesSpeed * Time.deltaTime);
        }

        public static Transform 
            InstantiateRightFistPrefabForChargingAndHideRightActualModelHand(
            Transform animatedPlayerPart)
        {
            //GameObject handObj = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            GameObject handObj = GameObject.Instantiate(GameObject.Find("RIGHT_HAND_OBJ"));
            animatedPlayerPart.GetComponent<AnimationController>().ChangeAnimationState(
                RaymanAnimations.ExtraAnimMissingRightHandStateName(), 1);
            handObj.tag = Tags.projectilesTag;
            handObj.GetComponent<TrailRenderer>().autodestruct = false;
            //handObj.GetComponent<TrailRenderer>().time = 10f;
            handObj.transform.position = animatedPlayerPart.transform.position + Vector3.up * 1.2f +
                (-animatedPlayerPart.TransformDirection(Vector3.right));

            handObj.AddComponent<SpinningFistChargBeh>();
            return handObj.transform;
        }

        public static Transform 
            InstantiateLeftFistPrefabForChargingAndHideLeftActualModelHand(
            Transform animatedPlayerPart)
        {
            GameObject handObj = GameObject.Instantiate(GameObject.Find("RIGHT_HAND_OBJ"));
            animatedPlayerPart.GetComponent<AnimationController>().ChangeAnimationState(
                RaymanAnimations.ExtraAnimMissingLeftHandStateName(), 2);
            handObj.tag = Tags.projectilesTag;
            handObj.GetComponent<TrailRenderer>().autodestruct = false;
            //handObj.GetComponent<TrailRenderer>().time = 10f;
            handObj.transform.position = animatedPlayerPart.transform.position + Vector3.up * 1.2f +
                (-animatedPlayerPart.TransformDirection(-Vector3.right));

            handObj.AddComponent<SpinningFistChargBeh>();
            return handObj.transform;
        }

        public static void DestroyRespectiveFistChargingPrefab(
            Transform currentChargingFistPrefabTransform,
            bool isChargingRightHandCurrently)
        {
            if (currentChargingFistPrefabTransform && currentChargingFistPrefabTransform.gameObject)
            {
                GameObject.Destroy(currentChargingFistPrefabTransform.gameObject);
            }            
            //throw new NotImplementedException();
        }
    }

    public class FistChargingAspect : MonoBehaviour
    {
        private float currentChargePower = 1.0f;
        private float lastKeepChargingCycleTime;

        public bool isChargingRightHandCurrently = false;
        private bool isInValidChargingCycle = false;

        private Transform animatedPlayerPart;
        private Transform currentChargingFistPrefabTransform;

        private float fistChargingSpeed = 0.1f;

        private void Start()
        {
            lastKeepChargingCycleTime = Time.time;
            animatedPlayerPart = GetComponentInChildren<AnimationController>().gameObject.transform;
        }

        public void KeepChargingCycle()
        {
            lastKeepChargingCycleTime = Time.time;
            if (isChargingRightHandCurrently)
            {
                FistChargingAnimationHelper
                    .SpinRightFistChargingPrefab(animatedPlayerPart, currentChargingFistPrefabTransform);
            }
            else
            {
                FistChargingAnimationHelper.SpinLeftFistChargingPrefab(
                    animatedPlayerPart, currentChargingFistPrefabTransform);
            }
            currentChargePower += fistChargingSpeed;          
        }

        public void InitiateFistChargingCycleForHand(bool isRightHand)
        {
            isChargingRightHandCurrently = isRightHand;
            isInValidChargingCycle = true;
            if (isChargingRightHandCurrently)
            {
                currentChargingFistPrefabTransform = FistChargingAnimationHelper
                    .InstantiateRightFistPrefabForChargingAndHideRightActualModelHand(animatedPlayerPart);
            } else
            {
                currentChargingFistPrefabTransform = FistChargingAnimationHelper
                    .InstantiateLeftFistPrefabForChargingAndHideLeftActualModelHand(animatedPlayerPart);
            }
        }

        public float FinishChargingCycleAndGetEventualChargePower()
        {
            float result = currentChargePower;
            currentChargePower = 1f;
            isInValidChargingCycle = false;

            FistChargingAnimationHelper
              .DestroyRespectiveFistChargingPrefab(
                 currentChargingFistPrefabTransform, isChargingRightHandCurrently);      
            return result;
        }

        public bool IsChargingRightHandCurrently()
        {
            return isChargingRightHandCurrently && IsInValidCycleCurrently();
        }

        private bool IsInValidCycleCurrently()
        {
            return isInValidChargingCycle;
        }

        public bool IsChargingLeftHandCurrently()
        {
            return !isChargingRightHandCurrently && IsInValidCycleCurrently();
        }
    }
}
