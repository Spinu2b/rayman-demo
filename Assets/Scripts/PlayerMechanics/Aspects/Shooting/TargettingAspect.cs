﻿using Assets.Scripts.Animations;
using Assets.Scripts.Common;
using Assets.Scripts.HUD;
using Assets.Scripts.HUD.Targetting;
using Assets.Scripts.PlayerMechanics.Rules;
using Assets.Scripts.PlayerMechanics.Rules.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Aspects.Shooting
{
    public static class TargettablesHelper
    {
        public static List<GameObject>
            GetTargettableGameObjectsInTheScene()
        {
            return 
                GameObject.FindGameObjectsWithTag(Tags.lumForHangingTag).ToList()
                    .Concat(GameObject.FindGameObjectsWithTag(Tags.pigPotCrystalsTag).ToList()).ToList();
        }

        public static GameObject 
            GetClosestValidTarget(
                GameObject playerObject,
                Vector3 playerLookingDirection,
                List<GameObject> targettables)
        {
            Transform targetMinimalDistance = null;
            float minimalDistance = Mathf.Infinity;
            float minimalAngle = Mathf.Infinity;

            float targetAngleLookingToleranceDegrees = 20f;

            float minimalDistanceTargetThresholdConsideration = 18f;
            Vector3 currentPosition = playerObject.transform.position;
            List<Tuple<GameObject, Vector3>> potentialTargets = new List<Tuple<GameObject, Vector3>>();
            foreach (GameObject targetObject in targettables)
            {
                Vector3 distanceDirection = (targetObject.transform.position - playerObject.transform.position);

                float distance =
                    //Vector3.ProjectOnPlane(
                    //    targetObject.transform.position - playerObject.transform.position, Vector3.up).magnitude;
                    Vector3.Distance(
                        targetObject.transform.position, currentPosition);

                Vector3 directionToTarget =
                    (targetObject.transform.position - playerObject.transform.position).normalized;

                float angleFromPlayerLookingDirection =
                    Vector3.Angle(directionToTarget, playerLookingDirection);

                if (
                    //distance < minimalDistance 
                    //&&
                    distance < minimalDistanceTargetThresholdConsideration
                    && (Vector3.Angle(
                        Vector3.ProjectOnPlane(directionToTarget, Vector3.up).normalized,
                        playerLookingDirection) < 90f || 
                        Vector3.Angle(Vector3.up, directionToTarget.normalized) < 5f)
                    //&& angleFromPlayerLookingDirection - minimalAngle < -targetAngleLookingToleranceDegrees
                    )
                {
                    potentialTargets.Add(Tuple.Create(targetObject, directionToTarget));
                    targetMinimalDistance = targetObject.transform;
                    minimalAngle = angleFromPlayerLookingDirection;
                    minimalDistance = distance;
                }
            }

            return potentialTargets.OrderBy(x => 
            Vector3.Angle(
                playerLookingDirection,
                Vector3.ProjectOnPlane(x.Item2, Vector3.up)))
                .FirstOrDefault()?.Item1;
            //return targetMinimalDistance?.gameObject;
        }

        public static bool IsTooFarToTargetAnything(
            GameObject playerObject,
            GameObject currentTarget,
            Vector3 playerLookingDirection,
            List<GameObject> targettables)
        {
            if (currentTarget)
            {
                float minimalDistanceTargetThresholdConsideration = 18f;
                return Vector3.Distance(currentTarget.transform.position,
                    playerObject.transform.position) >= minimalDistanceTargetThresholdConsideration;
            } else
            {
                return GetClosestValidTarget(
                    playerObject,
                    playerLookingDirection,
                    targettables) == null;
            }            
        }
    }

    public static class TargetShootSightScreenPositioningHelper
    {
        public static Vector3 GetScreenPositionForTarget(
            UnityEngine.Camera playerCamera, GameObject target)
        {
            Vector3 guiPosition = playerCamera.WorldToScreenPoint(target.transform.position);
            guiPosition.y = Screen.height - guiPosition.y;

            return guiPosition;
        }
    }

    public class TargettingAspect : MonoBehaviour
    {
        private GameObject playerObject;
        private GameObject animatedPlayerPart;
        private UnityEngine.Camera playerCamera;
        private PlayerMovementStateInfo playerMovementStateInfo;

        public GameObject currentTarget;

        private DrawHudTargetShootSight hudTargetShootSightDrawing;

        private List<GameObject> targettables = new List<GameObject>();

        private IStrafingRuleUtils groundStrafingRule;
        private IStrafingRuleUtils airStrafingRule;

        private void Awake()
        {
            this.playerObject = gameObject;
            this.playerCamera = FindObjectOfType<UnityEngine.Camera>();
            this.hudTargetShootSightDrawing = FindObjectOfType<DrawHudTargetShootSight>();
            this.playerMovementStateInfo = GetComponent<PlayerMovementStateInfo>();

            this.animatedPlayerPart = GetComponentInChildren<AnimationController>().gameObject;

            this.groundStrafingRule = GetComponent<RuleStrafingGround>();
            this.airStrafingRule = GetComponent<RuleStrafingAir>();
        }

        public Vector3 GetPlayerLookingDirectionTowardsTarget(Vector3 playerPosition)
        {
            if (currentTarget)
            {
                return Vector3.ProjectOnPlane(
                    currentTarget.transform.position - playerPosition,
                    Vector3.up).normalized;
            } else
            {
                return new Vector3();
            }
        }

        private void Start()
        {
            targettables = TargettablesHelper.GetTargettableGameObjectsInTheScene();
        }

        private Tuple<IStrafingRuleUtils, bool> GetCurrentStrafingRule()
        {
            if (playerMovementStateInfo.currentRule.Equals(PlayerMovementRuleEnum.RULE_STRAFING_GROUND))
            {
                return Tuple.Create(groundStrafingRule, true);
            } else if (playerMovementStateInfo.currentRule.Equals(PlayerMovementRuleEnum.RULE_STRAFING_AIR))
            {
                return Tuple.Create(airStrafingRule, true);
            } else
            {
                return Tuple.Create<IStrafingRuleUtils, bool>(null, false);
            }
        }

        private void FixedUpdate()
        {
            if (IsAllowedRuleCurrentlyForTargeting())
            {
                if (currentTarget == null || GetCurrentStrafingRule().Item2 == false)
                {
                    //Debug.Log("Changing target " + currentTarget);
                    currentTarget = TargettablesHelper.GetClosestValidTarget(
                        playerObject, -animatedPlayerPart.transform.forward, targettables);
                }

                if (TargettablesHelper.IsTooFarToTargetAnything(
                    playerObject, currentTarget, -animatedPlayerPart.transform.forward,
                    targettables))
                {
                    currentTarget = null;
                }

                if (currentTarget)
                {
                    hudTargetShootSightDrawing
                        .SetShootSightScreenCoordinates(
                            TargetShootSightScreenPositioningHelper
                                .GetScreenPositionForTarget(playerCamera, currentTarget)
                        );

                    var currentStrafingRuleInfo = GetCurrentStrafingRule();

                    if (currentStrafingRuleInfo.Item2 == true)
                    {
                        hudTargetShootSightDrawing.SetShootingDirection(
                            currentStrafingRuleInfo.Item1.GetShootSightDirection());
                    }
                    else
                    {
                        hudTargetShootSightDrawing.SetShootingDirection(ShootSightDirection.DOWN_ARROW_STRAIGHT);
                    }

                    hudTargetShootSightDrawing.SetIsShootSightVisible(
                        currentTarget.GetComponentInChildren<Renderer>().isVisible);
                }
                else
                {
                    hudTargetShootSightDrawing.SetIsShootSightVisible(false);
                }
            } else
            {
                hudTargetShootSightDrawing.SetIsShootSightVisible(false);
            }                     
        }

        private bool IsAllowedRuleCurrentlyForTargeting()
        {
            return playerMovementStateInfo.currentRule.Equals(PlayerMovementRuleEnum.RULE_GROUND) ||
                playerMovementStateInfo.currentRule.Equals(PlayerMovementRuleEnum.RULE_AIR) || 
                playerMovementStateInfo.currentRule.Equals(PlayerMovementRuleEnum.RULE_STRAFING_AIR) || 
                playerMovementStateInfo.currentRule.Equals(PlayerMovementRuleEnum.RULE_STRAFING_GROUND);
        }
    }
}
