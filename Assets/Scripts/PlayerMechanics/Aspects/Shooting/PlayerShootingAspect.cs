﻿using Assets.Scripts.Animations;
using Assets.Scripts.Animations.Models;
using Assets.Scripts.Common;
using Assets.Scripts.PlayerMechanics.Aspects.Shooting;
using Assets.Scripts.PlayerMechanics.Shooting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Aspects
{
    public static class HandShootingProjectilesFactory
    {
        public static void ShootHandProjectile(
            float chargePower,
            Transform animatedPlayerPartTransform,
            PlayerShootingAspect playerShootingAspect,
            bool isRightHand,
            Vector3 startPosition, Vector3 direction,
            GameObject target)
        {
            GameObject handProjectile = GameObject.Instantiate(GameObject.Find("RIGHT_HAND_OBJ"));
            handProjectile.transform.position = startPosition;
            handProjectile.transform.localScale = new Vector3(1f, 1f, 1f);

            var handProjectileBehaviour = handProjectile.AddComponent<HandProjectileBehaviour>();
            handProjectile.tag = Tags.projectilesTag;

            handProjectileBehaviour.chargePower = chargePower;
            handProjectileBehaviour.flyingDirection = direction;
            handProjectileBehaviour.animatedPlayerPartTransform = animatedPlayerPartTransform;
            handProjectileBehaviour.initialPlayerRightVector = 
                -animatedPlayerPartTransform.transform.right.normalized;
            handProjectileBehaviour.isRightHand = isRightHand;
            handProjectileBehaviour.playerShootingAspect = playerShootingAspect;
            handProjectileBehaviour.startPosition = startPosition;
            handProjectileBehaviour.target = target;
        }

        public static void ShootHandProjectileFreelyInCurvedPath(
            float chargePower,
            Transform animatedPlayerPartTransform,
            PlayerShootingAspect playerShootingAspect,
            bool isRightHand,
            Vector3 startPosition, Vector3 direction,
            Vector3 shootingInitialCurvedPathDirection,
            GameObject target
            )
        {
            GameObject handProjectile = GameObject.Instantiate(GameObject.Find("RIGHT_HAND_OBJ"));
            handProjectile.transform.position = startPosition;
            handProjectile.transform.localScale = new Vector3(1f, 1f, 1f);

            var handProjectileBehaviour = handProjectile.AddComponent<HandProjectileBehaviour>();
            handProjectile.tag = Tags.projectilesTag;

            handProjectileBehaviour.chargePower = chargePower;
            handProjectileBehaviour.flyingDirection = direction;
            handProjectileBehaviour.isFlyingInCurvedPath = true;
            handProjectileBehaviour.currentCurvedPathFlyingDirection = shootingInitialCurvedPathDirection;
            handProjectileBehaviour.initialPlayerRightVector = 
                -animatedPlayerPartTransform.transform.right.normalized;
            handProjectileBehaviour.animatedPlayerPartTransform = animatedPlayerPartTransform;
            handProjectileBehaviour.isRightHand = isRightHand;
            handProjectileBehaviour.startPosition = startPosition;
            handProjectileBehaviour.playerShootingAspect = playerShootingAspect;
            handProjectileBehaviour.target = target;
        }

        public static void SpawnHandProjectileGoingBackToPlayer(
            PlayerShootingAspect playerShootingAspect,
            Transform animatedPlayerPartTransform, Vector3 position, bool isRightHand)
        {
            GameObject handProjectile = GameObject.Instantiate(GameObject.Find("RIGHT_HAND_OBJ"));
            handProjectile.transform.position = position;
            handProjectile.transform.localScale = new Vector3(1f, 1f, 1f);
            var handProjectileBehaviour = handProjectile.AddComponent<HandProjectileBehaviour>();
            handProjectile.tag = Tags.projectilesTag;

            handProjectileBehaviour.animatedPlayerPartTransform = animatedPlayerPartTransform;
            handProjectileBehaviour.isRightHand = isRightHand;
            handProjectileBehaviour.playerShootingAspect = playerShootingAspect;
            handProjectileBehaviour.hasToGoBackImmediately = true;
            handProjectileBehaviour.GetComponent<TrailRenderer>().enabled = false;
        }

        public static GameObject SpawnStaticHandObject(
            Vector3 position, Vector3 forwardDirection, Vector3 rightDirection, Vector3 localScale)
        {
            GameObject handObject = GameObject.Instantiate(GameObject.Find("RIGHT_HAND_OBJ"));
            handObject.transform.position = position;
            handObject.transform.right = rightDirection;
            handObject.transform.forward = forwardDirection;
            handObject.transform.localScale = localScale;

            handObject.GetComponent<TrailRenderer>().enabled = false;
            return handObject;
        }
    }

    public class PlayerShootingAspect : MonoBehaviour
    {
        private bool canPlayerTriggerShootingHand = false;
        private AnimationController animationController;
        private PlayerMovementStateInfo playerMovementStateInfo;
        private GameObject animatedPlayerPart;

        private TargettingAspect targettingAspect;

        private bool hasLeftHand = true;
        private bool hasRightHand = true;

        private int leftHandAnimationLayer = 2;
        private int rightHandAnimationLayer = 1;        

        private FireInputHelper fireInputHelper;
        private FistChargingAspect fistChargingAspect;     

        public bool HasHandsToShoot()
        {
            return hasLeftHand || hasRightHand;
        }

        public bool IsGoingForwardOrBackwardsForShooting()
        {
            float angle = Vector3.Angle(-animatedPlayerPart.transform.forward,
                transform.TransformDirection(
                    Vector3.ProjectOnPlane(playerMovementStateInfo.movementVelocity.normalized,
                    Vector3.up)).normalized);

            return ((angle < 20f) || (angle > 160f))
                || 
                Vector3.ProjectOnPlane(playerMovementStateInfo.movementVelocity, Vector3.up).magnitude < 0.1f;
        }

        public bool CanTriggerShootingHand()
        {
            return canPlayerTriggerShootingHand;
        }

        public bool IsPressingShootingButtonInstantShot()
        {
            return !fireInputHelper.IsPressingFire() &&
                fireInputHelper.LastFirePressingSumTime() 
                    <= fireInputHelper.GetInstantShotFireMaxTimePressingThreshold();
        }

        public bool WantsToFinishChargingAndShoot()
        {
            return fireInputHelper.HasJustFinishedPressingFire()
                && fireInputHelper.LastFirePressingSumTime() > 
                fireInputHelper.GetInstantShotFireMaxTimePressingThreshold();
        }

        public bool IsPressingShootingButtonForCharging()
        {
            return fireInputHelper.IsPressingFire() &&
                fireInputHelper.CurrentFirePressingSumTime() > 
                fireInputHelper.GetInstantShotFireMaxTimePressingThreshold();
        }

        public bool HasJustStartedPressingShootingButtonForCharging()
        {
            return fireInputHelper.HasJustStartedPressingFireForCharging();
        }

        public void InitiateFistChargingCycleForAppropriateHand()
        {
            fistChargingAspect.InitiateFistChargingCycleForHand(isRightHand: hasRightHand);
        }

        public void FinishFistChargingCycleAndShootHand()
        {
            float chargePower = fistChargingAspect.FinishChargingCycleAndGetEventualChargePower();
            // determine the hand explicitly?
            ShootHand(chargePower, fistChargingAspect.isChargingRightHandCurrently == true); // ?
        }

        public void FinishFistChargingCycleAndShootHandFreelyInCurvedPath(Vector3 shootingInitialDirection)
        {
            float chargePower = fistChargingAspect.FinishChargingCycleAndGetEventualChargePower();
            ShootHandFreelyInCurvedPath(
                 shootingInitialDirection, chargePower,
                 fistChargingAspect.isChargingRightHandCurrently == true);
        }

        public void KeepChargingCycle()
        {
            fistChargingAspect.KeepChargingCycle();
        }

        public void RegainRightHand()
        {
            hasRightHand = true;
        }

        public void RegainLeftHand()
        {
            hasLeftHand = true;
        }

        public void ShootHandFreelyInCurvedPath(
            Vector3 shootingInitialDirection, float chargePower = 1.0f, bool? shootRightHand = null)
        {
            canPlayerTriggerShootingHand = false;

            if (hasRightHand && (shootRightHand == null || shootRightHand == true))
            {
                hasRightHand = false;
                animationController.ChangeAnimationStateWithPriority(
                    RaymanAnimations.ExtraAnimShootingRightHandStateName(), layerIndex: 1);
                ShootHandProjectileFreelyInCurvedPath(chargePower, true, shootingInitialDirection);
            }
            else if (hasLeftHand && (shootRightHand == null || shootRightHand == false))
            {
                hasLeftHand = false;
                animationController.ChangeAnimationStateWithPriority(
                    RaymanAnimations.ExtraAnimShootingLeftHandStateName(), layerIndex: 2);
                ShootHandProjectileFreelyInCurvedPath(chargePower, false, shootingInitialDirection);
            }
        }

        public void ShootHand(float chargePower = 1.0f, bool? shootRightHand = null)
        {
            canPlayerTriggerShootingHand = false;     

            if (hasRightHand && (shootRightHand == null || shootRightHand == true))
            {
                hasRightHand = false;
                animationController.ChangeAnimationStateWithPriority(
                    RaymanAnimations.ExtraAnimShootingRightHandStateName(), layerIndex: 1);
                ShootHandProjectile(chargePower, true);
            } else if (hasLeftHand && (shootRightHand == null || shootRightHand == false))
            {
                hasLeftHand = false;
                animationController.ChangeAnimationStateWithPriority(
                    RaymanAnimations.ExtraAnimShootingLeftHandStateName(), layerIndex: 2);
                ShootHandProjectile(chargePower, false);
            }       
            //Debug.Log("SHOOOT!");
            //throw new NotImplementedException();
        }

        private void ShootHandProjectileFreelyInCurvedPath(
            float chargePower, bool isRightHand, Vector3 shootingInitialDirection)
        {
            HandShootingProjectilesFactory.ShootHandProjectileFreelyInCurvedPath(
                chargePower,
                animatedPlayerPart.transform,
                this,
                isRightHand,
                animatedPlayerPart.transform.position + Vector3.up,
                -animatedPlayerPart.transform.forward,
                shootingInitialDirection,
                targettingAspect.currentTarget
                );
        }

        private void ShootHandProjectile(float chargePower, bool isRightHand)
        {
            HandShootingProjectilesFactory.ShootHandProjectile(
                chargePower,
                animatedPlayerPart.transform,
                this,
                isRightHand,
                animatedPlayerPart.transform.position + Vector3.up,
                -animatedPlayerPart.transform.forward,
                targettingAspect.currentTarget
                );
        }

        private void Awake()
        {
            this.animationController = GetComponentInChildren<AnimationController>();
            this.animatedPlayerPart = animationController.gameObject;

            this.fireInputHelper = GetComponent<FireInputHelper>();
            this.fistChargingAspect = GetComponent<FistChargingAspect>();
            this.playerMovementStateInfo = GetComponent<PlayerMovementStateInfo>();

            this.targettingAspect = GetComponent<TargettingAspect>();
        }

        private void FixedUpdate()
        {
            if (!IsPressingShootingButtonInstantShot())
            {
                canPlayerTriggerShootingHand = true;
            }

            if (hasLeftHand && 
                !animationController.GetCurrentAnimationState(leftHandAnimationLayer)
                    .Equals(RaymanAnimations.NonOverridingAnimationLayer2StateName())
                && !fistChargingAspect.IsChargingLeftHandCurrently())
            {
                animationController.ChangeAnimationState(
                    RaymanAnimations.NonOverridingAnimationLayer2StateName(), leftHandAnimationLayer);
            }

            if (hasRightHand &&
                !animationController.GetCurrentAnimationState(rightHandAnimationLayer)
                    .Equals(RaymanAnimations.NonOverridingAnimationLayer1StateName())
                 && !fistChargingAspect.IsChargingRightHandCurrently())
            {
                animationController.ChangeAnimationState(
                    RaymanAnimations.NonOverridingAnimationLayer1StateName(), rightHandAnimationLayer);
            }

            if (!hasLeftHand && 
                animationController.GetCurrentAnimationState(leftHandAnimationLayer)
                    .Equals(RaymanAnimations.ExtraAnimShootingLeftHandStateName()) && 
                animationController.HasAnimationEnded(leftHandAnimationLayer))
            {
                animationController.ChangeAnimationState(
                    RaymanAnimations.ExtraAnimMissingLeftHandStateName(), leftHandAnimationLayer);
            }


            if (!hasRightHand &&
                animationController.GetCurrentAnimationState(rightHandAnimationLayer)
                    .Equals(RaymanAnimations.ExtraAnimShootingRightHandStateName()) &&
                animationController.HasAnimationEnded(rightHandAnimationLayer))
            {
                animationController.ChangeAnimationState(
                    RaymanAnimations.ExtraAnimMissingRightHandStateName(), rightHandAnimationLayer);
            }
        }
    }
}
