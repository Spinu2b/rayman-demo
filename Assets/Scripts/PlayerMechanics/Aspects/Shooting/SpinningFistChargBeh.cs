﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.PlayerMechanics.Aspects.Shooting
{
    public class SpinningFistChargBeh : MonoBehaviour
    {
        public float currentSpinningAngle = 0.0f;

        public void Spin(
            Vector3 rotationCenter,
            Vector3 rotationAxis,
            float rotationRadius,
            float spinningAngularDegreesSpeed)
        {
            currentSpinningAngle += spinningAngularDegreesSpeed;
            transform.position = rotationCenter + 
                (Quaternion.AngleAxis(currentSpinningAngle, rotationAxis) * Vector3.up) * rotationRadius;
        }
    }
}
