﻿using Assets.Scripts.PlayerMechanics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Utils
{
    public static class WallCollisionHelper
    {
        public static Vector3 GetMovementVelocityConsideringWallCollisions(
            GameObject gameObject, RayCollider rayCollider, Vector3 playerForwardDirectionVector, Vector3 velocity)
        {
            if (rayCollider.IsHittingTheWall())
            {
                Tuple<Vector3, List<RaycastHit>>
                    wallsNormalsInfoPlayerCollidesWith = rayCollider.GetAveragedNormalFromAllTheWallsTheCollisionOccurs();

                Vector3 playerPosition = gameObject.transform.position;
                Vector3 flatVelocity = Vector3.ProjectOnPlane(velocity, Vector3.up);

                if (wallsNormalsInfoPlayerCollidesWith.Item2.Count == 0)
                {
                    // if not colliding with anything, just return the velocity
                    return velocity;
                }
                else
                {
                    if (wallsNormalsInfoPlayerCollidesWith.Item2.Count == 1)
                    {
                        Vector3 wallNormal = 
                            Vector3.ProjectOnPlane(
                                wallsNormalsInfoPlayerCollidesWith.Item2[0].normal, Vector3.up);

                        // if colliding with only one wall, then its easy
                        // project player's velocity on wall's plane so we can glide along the wall nicely
                        if (Vector3.SignedAngle(flatVelocity, wallNormal, Vector3.up) > 90
                            || Vector3.SignedAngle(flatVelocity, wallNormal, Vector3.up) < -90)
                        {
                            return Vector3.ProjectOnPlane(
                            velocity, Vector3.ProjectOnPlane(
                                // always flatten the wall normal to the Vector3.up plane
                                wallsNormalsInfoPlayerCollidesWith.Item2[0].normal, Vector3.up).normalized);
                        } else
                        {
                            return velocity;
                        }                        
                    }
                    else
                    {
                        // colliding with more than wall, just use the heuristic that we can always get 
                        // two walls' normals that are at the corner, as long as it does the job, who cares :)
                        Vector3 wall1Normal =
                            // always flatten the wall normal to the Vector3.up plane
                            Vector3.ProjectOnPlane(
                                wallsNormalsInfoPlayerCollidesWith.Item2[0].normal, Vector3.up).normalized;
                        Vector3 wall2Normal =
                            // always flatten the wall normal to the Vector3.up plane
                            Vector3.ProjectOnPlane(
                                wallsNormalsInfoPlayerCollidesWith.Item2[1].normal, Vector3.up).normalized;

                        Vector3 averagedWallsNormal = wallsNormalsInfoPlayerCollidesWith.Item1;

                        float angleBetweenAveragedWallsNormalAndWall1Normal = 
                            Vector3.Angle(wall1Normal, averagedWallsNormal);
                        float angleBetweenAveragedWallsNormalAndWall2Normal = 
                            Vector3.Angle(wall2Normal, averagedWallsNormal);

                        if (Vector3.Angle(playerForwardDirectionVector, averagedWallsNormal)
                            > angleBetweenAveragedWallsNormalAndWall1Normal
                            && Vector3.Angle(playerForwardDirectionVector, averagedWallsNormal)
                            > angleBetweenAveragedWallsNormalAndWall2Normal)
                        {
                            // if player tries to run in the direction outwards the corner in the direction towards the walls,
                            // simply do not let him move any further in that direction :)
                            // let him glide along one of the walls in the corner that is 'right' for that
                            Vector3 potentialVelocityAlongWall1 = Vector3.ProjectOnPlane(velocity, wall1Normal);
                            Vector3 potentialVelocityAlongWall2 = Vector3.ProjectOnPlane(velocity, wall2Normal);

                            // allow player to move only in the direction that does not lead to another wall
                            if (!Physics.Raycast(
                                playerPosition,
                                potentialVelocityAlongWall1,
                                2.5f))
                            {
                                return potentialVelocityAlongWall1;
                            }
                            else if
                              (!Physics.Raycast(
                              playerPosition,
                              potentialVelocityAlongWall2,
                              2.5f))
                            {
                                return potentialVelocityAlongWall2;
                            }
                            else
                            {
                                return new Vector3(0, velocity.y, 0);
                            }
                        }
                        else
                        {
                            return velocity;
                        }
                    }
                }
            } else
            {
                return velocity;
            }
        }
    }
}
