﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Common
{
    public static class KeyControls
    {
        public static string strafingButton = "Fire2";
        public static string groundRollButton = "Fire3";

        public static KeyCode cameraLeftButtonKeyboard = KeyCode.Q;
        public static KeyCode cameraRightButtonKeyboard = KeyCode.E;
        public static KeyCode walkingButtonKeyboard = KeyCode.LeftShift;
    }
}
