﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Common
{
    public static class Layers
    {
        public const int defaultLayerMask = 1 << 0;

        public static string ledgeCollidersLayer = "LedgeCollidersLayer";
        public static int ledgeCollidersLayerMask = 1 << 8;
        public static int ledgeCollidersLayerIndex = 8;
    }
}
