﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Common
{
    public static class Tags
    {
        public static string ledgeGrabbableTag = "LedgeGrabbable";
        public static string ledgeColliderTag = "LedgeCollider";
        public static string wallClimbTag = "ClimbWall";
        public static string roofHangingTag = "HangRoof";
        public static string gemsTag = "Gems";
        public static string projectilesTag = "Projectiles";
        public static string lumForHangingTag = "LumForHanging";
        public static string pigPotCrystalsTag = "PigPotCrystals";

        public static string targetablePartTag = "TargetablePart";
    }
}
