﻿using Assets.Scripts.Animations;
using Assets.Scripts.Animations.Models;
using UnityEngine;

namespace Assets.Scripts.Tests
{
    public class RaymanAnimationsTestComponent : MonoBehaviour
    {
        private AnimationController animationController;

        public void Awake()
        {
            animationController = GetComponent<AnimationController>();
        }

        public void Update()
        {
            animationController.ChangeAnimationState(
                RaymanAnimations.RunningAnimationStateName());
        }
    }
}
