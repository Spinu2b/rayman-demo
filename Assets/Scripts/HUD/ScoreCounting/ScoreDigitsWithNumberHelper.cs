﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.HUD.ScoreCounting
{
    public static class DigitsHelper
    {
        public static float 
            GetDigitStartTextureCoordinateY(
            List<float> digitsTexCoordsYThresholds,
            float digitScrollFrom0To10)
        {
            int periodIndex = ((int)digitScrollFrom0To10) % 10;
            float segment = 0f;
            if (periodIndex < 9)
            {
                segment = digitsTexCoordsYThresholds[periodIndex + 1] - digitsTexCoordsYThresholds[periodIndex];
            } else
            {
                segment = digitsTexCoordsYThresholds[0] + 1f - digitsTexCoordsYThresholds[periodIndex];
            }

            float startThreshold = digitsTexCoordsYThresholds[periodIndex];
            return startThreshold + (digitScrollFrom0To10 - periodIndex) * segment;
        }
    }

    public class ScoreDigitsWithNumberHelper
    {
        private Rect baseDigitTexFrameRect;
        private List<float> digitsTexCoordsYThresholds;

        public ScoreDigitsWithNumberHelper()
        {
            baseDigitTexFrameRect = new Rect(
                0.9136598f,
                0.06314433f,
                0.07731956f,
                0.09793814f
                );

            digitsTexCoordsYThresholds = new List<float>();

            // zero
            digitsTexCoordsYThresholds.Add(0.07421875f);

            // one
            digitsTexCoordsYThresholds.Add(0.1640625f);

            // two
            digitsTexCoordsYThresholds.Add(0.25390625f);

            // three
            digitsTexCoordsYThresholds.Add(0.3515625f);

            // four
            digitsTexCoordsYThresholds.Add(0.4453125f);

            // five
            digitsTexCoordsYThresholds.Add(0.5390625f);

            // six
            digitsTexCoordsYThresholds.Add(0.6328125f);

            // seven
            digitsTexCoordsYThresholds.Add(0.71875f);

            // eight
            digitsTexCoordsYThresholds.Add(0.81640625f);

            // nine
            digitsTexCoordsYThresholds.Add(0.9140625f);
        }

        public void DrawNumberFromScrolledDigits(
            float x,
            float y,
            float digitCellWidth,
            float digitCellHeight,
            float digitCellsInterval,
            List<float> digitsScrollsFrom0To10,
            int digitCellsAmount,
            Texture2D scoreCounterTexture)
        {
            for (int digitIndex = 0; digitIndex < digitCellsAmount; digitIndex++)
            {
                float digitCellX = x + ((digitCellWidth + digitCellsInterval) * digitIndex); 
                float digitCellY = y;

                DrawDigitCell(
                    digitCellX,
                    digitCellY,
                    digitCellWidth,
                    digitCellHeight,
                    digitsScrollsFrom0To10[digitCellsAmount - digitIndex - 1],
                    scoreCounterTexture);
            }
        }

        private void DrawDigitCell(
            float x,
            float y,
            float digitCellWidth,
            float digitCellHeight,
            float digitScrollFrom0To10,
            Texture2D scoreCounterTexture)
        {
            float digitStartTextureCoordinateY = DigitsHelper.GetDigitStartTextureCoordinateY(
                digitsTexCoordsYThresholds,
                digitScrollFrom0To10
                );

            Rect digitCellRect = new Rect(x, y, digitCellWidth, digitCellHeight);
            Rect digitTextureCoordinatesRect = new Rect(
                baseDigitTexFrameRect.x, digitStartTextureCoordinateY,
                baseDigitTexFrameRect.width, baseDigitTexFrameRect.height);

            GUI.DrawTextureWithTexCoords(digitCellRect, scoreCounterTexture,
                digitTextureCoordinatesRect);
        }
    }
}
