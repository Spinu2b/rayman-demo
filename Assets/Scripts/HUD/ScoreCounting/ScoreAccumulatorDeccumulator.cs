﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.HUD.ScoreCounting
{
    public class ScoreAccumulatorDeccumulator : MonoBehaviour
    {
        private ScoreCounter scoreCounter;
        private float lastScoringTime;
        private float comboMaxTimeWindowActivationThresholdSeconds;

        private void Start()
        {
            scheduledScorings = new List<ScheduledScoring>();
            scheduledComboScorings = new List<ScheduledScoring>();
            scheduledFromComboToMainScorings = new List<ScheduledScoring>();
            lastScoringTime = Time.time;
            comboMaxTimeWindowActivationThresholdSeconds = 1f; // 1 second
        }

        public void SetScoreCounter(ScoreCounter scoreCounter)
        {
            this.scoreCounter = scoreCounter;
        }

        private int latestScoringId = 0;
        private int latestFromComboToMainScoringId = 0;
        private List<ScheduledScoring> scheduledScorings;
        private List<ScheduledScoring> scheduledComboScorings;

        private List<ScheduledScoring> scheduledFromComboToMainScorings;

        public void ScorePointsInTime(float amount, int milliseconds)
        {
            int ticksPerSecond = (int)(1f / Time.fixedDeltaTime);
            int ticksToPerformScoringIn = (int)((milliseconds / 1000f) * ticksPerSecond);
            float scoreStepInTick = amount / ticksToPerformScoringIn;

            scheduledScorings.Add(
                new ScheduledScoring(latestScoringId, scoreStepInTick,
                    ticksToPerformScoringIn));
            latestScoringId++;
            float currentTime = Time.time;
            if (currentTime - lastScoringTime < 
                comboMaxTimeWindowActivationThresholdSeconds || 
                scoreCounter.IsComboOngoing())
            {
                scheduledComboScorings.Add(
                    new ScheduledScoring(latestScoringId, scoreStepInTick,
                        ticksToPerformScoringIn));
            }

            lastScoringTime = Time.time;
        }

        public void CancelCurrentComboDeccumulationAltogether()
        {
            scheduledFromComboToMainScorings.Clear();
            latestFromComboToMainScoringId = 0;
        }

        public void DeccumulateComboPointsWithMainPointsScoringInTime(float amount, int milliseconds)
        {
            int ticksPerSecond = (int)(1f / Time.fixedDeltaTime);
            int ticksToPerformScoringIn = (int)((milliseconds / 1000f) * ticksPerSecond);
            float scoreStepInTick = amount / ticksToPerformScoringIn;

            scheduledFromComboToMainScorings.Add(
                new ScheduledScoring(latestFromComboToMainScoringId, scoreStepInTick,
                    ticksToPerformScoringIn));
            latestFromComboToMainScoringId++;
        }

        private void FixedUpdate()
        {
            scoreCounter.FixedUpdate();
            List<int> scoringsToDelete = new List<int>();
            List<int> comboScoringsToDelete = new List<int>();
            List<int> fromComboScoringsToMainToDelete = new List<int>();

            if (scheduledScorings.Count > 0)
            {
                for (int i = 0; i < scheduledScorings.Count; i++)
                {
                    var scheduledScoring = scheduledScorings[i];
                    if (scheduledScoring.fixedUpdatesLeft <= 0)
                    {
                        scoringsToDelete.Add(scheduledScoring.id);
                    }
                    else
                    {
                        scoreCounter.AccumulateScore(scheduledScoring.scoreStepInTick);
                        scheduledScoring.fixedUpdatesLeft--;
                    }
                }
            }

            if (scheduledComboScorings.Count > 0)
            {
                for (int i = 0; i < scheduledComboScorings.Count; i++)
                {
                    var scheduledComboScoring = scheduledComboScorings[i];
                    if (scheduledComboScoring.fixedUpdatesLeft <= 0)
                    {
                        comboScoringsToDelete.Add(scheduledComboScoring.id);
                    }
                    else
                    {
                        scoreCounter.AccumulateComboScore(scheduledComboScoring.scoreStepInTick);
                        scheduledComboScoring.fixedUpdatesLeft--;
                    }
                }
            }


            if (scheduledFromComboToMainScorings.Count > 0)
            {
                for (int i = 0; i < scheduledFromComboToMainScorings.Count; i++)
                {
                    var scheduledFromComboToMainScoring = scheduledFromComboToMainScorings[i];
                    if (scheduledFromComboToMainScoring.fixedUpdatesLeft <= 0)
                    {
                        fromComboScoringsToMainToDelete.Add(scheduledFromComboToMainScoring.id);
                    }
                    else
                    {
                        scoreCounter.AccumulateScore(scheduledFromComboToMainScoring.scoreStepInTick);
                        scoreCounter.DeccumulateComboScore(scheduledFromComboToMainScoring.scoreStepInTick);
                        scheduledFromComboToMainScoring.fixedUpdatesLeft--;
                    }
                }
            }

            scheduledComboScorings.RemoveAll(x => comboScoringsToDelete.Contains(x.id));
            scheduledScorings.RemoveAll(x => scoringsToDelete.Contains(x.id));
            scheduledFromComboToMainScorings.RemoveAll(x => fromComboScoringsToMainToDelete.Contains(x.id));
        }
    }
}
