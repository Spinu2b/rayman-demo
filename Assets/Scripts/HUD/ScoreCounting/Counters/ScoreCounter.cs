﻿using Assets.Scripts.HUD.ScoreCounting.Counters;
using Assets.Scripts.HUD.ScoreCounting.Counters.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.HUD.ScoreCounting
{
    public class ScoreCounter : ScoreCounterBase
    {
        private ComboScoreCounter comboScoreCounter;

        public ScoreCounter(ScoreAccumulatorDeccumulator scoreAccumulatorDeccumulator) : 
            base(new ScoreCounterHelper(), 50, 250, 0.5f, 5, scoreAccumulatorDeccumulator) {
            comboScoreCounter = new ComboScoreCounter(scoreAccumulatorDeccumulator);
            this.scoreCounterHelper.SetScoreCounter(this);
        }

        public override void Draw(float x, float y, float width, float height, Texture2D playerScoreCounterTexture)
        {
            comboScoreCounter.Draw(x, y, width, height, playerScoreCounterTexture);
            base.Draw(x, y, width, height, playerScoreCounterTexture);
        }

        public void AccumulateComboScore(float accumulation)
        {
            comboScoreCounter.AccumulateScore(accumulation);
        }

        public void DeccumulateComboScore(float deccumulation)
        {
            comboScoreCounter.DeccumulateScore(deccumulation);
        }

        public bool IsComboOngoing()
        {
            return comboScoreCounter.IsComboOngoing();
        }

        public override void FixedUpdate()
        {
            base.FixedUpdate();
            comboScoreCounter.FixedUpdate();
        }
    }
}
