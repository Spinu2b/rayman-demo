﻿using Assets.Scripts.HUD.ScoreCounting.Counters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.HUD.ScoreCounting
{
    public class ComboScoreCounter : ScoreCounterBase
    {
        //private ScoreCounter mainScoreCounter;
        //private float offsetY;
        private bool isComboOngoing;
        private bool isComboDeccumulationOngoing;

        private float comboDeactivationMaxTimeWindowSeconds;

        private float lastComboScoreAccumulationTime;

        //private float comboActivatedYOffsetThreshold = 10f;

        public ComboScoreCounter(
            ScoreAccumulatorDeccumulator scoreAccumulatorDeccumulator) :
           base(new ComboScoreCounterHelper(), 50, 250, 0.5f, 5, scoreAccumulatorDeccumulator)
        {
            this.scoreCounterHelper.SetScoreCounter(this);
            //this.mainScoreCounter = mainScoreCounter;
            this.lastComboScoreAccumulationTime = Time.time;
            //this.offsetY = 0;
            this.isComboOngoing = false;
            comboDeactivationMaxTimeWindowSeconds = 1f;
        }

        public override void AccumulateScore(float accumulation)
        {
            base.AccumulateScore(accumulation);
            isComboOngoing = true;
            lastComboScoreAccumulationTime = Time.time;
            scoreAccumulatorDeccumulator.CancelCurrentComboDeccumulationAltogether();
            isComboDeccumulationOngoing = false;
        }

        public override void DeccumulateScore(float deccumulation)
        {
            base.DeccumulateScore(deccumulation);
            lastComboScoreAccumulationTime = Time.time;
        }

        public override void FixedUpdate()
        {
            base.FixedUpdate();
            float currentTime = Time.time;
            if (currentTime - lastComboScoreAccumulationTime > comboDeactivationMaxTimeWindowSeconds)
            {
                if (currentScore > 0.1f && !isComboDeccumulationOngoing)
                {
                    scoreAccumulatorDeccumulator.DeccumulateComboPointsWithMainPointsScoringInTime(currentScore, 1000);
                    //scoreAccumulatorDeccumulator.ScorePointsWithoutComboInTime(currentScore, 1000);
                    isComboDeccumulationOngoing = true;
                }
                else if (isComboDeccumulationOngoing)
                {
                    isComboDeccumulationOngoing = false;
                    // give player yet another brief moment after current deccumulation to have him have
                    // a chance to lengthen the combo
                    lastComboScoreAccumulationTime = Time.time;
                    //isComboOngoing = false;
                }
                else
                {
                    isComboOngoing = false;
                }                
            }
        }

        public bool IsComboOngoing()
        {
            return isComboOngoing;
        }
    }
}
