﻿using Assets.Scripts.HUD.ScoreCounting.Counters.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.HUD.ScoreCounting.Counters
{
    public abstract class ScoreCounterBase
    {
        protected float currentScore;
        protected ScoreCounterHelperBase scoreCounterHelper;

        protected List<float> digitsScrollsFrom0To10;
        protected List<float> digitsScrollsOnBeginningOfCurrentCounterNormalization;

        protected bool counterNormalizationOngoing;

        protected float lastScoreAccumulationTime;
        protected int counterNormalizationWaitTimeThresholdMilliseconds;

        protected int normalizationTimeMilliseconds;

        protected float animatedNormalizationMaxTime;
        protected float normalizationStartTime;

        protected int digitsAmount;

        protected ScoreAccumulatorDeccumulator scoreAccumulatorDeccumulator;

        public ScoreCounterBase(
            ScoreCounterHelperBase scoreCounterHelper,
            int counterNormalizationWaitTimeThresholdMilliseconds,
            int normalizationTimeMilliseconds,
            float animatedNormalizationMaxTime,
            int digitsAmount,
            ScoreAccumulatorDeccumulator scoreAccumulatorDeccumulator,
            float initialScore = 0f)
        {
            this.currentScore = initialScore;
            this.scoreCounterHelper = scoreCounterHelper;
            lastScoreAccumulationTime = Time.time;
            normalizationStartTime = Time.time;
            this.counterNormalizationWaitTimeThresholdMilliseconds = 
                counterNormalizationWaitTimeThresholdMilliseconds;
            this.normalizationTimeMilliseconds = normalizationTimeMilliseconds;

            this.animatedNormalizationMaxTime = animatedNormalizationMaxTime;

            this.digitsAmount = digitsAmount;

            this.digitsScrollsFrom0To10 = new List<float>();
            this.digitsScrollsOnBeginningOfCurrentCounterNormalization = new List<float>();

            this.scoreAccumulatorDeccumulator = scoreAccumulatorDeccumulator;

            for (int i = 0; i < digitsAmount; i++)
            {
                this.digitsScrollsFrom0To10.Add(0f);
                this.digitsScrollsOnBeginningOfCurrentCounterNormalization.Add(0f);
            }

            //this.digitsScrollsFrom0To10 =
            //    new List<float>() { 0f, 0f, 0f, 0f, 0f };
            //this.digitsScrollsOnBeginningOfCurrentCounterNormalization =
            //    new List<float>() { 0f, 0f, 0f, 0f, 0f };
            counterNormalizationOngoing = false;
        }

        public virtual void AccumulateScore(float accumulation)
        {
            currentScore += accumulation;
            lastScoreAccumulationTime = Time.time;
            DigitsScrollsHelper
                .AccumulateScrollsWithScoreNumberStep(
                    digitsScrollsFrom0To10, accumulation);
        }

        public virtual void DeccumulateScore(float deccumulation)
        {
            currentScore -= deccumulation;
            lastScoreAccumulationTime = Time.time;
            DigitsScrollsHelper
                .DecummulateScrollsWithScoreNumberStep(
                    digitsScrollsFrom0To10, deccumulation);
        }

        public virtual void Draw(
            float x, float y,
            float width, float height,
            Texture2D playerScoreCounterTexture)
        {
            scoreCounterHelper.DrawScoreCounter(
                x, y,
                width, height,
                digitsScrollsFrom0To10,
                playerScoreCounterTexture
                );
        }

        public virtual void FixedUpdate()
        {
            float currentTime = Time.time;
            if (currentTime - lastScoreAccumulationTime >
                (counterNormalizationWaitTimeThresholdMilliseconds / 1000f) || counterNormalizationOngoing)
            {
                if (!counterNormalizationOngoing)
                {
                    //Debug.Log("Starting counter normalization..., time difference " +
                    //    (currentTime - lastScoreAccumulationTime));
                    // we start normalization procedure -
                    // this if is accessed only once during each start of normalization
                    digitsScrollsOnBeginningOfCurrentCounterNormalization =
                        new List<float>(digitsScrollsFrom0To10);

                    normalizationStartTime = Time.time;
                }

                counterNormalizationOngoing = true;

                if (currentTime - normalizationStartTime < animatedNormalizationMaxTime)
                {
                    NormalizeCounterTowardsNumber(currentScore);
                }
                else
                {
                    // this is taking too long, just force the counter to particular number
                    SetCounterToDisplayNumber(currentScore);
                    counterNormalizationOngoing = false;
                }
            }
            else
            {
                counterNormalizationOngoing = false;
            }
        }

        protected void SetCounterToDisplayNumber(float number)
        {
            DigitsScrollsHelper.SetDigitsScrollsToReflectNumber(
                digitsScrollsFrom0To10, number);
        }

        protected void NormalizeCounterTowardsNumber(float number)
        {
            for (int digitIndex = 0;
                digitIndex < digitsScrollsFrom0To10.Count;
                digitIndex++)
            {
                int properDigitIndex = digitsScrollsFrom0To10.Count - digitIndex - 1;
                digitsScrollsFrom0To10[properDigitIndex] +=
                    DigitsScrollsHelper
                        .GetDigitScrollNormalizationInTimeStep(
                            number, properDigitIndex,
                            digitsScrollsFrom0To10[properDigitIndex],
                            digitsScrollsOnBeginningOfCurrentCounterNormalization[properDigitIndex],
                            normalizationTimeMilliseconds);
                digitsScrollsFrom0To10[properDigitIndex] = digitsScrollsFrom0To10[properDigitIndex] % 10f;
            }
        }
    }
}
