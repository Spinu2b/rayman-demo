﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.HUD.Targetting
{
    public enum ShootSightDirection
    {
        UP_ARROW_STRAIGHT,
        UP_ARROW_LEFT,    
        UP_ARROW_RIGHT,

        DOWN_ARROW_STRAIGHT,
        DOWN_ARROW_LEFT,
        DOWN_ARROW_RIGHT
    }
}
