using Assets.Scripts.HUD.ScoreCounting;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScheduledScoring
{
    public float scoreStepInTick;
    public int fixedUpdatesLeft;
    public int id;

    public ScheduledScoring(int id, float scoreStepInTick, int fixedUpdatesLeft)
    {
        this.id = id;
        this.scoreStepInTick = scoreStepInTick;
        this.fixedUpdatesLeft = fixedUpdatesLeft;
    }
}

public class DrawPlayerScoreCounter : MonoBehaviour
{
    public Texture2D playerScoreCounterTexture;
    private ScoreCounter scoreCounter;

    private void Awake()
    {
        ScoreAccumulatorDeccumulator scoreAccumulatorDeccumulator = 
            FindObjectOfType<ScoreAccumulatorDeccumulator>();
        scoreCounter = new ScoreCounter(scoreAccumulatorDeccumulator);
        scoreAccumulatorDeccumulator.SetScoreCounter(scoreCounter);
    }

    private void OnGUI()
    {
        scoreCounter.Draw(
            70, 70,
            0,
            0,
            playerScoreCounterTexture
            );
    }
}
