﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.HUD
{
    public class DrawTestHUDTextureCoordinates : MonoBehaviour
    {
        private Rect texCoordXRect;
        private Rect texCoordYRect;

        private Rect texCoordWidthRect;
        private Rect texCoordHeightRect;

        private Rect testRect;

        public Texture2D texture;

        public float srcWidth;
        public float srcHeight;
        public float srcX;
        public float srcY;

        private void Start()
        {
            testRect = new Rect(
            Screen.width / 2,
            Screen.height / 2,
            200,
            200
            );

            float center = Screen.width / 2.0f;
            //rect = new Rect(center - 200, 200, 400, 250);

            texCoordXRect = new Rect(center - 200, 125, 400, 30);
            texCoordYRect = new Rect(center - 200, 160, 400, 30);

            texCoordWidthRect = new Rect(center - 200, 190, 400, 30);
            texCoordHeightRect = new Rect(center - 200, 230, 400, 30);
        }

        void OnGUI()
        {
            srcWidth = GUI.HorizontalSlider(texCoordWidthRect, srcWidth, -1f, 1f);
            srcHeight = GUI.HorizontalSlider(texCoordHeightRect, srcHeight, -1f, 1f);

            srcX = GUI.HorizontalSlider(texCoordXRect, srcX, 0.0f, 1f);
            srcY = GUI.HorizontalSlider(texCoordYRect, srcY, 0.0f, 1f);

            GUI.DrawTextureWithTexCoords(testRect, texture,
                new Rect(srcX, srcY, srcWidth, srcHeight));
        }
    }
 }
