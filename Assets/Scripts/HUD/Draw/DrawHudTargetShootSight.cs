﻿using Assets.Scripts.HUD.Targetting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.HUD
{
    public class DrawHudTargetShootSight : MonoBehaviour
    {
        public Texture2D targetShootSightCircle;
        public Texture2D targetShootSightRightArrow;
        public Texture2D targetShootSightLeftArrow;
        public Texture2D targetShootSightStraightArrow;

        private Vector3 targetShootSightScreenPosition;

        private bool isShootSightVisible = false;

        private float targetShootSightWidthPixels = 150;
        private float targetShootSightHeightPixels = 150;

        private float circleSizeFluctuationStep = 0.1f;
        private float currentCircleSizeFluctuation = 0f;

        private ShootSightDirection shootSightDirection;

        public void SetIsShootSightVisible(bool isShootSightVisible)
        {
            this.isShootSightVisible = isShootSightVisible;
        }

        public void SetShootSightScreenCoordinates(
            Vector3 targetShootSightScreenPosition)
        {
            this.targetShootSightScreenPosition = targetShootSightScreenPosition;
        }

        private void FixedUpdate()
        {
            if (isShootSightVisible)
            {
                currentCircleSizeFluctuation += circleSizeFluctuationStep;
            }
        }

        private void OnGUI()
        {
            if (isShootSightVisible)
            {
                float circleSizeMultiplier = (Mathf.Sin(currentCircleSizeFluctuation) / 5.0f) + 0.8f;
                float currentCircleWidth = targetShootSightWidthPixels * circleSizeMultiplier;
                float currentCircleHeight = targetShootSightHeightPixels * circleSizeMultiplier;

                Rect shootSightCirclePosition = 
                    new Rect(
                        targetShootSightScreenPosition.x - (currentCircleWidth/2.0f),
                        targetShootSightScreenPosition.y - (currentCircleHeight/2.0f),
                        currentCircleWidth,
                        currentCircleHeight
                        );



                Rect shootArrowPositionUpToBottom =
                    new Rect(
                        targetShootSightScreenPosition.x - (targetShootSightWidthPixels / 2.0f),
                        targetShootSightScreenPosition.y - (targetShootSightHeightPixels / 2.0f),
                        targetShootSightWidthPixels,
                        targetShootSightHeightPixels
                        );

                Rect shootArrowPositionBottomToUp =
                    new Rect(
                        targetShootSightScreenPosition.x - (targetShootSightWidthPixels / 2.0f),
                        targetShootSightScreenPosition.y - (targetShootSightHeightPixels / 2.0f) + targetShootSightHeightPixels,
                        targetShootSightWidthPixels,
                        -targetShootSightHeightPixels
                        );


                GUI.DrawTexture(shootSightCirclePosition, targetShootSightCircle);

                if (shootSightDirection == ShootSightDirection.UP_ARROW_STRAIGHT)
                {
                    GUI.DrawTexture(shootArrowPositionUpToBottom, targetShootSightStraightArrow);
                } else if (shootSightDirection == ShootSightDirection.UP_ARROW_RIGHT)
                {
                    GUI.DrawTexture(shootArrowPositionUpToBottom, targetShootSightRightArrow);
                } else if (shootSightDirection == ShootSightDirection.UP_ARROW_LEFT)
                {
                    GUI.DrawTexture(shootArrowPositionUpToBottom, targetShootSightLeftArrow);
                }
                
                
                
                else if (shootSightDirection == ShootSightDirection.DOWN_ARROW_STRAIGHT)
                {
                    GUI.DrawTexture(shootArrowPositionBottomToUp, targetShootSightStraightArrow);
                }
                else if (shootSightDirection == ShootSightDirection.DOWN_ARROW_RIGHT)
                {
                    GUI.DrawTexture(shootArrowPositionBottomToUp, targetShootSightRightArrow);
                }
                else if (shootSightDirection == ShootSightDirection.DOWN_ARROW_LEFT)
                {
                    GUI.DrawTexture(shootArrowPositionBottomToUp, targetShootSightLeftArrow);
                }
            }
        }

        public void SetShootingDirection(ShootSightDirection shootSightDirection)
        {
            this.shootSightDirection = shootSightDirection;
        }
    }
}
