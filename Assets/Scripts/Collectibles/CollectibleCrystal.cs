using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleCrystal : MonoBehaviour
{
    public bool canBeCollected = true;
    public float rotationSpeed = 1.0f;

    private bool isInMiddleOfBeingCollected = false;
    private Transform playerTransform;

    private AudioSource collectedSound;

    private float currentCollectingRotationAngle = 0f;
    private float collectingRotationSpeed = 20f;
    private float collectingAscendingSpeed = 0.6f;

    private float crystalDestroyHeightThreshold = 100f;

    private float currentHeight;

    public void InvokeCollectedBehaviour(Transform playerTransform)
    {
        if (canBeCollected)
        {
            canBeCollected = false;
            isInMiddleOfBeingCollected = true;
            this.playerTransform = playerTransform;
            currentHeight = playerTransform.position.y + 1.0f;
            collectedSound.Play();
        }        
    }

    private void Awake()
    {
        collectedSound = GetComponent<AudioSource>();
    }

    private void FixedUpdate()
    {
        if (isInMiddleOfBeingCollected)
        {
            transform.position = playerTransform.position + 
                Quaternion.AngleAxis(currentCollectingRotationAngle, Vector3.up) * Vector3.forward;
            transform.position = new Vector3(transform.position.x, currentHeight, transform.position.z);

            if (currentCollectingRotationAngle > 360f)
            {
                transform.position = new Vector3(
                    playerTransform.position.x, currentHeight,
                    playerTransform.position.z);
                currentHeight += collectingAscendingSpeed;
            } else
            {
                currentCollectingRotationAngle += collectingRotationSpeed;
            }
            
            if (currentHeight > playerTransform.position.y + crystalDestroyHeightThreshold)
            {
                Debug.Log("Crystal destroyed!");
                Destroy(gameObject);
            }
        } else
        {
            transform.Rotate(new Vector3(0f, rotationSpeed, 0f));
        }
    }
}
